import React, { useContext } from "react";
import GlobalStyle from "./components/style-components/global/GlobalStyle";
import { BrowserRouter as Router, Route } from "react-router-dom";
import MainPage from "./components/main-page/MainPage";
import LoginPage from "./components/login/LoginPage";
import TrendingPage from "./components/trending/TrendingPage";
import SignUp from "components/Signup/SignUpPage";
import { ThemeProvider } from "styled-components";
import Profile from "./components/profile/Profile";
import useTheme from "./components/hooks/useTheme";
import PrivateRoute from "components/PrivateRoute";
import AuthContextProvider from "./components/Context/AuthContext";
import UserContextProvider from "components/Context/UserContext";
import ChangeMode from "./components/Theming/ChangeMode";
import DisplayModal from "./components/main-page/Left-side-bar/DisplayModal";
import Chat from "components/chat/Chat";
import ChatContextProvider from "components/Context/ChatContext";
import TimelineContextProvider from "./components/Context/TimelineContext";
import NewChatModal from "components/chat/NewChatModal";
import { ModalContext } from "components/Context/ModalConext";
import ProfileQR from "components/profile/ProfileQR";
import AddChatUser from "components/chat/AddChatUser";
import InfoModal from "components/InfoModal";
import ProfileSettingsModal from "./components/profile/ProfileSettingsModal";

function App() {
  const theme = useTheme();
  const { modal } = useContext(ModalContext);
  return (
    <ThemeProvider theme={theme}>
      <AuthContextProvider>
        <UserContextProvider>
          <TimelineContextProvider>
            <GlobalStyle />
            <Router>
              {modal.displaySettings && <DisplayModal />}
              {modal.newChat && <NewChatModal />}
              {modal.qr && <ProfileQR />}
              {modal.twatterInfo && <InfoModal />}
              {modal.profileSettings && <ProfileSettingsModal />}

              <div className="App">
                <PrivateRoute
                  exact
                  path={["/home", "/"]}
                  component={MainPage}
                />
                <Route exact path="/login" component={LoginPage} />
                <Route exact path="/signup" component={SignUp} />
                <Route path="/profile/:profileVariable" component={Profile} />
                <Route path="/trending" component={TrendingPage} />
                <ChatContextProvider>
                  {modal.addChatUser && <AddChatUser />}

                  <PrivateRoute path="/messages" component={Chat} />
                </ChatContextProvider>
              </div>
            </Router>
          </TimelineContextProvider>
        </UserContextProvider>
      </AuthContextProvider>
    </ThemeProvider>
  );
}

export default App;
