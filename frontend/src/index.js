import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import Axios from "axios";
import Spinner from "components/shared/Spinner";
import { SuspenseWithPerf, FirebaseAppProvider } from "reactfire";
import ModalContextProvider from "components/Context/ModalConext";
const firebaseConfig = JSON.parse(process.env.REACT_APP_FIREBASE_CONFIG);

Axios.defaults.withCredentials = true;
ReactDOM.render(
  <FirebaseAppProvider firebaseConfig={firebaseConfig}>
    <React.StrictMode>
      <ModalContextProvider>
        <App />
      </ModalContextProvider>
    </React.StrictMode>
  </FirebaseAppProvider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
