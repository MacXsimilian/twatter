import React from "react";
import styled from "styled-components";
import image from "../../../gorillatwatter.png";

const WhoToFollowStyle = styled.div`
  background-color: var(--offSetBackground);
  width: 100%;
  min-height: 200px;
  border-radius: 1.5rem;
  margin-bottom: 15px;

  .who-to-follow-title {
    font-size: 2rem;
    font-weight: 900;
    width: 100%;
    height: 45px;
    padding: 10px 15px;
    border-bottom: 1px solid;
    border-color: var(--borderColor);
  }

  .who-to-follow-items {
    position: relative;
    display: grid;
    grid-template-columns: 1fr 5fr;
    padding: 10px 15px;
    border-bottom: 1px solid;
    border-color: var(--borderColor);
    transition: background-color 0.24s ease;
    cursor: pointer;
  }

  .who-to-follow-items:hover {
    background-color: rgba(125, 134, 146, 0.2);
  }

  .who-to-follow-items img {
    width: 50px;
    border-radius: 3rem;
  }

  .who-to-follow-items-content {
    margin-left: 10px;
  }

  .who-to-follow-item-names {
    display: flex;
    flex-direction: row;
    font-size: 1.5rem;
    font-weight: 700;
  }

  .who-to-follow-item-tags {
    color: rgb(136, 153, 166);
    font-size: 1.3rem;
  }

  .follow-button {
    position: absolute;
    top: 15px;
    right: 20px;
    background-color: rgb(0, 0, 0, 0);
    border: 1px solid var(--secondary);
    width: 75px;
    height: 30px;
    border-radius: 3rem;
    color: var(--secondary);
    font-size: 1.4rem;
    font-weight: 600;
    font-family: Arial, serif;
    transition: background-color 0.24s ease;
    cursor: pointer;
    outline: none;
  }

  .follow-button:hover {
    background-color: var(--secondaryTransparent);
  }

  .show-more {
    display: flex;
    justify-content: start;
    align-items: center;
    font-size: 1.5rem;
    color: var(--secondary);
    padding: 13px 15px;
    transition: background-color 0.24s ease;
    cursor: pointer;
    border-bottom-left-radius: 1.5rem;
    border-bottom-right-radius: 1.5rem;
  }
  .show-more:hover {
    background-color: rgba(125, 134, 146, 0.2);
  }
`;

function WhoToFollow(props) {
  return (
    <WhoToFollowStyle>
      <div className="who-to-follow-title">{props.title}</div>
      <div className="who-to-follow-container">
        <div className="who-to-follow-items">
          <img src={image} alt="" />
          <div className="who-to-follow-items-content">
            <div className="who-to-follow-item-names">
              <div>Formula 1</div>
              <i className="fas fa-check"></i>
            </div>
            <div className="who-to-follow-item-tags">@F1</div>
          </div>
          <button className="follow-button"> Follow</button>
        </div>
        <div className="who-to-follow-items">
          <img src={image} alt="" />
          <div className="who-to-follow-items-content">
            <div className="who-to-follow-item-names">
              <div>PC Gamer</div>
              <i className="fas fa-check"></i>
            </div>
            <div className="who-to-follow-item-tags">@pcgamer</div>
          </div>
          <button className="follow-button"> Follow</button>
        </div>
      </div>
      <div id="who-to-follow-more-button" className="show-more">
        Show more
      </div>
    </WhoToFollowStyle>
  );
}

export default WhoToFollow;
