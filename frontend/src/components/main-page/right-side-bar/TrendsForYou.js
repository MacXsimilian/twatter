import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import icons from "../../shared/icons";
import Axios from "axios";
import Trend from "./Trend";

const TrendsForYouStyle = styled.div`
    background-color: var(--offSetBackground);
    width: 100%;
    min-height: 200px;
    border-radius: 1.5rem;
    margin-bottom: 15px;

    .trend-title-container {
        display: flex;
        flex-direction: row;
        align-items: center;
        justify-content: space-between;
        width: 100%;
        height: 45px;
        padding: 10px 15px;
        border-bottom: 1px solid;
        border-color: var(--borderColor);
    }

    .trend-title-name {
        font-size: 2rem;
        font-weight: 900;
    }

    .trend-title-container i {
        font-size: 2rem;
        color: rgb(29, 161, 242);
        padding: 5px 10px;
        border-radius: 5rem;
        cursor: pointer;
    }

    .trend-title-container i:hover {
        background-color: rgba(68, 110, 163, 0.2);
    }

    .trend-items {
        position: relative;
        padding: 10px 15px;
        border-bottom: 1px solid;
        border-color: var(--borderColor);
        transition: background-color 0.25s ease;
        cursor: pointer;
    }

    .trend-items:hover {
        background-color: rgba(125, 134, 146, 0.2);
    }

    .trend-items i {
        position: absolute;
        top: 7px;
        right: 10px;
        font-size: 1.4rem;
        color: rgb(136, 153, 166);
        padding: 8px;
        border-radius: 3rem;
        transition: background-color 0.24s ease;
        cursor: pointer;
    }

    .trend-items i:hover {
        background-color: var(--secondaryTransparent);
    }

    .trend-types {
        color: rgb(136, 153, 166);
        font-size: 1.3rem;
    }

    .trend-titles {
        font-size: 1.5rem;
        font-weight: 700;
    }

    .trend-tweet-numbers {
        color: rgb(136, 153, 166);
        font-size: 1.2rem;
        padding-top: 10px;
    }

    .show-more {
        display: flex;
        justify-content: start;
        align-items: center;
        font-size: 1.5rem;
        color: var(--secondary);
        padding: 13px 15px;
        transition: background-color 0.24s ease;
        cursor: pointer;
        border-bottom-left-radius: 1.5rem;
        border-bottom-right-radius: 1.5rem;
    }
    .show-more:hover {
        background-color: rgba(125, 134, 146, 0.2);
    }

    .settings svg {
        transition: background-color 0.24s ease;

        height: 33px;
        fill: var(--secondary);
        border-radius: 50%;
        padding: 5px;
        cursor: pointer;
    }

    .settings:hover svg {
        background-color: var(--secondaryTransparent);
    }
`;

function TrendsForYou() {
    const [trends, setTrends] = useState({ trendingHashtags: [] });

    useEffect(() => {
        Axios.get(process.env.REACT_APP_API_URL + ":" + process.env.REACT_APP_PORT + "/api/trends/monthly")
            .then((resp) => {
                setTrends(resp.data);
            })
            .catch((resp) => {
                return console.log(resp);
            });
    }, []);

    let topTrends = [];

    let id = 0;
    for (let trend of trends.trendingHashtags) {
        topTrends.push({ id, trend });
        if (id++ === 2) break;
    }

    return (
        <TrendsForYouStyle>
            <div className="trend-title-container">
                <div className="trend-title-name">Trends for you</div>
                <span id="trends-settings" className="settings">
                    {icons.settings}
                </span>
            </div>
            <div className="trend-container">
                {topTrends.length > 0 &&
                    topTrends.map((topTrend) => {
                        return <Trend key={topTrend.id} trend={topTrend.trend} />;
                    })}
            </div>

            <Link to="/trending" id="trends-more-button" className="show-more">
                Show more
            </Link>
        </TrendsForYouStyle>
    );
}

export default TrendsForYou;
