import React, { useState, useContext } from "react";
import { Link } from "react-router-dom";

export default function Trend({ trend }) {
  const [selectedTrend, setSelectedTrend] = useState();

  const clickOnTrend = (hashtag, event) => {
    setSelectedTrend(hashtag);
  };

  return (
    <Link
      className="trend-items"
      style={{ color: "inherit", textDecoration: "none", display: "block" }}
      to={`/trending/${trend.hashtag}`}
      onClick={(e) => clickOnTrend(trend.hashtag, e)}
    >
      <div className="trend-types">Trending in Hungary</div>
      <i className="fas fa-chevron-down"></i>
      <span id="trend-title" className="trend-titles">
        {trend.hashtag}
      </span>
      <div id="-trend-count" className="trend-tweet-numbers">
        {trend.count} Tweets
      </div>
    </Link>
  );
}
