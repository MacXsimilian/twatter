import React, { useCallback, useState } from "react";
import styled from "styled-components";
import icons from "../../shared/icons";
import SearchBarDropDown from "./SearchBarDropDown";
import debounce from "lodash.debounce";
import Axios from "axios";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";

const SearchBarStyle = styled.div`
  position: relative;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 42px;
  background-color: ${(props) =>
    `${props.dropDown ? `var(--background)` : `var(--offSetBackground)`}`};
  border-color: ${(props) =>
    `${props.dropDown ? `var(--secondary)` : `var(--offSetBackground)`}`};
  border-style: solid;
  border-width: 1px;
  border-radius: 2rem;
  margin: 5px 5px 15px 0;

  i {
    color: rgb(136, 153, 166);
    font-size: 25px;
    height: 25px;
    margin: 17px;
  }

  .search-input {
    font-size: 1.5rem;
    font-weight: 100;
    font-family: Arial, serif;
    background-color: rgb(0, 0, 0, 0);
    border: none;
    width: 100%;
    height: 100%;
    color: var(--foreground);
    outline: none;
  }
  .search-input::placeholder {
    color: rgb(175, 175, 175);
  }

  .icon svg {
    padding-left: 15px;
    padding-right: 15px;
    height: 23px;
    fill: ${(props) =>
      `${props.dropDown ? `var(--secondary)` : `rgb(175, 175, 175)`}`};
  }

  .clear-input-button {
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    right: 10px;
    top: 50%;
    border-radius: 60%;
    transform: translateY(-50%);
    height: 23px;
    width: 23px;
    background-color: var(--secondary);
    transition: opacity 0.24s ease;
    svg {
      width: 18px;
      height: 18px;
      fill: var(--background);
    }
    :hover {
      cursor: pointer;
      opacity: 0.8;
    }
  }
`;

function SearchBar(props) {
  const [dropDown, setDropDown] = useState(false);

  const [inputContent, setInputContent] = useState("");

  const [offers, setOffers] = useState([]);

  const sendPostRequest = (nextValue) => {
    Axios.post(
      process.env.REACT_APP_API_URL +
        ":" +
        process.env.REACT_APP_PORT +
        "/api/search/post",
      { query: nextValue }
    )
      .then((resp) => {
        if (resp.data.length < 1) {
          setOffers([]);
        } else {
          setOffers(resp.data);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const debouncedSave = useCallback(
    debounce((nextValue) => sendPostRequest(nextValue), 300),
    []
  );

  const inputFocus = () => {
    setDropDown(true);
  };

  const clearInputField = (e) => {
    setInputContent("");
    e.currentTarget.previousElementSibling.value = "";
    setOffers([]);
    e.currentTarget.previousElementSibling.focus();
  };

  const onEnterPress = (e) => {
    if (e.key === "Enter") {
      console.log(e.target.value);
    }
  };

  const inputBlur = (e) => {
    setDropDown(false);
  };

  const onChange = (e) => {
    setInputContent(e.target.value);
    if (e.target.value.length >= 3) {
      debouncedSave(e.target.value);
    } else {
      setOffers([]);
    }
  };

  return (
    <ClickAwayListener onClickAway={inputBlur}>
      <SearchBarStyle dropDown={dropDown}>
        <div className="icon">{icons.search}</div>
        <input
          autoComplete="off"
          id="searchbar-input"
          onClick={inputFocus}
          onKeyDown={onEnterPress}
          onChange={onChange}
          className="search-input"
          type="text"
          placeholder="Search Twatter"
        />
        {inputContent !== "" && (
          <div className="clear-input-button" onClick={clearInputField}>
            {icons.close}
          </div>
        )}
        {dropDown && (
          <SearchBarDropDown offers={offers} inputContent={inputContent} />
        )}
      </SearchBarStyle>
    </ClickAwayListener>
  );
}

export default SearchBar;
