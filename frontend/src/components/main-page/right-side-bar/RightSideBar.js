import React from "react";
import styled from "styled-components";
import SearchBar from "./SearchBar";
import TrendsForYou from "./TrendsForYou";
import WhoToFollow from "./WhoToFollow";
import SidebarFooter from "./SidebarFooter";

const RightSideBarStyle = styled.div`
  grid-area: rightBar;
  width: max(350px);
  margin: 0 20px;

  @media screen and (max-width: 1090px) {
    width: max(280px);
  }

  @media screen and (max-width: 1000px) {
    display: none;
  }
`;

function RightSideBar() {
  return (
    <RightSideBarStyle>
      <SearchBar />
      <TrendsForYou />
      <WhoToFollow title="Who to follow" />
      <SidebarFooter />
    </RightSideBarStyle>
  );
}

export default RightSideBar;
