import React from "react";
import styled from "styled-components";
import { Link, useHistory } from "react-router-dom";
import profilepic from "../../shared/default_profile_200x200.png";
import Spinner from "../../shared/Spinner";

const SearchBarDropDownStyled = styled.div`
  position: absolute;
  top: 42px;
  display: flex;
  flex-direction: column;
  width: 350px;
  z-index: 1000;
  box-shadow: 0 1px 5px 0 var(--foreground);
  border-radius: 2px;

  a {
    transition: background-color 0.24s ease;
  }

  a:hover {
    cursor: pointer;
    background-color: var(--secondaryHover);
  }

  .empty-content-message {
    display: flex;
    justify-content: center;
    height: 100px;
    color: rgba(175, 175, 175, 0.65);
    background-color: var(--background);
    font-size: 14px;
    font-weight: bold;
    padding: 20px 10px;
  }

  .input-content {
    text-decoration: none;
    height: 50px;
    padding: 15px;
    color: var(--foreground);
    background-color: var(--background);
    font-size: 15px;
    font-weight: bold;
    border-bottom: 1px solid rgb(56, 68, 77);
  }

  .offer-container {
    display: flex;
    flex-direction: column;
    width: inherit;
    border-radius: 2px;
  }

  .offers {
    border-bottom: 1px solid rgb(56, 68, 77);
    text-decoration: none;
    height: 50px;
    background-color: var(--background);
    color: var(--foreground);
    font-size: 15px;
    font-weight: bold;
    display: flex;
    justify-content: start;
    align-items: center;
    padding: 15px;
    border-radius: 2px;
  }

  .csiik {
    width: 100%;
    height: 10px;
    background-color: var(--searchBackground);
  }

  .people-offers {
    border-bottom: 1px solid rgb(56, 68, 77);
    text-decoration: none;
    height: 60px;
    background-color: var(--background);
    display: grid;
    padding: 10px 15px;
    grid-template-columns: 40px 260px;
  }

  img {
    height: 40px;
    width: 40px;
    border-radius: 60%;
  }

  .people-offers-text-containers {
    display: flex;
    justify-content: center;
    flex-direction: column;
    position: relative;
    left: 10px;
  }

  .people-offers-names {
    color: var(--foreground);
    font-weight: bold;
    font-size: 1.6rem;
    font-family: system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI",
      Roboto, Ubuntu, "Helvetica Neue", sans-serif;
  }

  .people-offers-emails {
    color: rgb(136, 153, 166);
    font-size: 1.3rem;
    font-family: system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI",
      Roboto, Ubuntu, "Helvetica Neue", sans-serif;
  }

  .spinner {
    display: flex;
    justify-content: center;
    height: 100px;
    background-color: var(--background);
    font-size: 14px;
    font-weight: bold;
    padding: 20px 10px;
  }
`;

function SearchBarDropDown(props) {
  return (
    <SearchBarDropDownStyled id="searchbar-dropdown">
      {!props.inputContent && (
        <div className="empty-content-message">
          Try searching for people, topics, or keywords
        </div>
      )}
      {props.offers.length < 1 && props.inputContent && (
        <div className="spinner">
          <Spinner />
        </div>
      )}
      {props.offers.length > 0 && (
        <>
          <Link to="#" className="input-content">
            Search for "{props.inputContent}"
          </Link>
          {/*<div className="offer-container">*/}
          {/*    <Link to="#" className="offers">asd</Link>*/}
          {/*    <Link to="#" className="offers">bello</Link>*/}
          {/*    <Link to="#" className="offers">hello</Link>*/}
          {/*</div>*/}
          <div className="csiik"></div>
          <div>
            {props.offers.map((offer) => {
              return (
                <Link
                  to={`/profile/${offer.name}`}
                  onClick={() =>
                    (window.location.href = `/profile/${offer.name}`)
                  }
                  className="people-offers"
                >
                  <img src={profilepic} alt="" />
                  <div className="people-offers-text-containers">
                    <div className="people-offers-names">{offer.name}</div>
                    <div className="people-offers-emails">@{offer.name}</div>
                  </div>
                </Link>
              );
            })}
          </div>
          <Link to="#" className="input-content">
            Go to @{props.inputContent}
          </Link>
        </>
      )}
    </SearchBarDropDownStyled>
  );
}

export default SearchBarDropDown;
