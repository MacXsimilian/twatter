import React, { useContext, useState, useEffect, Suspense } from "react";
import Axios from "axios";
import { UserContext } from "components/Context/UserContext";
import Tweet from "../Tweet/Tweet";
import Spinner from "components/shared/Spinner";
import { TimelineContext } from "../../Context/TimelineContext";
import _ from "lodash";
export default function TweetFeed(props) {
  const { user } = useContext(UserContext);
  const { tweets, setTweets } = useContext(TimelineContext);

  useEffect(() => {
    Axios.get(
      `${process.env.REACT_APP_API_URL}:${
        process.env.REACT_APP_PORT
      }/api/timeline/${props.src ? props.src.path : "home"}/${
        props.src ? props.src.userid : user.id
      }`
    )
      .then(({ data }) => {
        setTweets(
          _.sortBy(data, [
            function (o) {
              return parseInt(o.postedAt);
            },
          ]).reverse()
        );
      })
      .catch((err) => {});
  }, []);
  return (
    <>
      {tweets == null ? (
        <div style={{ height: "100%", display: "flex" }}>
          <Spinner />
        </div>
      ) : (
        tweets.map((tweet) => {
          return <Tweet tweet={tweet} key={tweet.id} />;
        })
      )}
    </>
  );
}
