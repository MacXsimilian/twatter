import React, { Suspense } from "react";
import styled from "styled-components";
import NewTweet from "../Tweet/NewTweet";
import icons from "../../shared/icons";
import TweetFeed from "./TweetFeed";
import Spinner from "../../shared/Spinner";

const MiddleBarStyle = styled.div`
  grid-area: middleBar;
  display: flex;
  flex-direction: column;
  position: relative;
  border-style: solid;
  width: auto;
  border-width: 0 1.5px;
  border-color: var(--borderColor);

  .home-bar {
    z-index: 1000;

    background-color: var(--background);
    position: sticky;
    top: 0;
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    padding-left: 15px;
    padding-right: 15px;
    height: 53px;
    border-bottom: 1px solid rgb(56, 68, 77);
    border-color: var(--borderColor);
  }

  .home-button {
    font-size: 2rem;
    font-weight: 800;
  }
  .stars-button {
    line-height: 1;
    border-radius: 50%;
    padding: 8px;
  }
  .stars-button svg {
    fill: var(--secondary);
    font-size: 3rem;

    height: 22px;
  }
  .stars-button:hover {
    background-color: var(--secondaryTransparent);
  }
  .middle-bar-container {
    position: relative;
    top: 15px;
  }

  .csiik {
    width: 100%;
    height: 10px;
    background-color: var(--searchBackground);
  }
`;

function MiddleBar() {
  return (
    <MiddleBarStyle>
      <div className="home-bar">
        <div id="middle-bar-home-button" className="home-button">
          Home
        </div>
        <div id="middle-bar-stars-button" className="stars-button">
          {icons.stars}
        </div>
      </div>
      <div className="middle-bar-container">
        <NewTweet />
        <div className="csiik"></div>
        <TweetFeed />
      </div>
    </MiddleBarStyle>
  );
}

export default MiddleBar;
