import React from "react";
import styled from "styled-components";
import LeftSideBar from "./Left-side-bar/LeftSideBar";
import MiddleBar from "./middle-bar/MiddleBar";
import RightSideBar from "./right-side-bar/RightSideBar";

const MainPageStyle = styled.div`
  justify-content: center;
  width: 100%;
  height: 100vh;
  position: absolute;
  display: grid;
  grid-template-columns: minmax(250px, 319px) 600px max(400px);
  grid-template-areas: "leftBar middleBar rightBar";

  @media screen and (max-width: 1275px) {
    grid-template-columns: minmax(90px, auto) max(600px) minmax(auto, auto);
  }

  @media screen and (max-width: 1090px) {
    width: 100%;
  }

  @media screen and (max-width: 1000px) {
    width: 100%;
    grid-template-columns: minmax(85px, auto) minmax(280px, 600px);
  }

  @media screen and (max-width: 600px) {
    grid-template-columns: minmax(85px, auto) minmax(280px, 600px);
  }

  @media screen and (max-width: 440px) {
    grid-template-columns: minmax(70px, auto) minmax(280px, 380px);
  }
`;

function MainPage() {
  return (
    <MainPageStyle>
      <LeftSideBar />
      <MiddleBar />
      <RightSideBar />
    </MainPageStyle>
  );
}

export default MainPage;
