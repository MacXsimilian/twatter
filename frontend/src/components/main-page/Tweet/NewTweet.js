import React, { useContext, useState } from "react";
import styled from "styled-components";
import profilepic from "../../shared/default_profile_200x200.png";
import icons from "../../shared/icons";
import Axios from "axios";
import { TimelineContext } from "../../Context/TimelineContext";

const NewTweetStyle = styled.div`
  width: inherit;
  /*height: 100px;*/
  padding: 0 10px 0 0;

  .tweet-user-picture {
    place-content: center;
    display: flex;
    grid-area: profile-picture;
    position: relative;
  }

  .user-picture {
    height: 40px;
    width: 40px;
    border-radius: 60%;
    place-self: center;
  }

  .tweet-container {
    display: grid;
    grid-template-rows: repeat(1, 50% 1fr);
    grid-template-columns: 13% 87%;
    grid-template-areas:
      "profile-picture input"
      "profile-picture buttons";
    width: 100%;
  }

  input,
  textarea {
    resize: none !important;
    grid-area: input;
    border: 0;
    outline: none;
    background-color: var(--background);
    color: rgb(136, 153, 166);
    resize: horizontal;
    width: 100%;
    height: 50px;
    font-weight: 300;
    font-size: 2.2rem;
  }

  .tweet-buttons-container {
    grid-area: buttons;
    height: 50px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
  }

  .tweet-button-placeholders {
    height: 35px;
    width: 35px;
    cursor: pointer;
    margin: 0 12px 0 0;
    border-radius: 50%;
    padding: 7px;
    :hover {
      background-color: var(--secondaryTransparent);
    }
    @media screen and (max-width: 370px) {
      margin: 0 2px 0 0;
    }
  }

  .tweet-submit-button {
    outline: none;
    width: 73px;
    height: 35px;
    font-weight: 600;
    color: white;
    font-size: 1.6rem;
    display: flex;
    cursor: pointer;
    justify-content: center;
    align-items: center;
    background-color: var(--secondary);
    border: none;
    border-radius: 28px;
  }
  .tweet-buttons {
    display: flex;
    flex-direction: row;
  }
  .tweet-buttons svg {
    fill: var(--secondary);
  }

  #tweet-button-poll {
    @media screen and (max-width: 700px) {
      display: none;
    }
  }
  #tweet-button-schedule {
    @media screen and (max-width: 700px) {
      display: none;
    }
  }
`;

function NewTweet() {
  const [inputContent, setInputContent] = useState("");
  const { tweets, setTweets } = useContext(TimelineContext);

  const postNewTweet = () => {
    if (inputContent === "") return "Empty string!";
    Axios.post(
      process.env.REACT_APP_API_URL +
        ":" +
        process.env.REACT_APP_PORT +
        "/api/tweets",
      { content: inputContent }
    )
      .then((resp) => setTweets([resp.data, ...tweets]))
      .catch((err) => console.log(err));
    setInputContent("");
  };

  return (
    <NewTweetStyle>
      <div className="tweet-container">
        <img className="user-picture" src={profilepic} alt="" />

        <textarea
          value={inputContent}
          id="new-tweet-content-input"
          type="text"
          placeholder="What's happening"
          onChange={(e) => setInputContent(e.target.value)}
        />
        <div className="tweet-buttons-container">
          <div className="tweet-buttons">
            <span className="tweet-button-placeholders" id="tweet-button-pics">
              {" "}
              {icons.pics}{" "}
            </span>
            <span className="tweet-button-placeholders" id="tweet-button-gif">
              {" "}
              {icons.gif}{" "}
            </span>
            <span className="tweet-button-placeholders" id="tweet-button-poll">
              {" "}
              {icons.poll}{" "}
            </span>
            <span className="tweet-button-placeholders" id="tweet-button-emoji">
              {" "}
              {icons.emoji}{" "}
            </span>
            <span
              className="tweet-button-placeholders"
              id="tweet-button-schedule"
            >
              {" "}
              {icons.schedule}
            </span>
          </div>
          <button className="tweet-submit-button" onClick={postNewTweet}>
            Tweet
          </button>
        </div>
      </div>
    </NewTweetStyle>
  );
}

export default NewTweet;
