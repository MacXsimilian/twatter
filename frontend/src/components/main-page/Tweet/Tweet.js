import React from "react";
import styled from "styled-components";
import profilepic from "../../shared/default_profile_200x200.png";
import icons from "../../shared/icons";
import {
  getDefaultNormalizer,
  getAllByPlaceholderText,
} from "@testing-library/react";
function getPostedTime(postedAt) {
  const monthNames = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  postedAt = parseInt(postedAt);
  const secdiff =
    Math.floor(new Date().getTime() - new Date(postedAt * 1000)) / 1000;

  if (secdiff <= 10) {
    return "now";
  }
  if (secdiff < 60) {
    return Math.floor(secdiff) + "s";
  }
  if (secdiff < 3600) {
    return Math.floor(secdiff / 60) + "m";
  }
  if (secdiff < 86400) {
    return Math.floor(secdiff / 60 / 60) + "h";
  }
  const postedDate = new Date(postedAt * 1000);
  return monthNames[postedDate.getMonth()] + " " + postedDate.getDate();
}
const TwtStyle = styled.div`
  font-size: 1.8rem;
  cursor: pointer;
  padding-top: 6px;

  img {
    height: 40px;
    width: 40px;
    border-radius: 60%;
  }

  :hover {
    background-color: rgb(125 134 146 / 3%);
    transition: 0.24s;
  }
`;

const TweetHeaderStyle = styled.div`
  flex-direction: row;
  display: flex;
  padding: 10px 10px 10px 0;
  align-self: center;
  width: 100%;
  justify-content: space-between;
  .tweet-arrow {
    box-sizing: content-box;
    height: 19px;
    width: 19px;
    border-radius: 50%;
    padding: 5px;
    :hover {
      background-color: var(--secondaryTransparent);
    }
  }
  .header {
    margin-left: 10px;
  }
  .username {
    font-weight: bold;
    :hover {
      text-decoration: underline;
    }
  }
`;

const TweetBodyStyle = styled.div`
  display: flex;
  word-break: break-word;
  white-space: pre-wrap;
  padding: 0 33px 0 18px;
  margin-bottom: 10px;
`;

const TweetFooterStyle = styled.div`
  display: flex;
  justify-content: space-evenly;
  border-bottom: 1px solid rgb(56, 68, 77);
  div {
    width: 35px;
    height: 35px;
    cursor: pointer;
    margin: 0 12px 0 0;
    border-radius: 50%;
    padding: 7px;

    span {
      position: absolute;
      margin-left: 7px;
    }
  }

  .comments,
  .share {
    :hover {
      background-color: rgba(29, 161, 242, 0.1);
      fill: rgb(29, 161, 242);
      color: rgb(29, 161, 242);
    }
  }
  .retweets {
    :hover {
      background-color: rgba(21, 172, 89, 0.1);
      fill: rgb(23, 191, 99);
      color: rgb(23, 191, 99);
    }
  }
  .likes {
    :hover {
      background-color: rgba(224, 36, 94, 0.1);
      fill: rgb(224, 36, 94);
      color: rgb(224, 36, 94);
    }
  }
`;

const ProfilePic = styled.div``;

export default function Tweet(props) {
  const { content, id, postedAt, userId, username } = props.tweet;
  return (
    <TwtStyle>
      <div style={{ display: "flex", margin: "16px" }}>
        <ProfilePic
          onClick={() => {
            window.location.href = "/profile/" + username;
          }}
          className="profilepic"
        >
          <img src={profilepic} alt="" />
        </ProfilePic>
        <TweetHeaderStyle>
          <div className="header">
            <span
              onClick={() => {
                window.location.href = "/profile/" + username;
              }}
              id={id + "-tweet-username"}
              className="username"
            >
              {username}
            </span>{" "}
            @{username} ·{" "}
            <span id={id + "-tweet-date"} className="date">
              {getPostedTime(postedAt)}
            </span>
          </div>
          <div id={id + "-tweet-arrow"} className="tweet-arrow">
            {icons.downArrow}
          </div>
        </TweetHeaderStyle>
      </div>
      <TweetBodyStyle id={id + "-tweet-body"}>{content}</TweetBodyStyle>
      <TweetFooterStyle>
        <div id={id + "-tweet-comment-button"} className="comments">
          {icons.replies}
          <span>0</span>
        </div>
        <div id={id + "-tweet-retweet-button"} className="retweets">
          {icons.retweets}
          <span>0</span>
        </div>
        <div id={id + "-tweet-like-button"} className="likes">
          {icons.likes}
          <span>0</span>
        </div>
        <div id={id + "-tweet-share-button"} className="share">
          {icons.shareTweet}
        </div>
      </TweetFooterStyle>
    </TwtStyle>
  );
}
