import React, { useContext } from "react";
import styled from "styled-components";

import profilepic from "../../shared/default_profile_200x200.png";
import { UserContext } from "components/Context/UserContext";
import icons from "components/shared/icons";
import { AuthContext } from "components/Context/AuthContext";
import { ModalContext } from "components/Context/ModalConext";

const LeftBottomPanelStyle = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  border-radius: 28px;
  height: 50px;
  cursor: pointer;
  align-items: center;
  transition: background-color 0.24s ease;

  @media screen and (max-width: 600px) {
    align-self: center;
  }

  :hover {
    background-color: var(--secondaryTransparent);
  }

  img {
    height: 40px;
    width: 40px;
    border-radius: 60%;
  }

  svg {
    fill: white;
    height: 20px;
    width: 20px;
  }
  .qr-icon {
    @media screen and (min-width: 1100px) {
      display: none;
    }

    cursor: pointer;
    padding: 6px;
    border-radius: 1rem;
    :hover {
      background-color: var(--secondaryTransparent);
    }
    svg {
      height: 20px;
      width: 20px;
    }
  }
`;

const UserNameStyle = styled.div`
  display: flex;
  flex-direction: column;
  font-weight: 700;
  font-size: 1.3rem;
  align-items: center;
  .email {
    color: rgb(136, 153, 166);
    font-weight: 500;
  }

  @media screen and (max-width: 1275px) {
    display: none;
  }
`;

const Logout = styled.div`
  svg {
    fill: var(--foreground);
  }
  padding: 9px;
  border-radius: 50%;

  @media screen and (max-width: 1275px) {
    display: none;
  }
`;

export default function LeftBottomPanel() {
  const { user } = useContext(UserContext);
  const { logout } = useContext(AuthContext);
  const { openModal } = useContext(ModalContext);
  return (
    <LeftBottomPanelStyle>
      <div className="qr-icon" onClick={() => openModal("qr")}>
        {icons.qr}
      </div>
      <img src={profilepic} alt="" />
      <UserNameStyle id="bottom-bar-username">
        <div>{user.username}</div>
        <div className="email">@{user.username}</div>
      </UserNameStyle>
      <Logout id="bottom-bar-logout" className="logout-button" onClick={logout}>
        {icons.logout}
      </Logout>
    </LeftBottomPanelStyle>
  );
}
