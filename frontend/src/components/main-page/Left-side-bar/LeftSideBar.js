import React from "react";
import styled from "styled-components";

import LeftUpperPanel from "./LeftUpperPanel";
import LeftBottomPanel from "./LeftBottomPanel";

const LeftSideBarStyle = styled.div`
  display: flex;
  grid-area: leftBar;
  flex-direction: column;
  -moz-box-pack: justify;
  justify-content: space-between;
  align-items: flex-end;
  height: 100vh;

  .left-bar-scroll {
    width: 280px;
    height: 100vh;
    overflow-y: auto;
    position: fixed;
    overflow-x: hidden;

    @media screen and (max-width: 1275px) {
      width: auto;
    }
  }

  .left-bar-container {
    width: max-content;
    height: 100vh;
    justify-content: space-between;
    display: flex;
    flex-direction: column;

    @media screen and (max-width: 1275px) {
      margin: 0 12px;
    }

    @media screen and (max-width: 700px) {
      margin: 0 6px;
    }

    @media screen and (max-width: 400px) {
      margin: 0 0;
    }
  }
`;

function LeftSideBar() {
  return (
    <LeftSideBarStyle>
      <div className="left-bar-scroll">
        <div className="left-bar-container">
          <LeftUpperPanel /> <LeftBottomPanel />
        </div>
      </div>
    </LeftSideBarStyle>
  );
}

export default LeftSideBar;
