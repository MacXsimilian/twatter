import React, { useContext } from "react";
import styled from "styled-components";
import TwatterLogo from "components/shared/TwatterLogo";
import icons from "../../shared/icons";
import { Link } from "react-router-dom";
import { UserContext } from "../../Context/UserContext";
import { ModalContext } from "components/Context/ModalConext";
const LeftUpperPanelStyle = styled.div`
  grid-area: leftBar;
  align-self: baseline;
  /*on firefox it needs*/
  /*margin: 0 12px;*/
  margin-bottom: 30px;
  width: max-content;

  @media screen and (max-width: 700px) {
    align-self: center;
  }

  @media screen and (max-width: 600px) {
    margin-left: 6px;
    margin-right: 6px;
  }

  div {
    padding: 10px;
    border-radius: 28px;
  }

  svg {
    margin-right: 15px;
    height: 27px;
    width: 27px;

    @media screen and (max-width: 1275px) {
      margin-right: 0;
    }
  }

  .desktop-version {
    @media screen and (max-width: 1275px) {
      display: none;
    }
  }

  .tablet-mobile-version {
    @media screen and (min-width: 1275px) {
      display: none;
      margin-right: 0;
    }
  }
`;

const MenuStyle = styled(Link)`
  padding: 10px;
  border-radius: 28px;
  text-decoration: none;
  width: max-content;
  font-weight: bold;
  color: var(--getForeground);
  fill: var(--getForeground);

  font-size: 1.8rem;
  display: flex;
  cursor: pointer;
  align-items: center;
  :hover {
    background-color: var(--secondaryTransparent);
    fill: var(--secondary);
    color: var(--secondary);
    transition: 0.24s;
  }
  @media screen and (max-width: 1275px) {
    width: 55px;
    padding: 15px;
  }

  @media screen and (min-width: 1275px) {
    margin: 8px;
  }

  @media screen and (max-width: 700px) {
    align-self: center;
  }

  @media screen and (max-width: 600px) {
    margin: 0px 0px;
  }

  :active {
  }

  .paint {
    position: absolute;
    fill: var(--secondary);
    width: inherit;
    height: inherit;
  }
`;

const TweetButtonStyle = styled.div`
  margin: auto;
  font-weight: bold;
  color: white;
  font-size: 1.8rem;
  display: flex;
  cursor: pointer;
  justify-content: center;
  align-items: center;
  background-color: var(--secondary);
  border-radius: 28px;
  :hover {
    transition: 0.24s;
  }
`;

const ActiveMenu = styled(MenuStyle)`
  fill: var(--secondary);
  color: var(--secondary);
`;
const isThisActive = (menu) => {
  return window.location.pathname.includes(menu.toLowerCase());
};

export default function LeftUpperPanel() {
  const { user } = useContext(UserContext);

  const Menu = [
    {
      title: "Home",
      icon: icons.home,
      filledIcon: icons.homeFilled,
      to: "/home",
    },
    {
      title: "Explore",
      icon: icons.explore,
      filledIcon: icons.exploreFilled,
      to: "#",
    },
    {
      title: "Notifications",
      icon: icons.notifications,
      filledIcon: icons.notificationsFilled,
      to: "#",
    },
    {
      title: "Messages",
      icon: icons.messages,
      filledIcon: icons.messagesFilled,
      to: "/messages",
    },
    {
      title: "Bookmarks",
      icon: icons.bookmarks,
      filledIcon: icons.bookmarksFilled,
      to: "#",
    },
    {
      title: "Lists",
      icon: icons.lists,
      filledIcon: icons.listsFilled,
      to: "#",
    },
    {
      title: "Profile",
      icon: icons.profile,
      filledIcon: icons.profileFilled,
      to: `/profile/${JSON.parse(localStorage.getItem("user")).username}`,
    },
  ];
  const { modal, setModal, openModal } = useContext(ModalContext);

  return (
    <LeftUpperPanelStyle>
      <MenuStyle className="desktop-version" id="twatter-logo" to="#">
        <TwatterLogo width="28px" heigth="28px" />
      </MenuStyle>
      <MenuStyle className="tablet-mobile-version" id="twatter-logo" to="#">
        <TwatterLogo width="28px" heigth="28px" />
      </MenuStyle>
      {Menu.map((menuTitle) => {
        if (isThisActive(menuTitle.title)) {
          return (
            <React.Fragment>
              <ActiveMenu
                id={"menu-" + menuTitle.title.toLowerCase()}
                className="desktop-version"
              >
                {menuTitle.filledIcon} {menuTitle.title}{" "}
              </ActiveMenu>
              <ActiveMenu
                id={"menu-" + menuTitle.title.toLowerCase()}
                className="tablet-mobile-version"
              >
                {menuTitle.filledIcon}
              </ActiveMenu>
            </React.Fragment>
          );
        } else {
          return (
            <React.Fragment>
              <MenuStyle
                id={"menu-" + menuTitle.title.toLowerCase()}
                className="desktop-version"
                to={menuTitle.to}
              >
                {" "}
                {menuTitle.icon} {menuTitle.title}
              </MenuStyle>
              <MenuStyle
                id={"menu-" + menuTitle.title.toLowerCase()}
                className="tablet-mobile-version"
                to={menuTitle.to}
              >
                {" "}
                {menuTitle.icon}
              </MenuStyle>
            </React.Fragment>
          );
        }
      })}

      <MenuStyle
        className="desktop-version"
        onClick={() => openModal("displaySettings")}
      >
        {icons.displaySetting}
        <span className="paint">{icons.paint}</span>
        Display
      </MenuStyle>
      <MenuStyle
        className="tablet-mobile-version"
        onClick={() => openModal("displaySettings")}
      >
        <span>{icons.displaySetting}</span>
        <span className="paint">{icons.paint}</span>
      </MenuStyle>
      <TweetButtonStyle className="desktop-version">Tweet</TweetButtonStyle>
      <TweetButtonStyle className="tablet-mobile-version">
        {" "}
        {icons.tweet}
      </TweetButtonStyle>
    </LeftUpperPanelStyle>
  );
}
