import React, { useContext } from "react";
import Rooms from "./ChatRooms/Rooms";
import Messages from "./Messages/Messages";
import LeftSideBar from "components/main-page/Left-side-bar/LeftSideBar";
import styled from "styled-components";
import { SuspenseWithPerf } from "reactfire";
import Spinner from "components/shared/Spinner";
import ChatContextProvider, {
  ChatContext,
} from "components/Context/ChatContext";

const ChatStyle = styled.div`
  width: 1255px;
  height: 100vh;
  position: absolute;
  left: 50%;
  transform: translateX(-50%);
  display: grid;
  grid-template-columns: 275px;
  grid-template-areas: "leftBar middleBar rightBar";
  .chat {
    display: flex;
    width: 900px;
    @media screen and (max-width: 1090px) {
      width: auto;
      flex-direction: column;

      border-left: 1px solid var(--borderColor);
      border-right: 1px solid var(--borderColor);
    }
  }
`;
function Chat() {
  const { chat } = useContext(ChatContext);
  return (
    <ChatStyle>
      <LeftSideBar />
      <div className="chat">
        <SuspenseWithPerf fallback={<Spinner />}>
          <Rooms />
        </SuspenseWithPerf>

        <SuspenseWithPerf fallback={<Spinner />}>
          {chat.partnerUsername && <Messages />}
        </SuspenseWithPerf>
      </div>
    </ChatStyle>
  );
}

export default Chat;
