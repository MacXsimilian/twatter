import React, { useContext } from "react";
import NewChatModal from "./NewChatModal";
import { ChatContext } from "components/Context/ChatContext";

function AddChatUser() {
  const { chat } = useContext(ChatContext);
  const chatData = {
    id: chat.roomId,
    users: [
      ...chat.partnerUsername.split(",").map((uname) => {
        return {
          name: uname,
        };
      }),
    ],
  };
  console.log(chatData);
  return <NewChatModal chatData={chatData} />;
}

export default AddChatUser;
