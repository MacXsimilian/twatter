import React, { useContext, useState, useCallback, useRef } from "react";
import styled from "styled-components";
import { ModalContext } from "components/Context/ModalConext";
import icons from "../shared/icons";
import Axios from "axios";
import debounce from "lodash.debounce";
import profilepic from "../shared/default_profile_200x200.png";
import Spinner from "components/shared/Spinner";
import { UserContext } from "components/Context/UserContext";
import { useFirestore } from "reactfire";
import _, { reduce } from "lodash";
import firebase from "firebase";

const SearchBarStyle = styled.div`
  position: relative;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 42px;
  background-color: var(--offSetBackground);
  border-color: rgba(0, 0, 0, 0);
  border-style: solid;
  border-width: 1px;
  margin: 5px 5px 15px 0;

  i {
    color: rgb(136, 153, 166);
    font-size: 25px;
    height: 25px;
    margin: 17px;
  }

  .search-input {
    font-size: 1.5rem;
    font-weight: 100;
    font-family: Arial, serif;
    background-color: rgb(0, 0, 0, 0);
    border: none;
    width: 100%;
    height: 100%;
    color: var(--foreground);
    outline: none;
  }
  .search-input::placeholder {
    color: rgb(175, 175, 175);
  }

  .icon svg {
    padding-left: 15px;
    padding-right: 15px;
    height: 23px;
    fill: var(--secondary);
  }

  .clear-input-button {
    display: flex;
    justify-content: center;
    align-items: center;
    position: absolute;
    right: 10px;
    top: 50%;
    border-radius: 60%;
    transform: translateY(-50%);
    height: 23px;
    width: 23px;
    background-color: var(--secondary);
    transition: opacity 0.24s ease;
    svg {
      width: 18px;
      height: 18px;
      fill: var(--background);
    }
    :hover {
      cursor: pointer;
      opacity: 0.8;
    }
  }
`;
const SearchResults = styled.div`
  display: flex;
  flex-direction: column;
  height: 480px;
  overflow-x: auto;

  .offers {
    border-bottom: 1px solid rgb(56, 68, 77);
    text-decoration: none;
    height: 50px;
    background-color: var(--background);
    color: var(--foreground);
    font-size: 15px;
    font-weight: bold;
    display: flex;
    justify-content: start;
    align-items: center;
    padding: 15px;
    border-radius: 2px;
  }

  .people-offers {
    cursor: pointer;
    border-bottom: 1px solid rgb(56, 68, 77);
    text-decoration: none;
    height: 60px;
    background-color: var(--background);
    display: grid;
    padding: 10px 15px;
    grid-template-columns: 40px 260px;
    :hover {
      background-color: var(--offSetBackground);
    }
  }
  .people-offers-names {
    color: var(--foreground);
    font-weight: bold;
    font-size: 1.6rem;
    font-family: system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI",
      Roboto, Ubuntu, "Helvetica Neue", sans-serif;
  }

  .people-offers-emails {
    color: rgb(136, 153, 166);
    font-size: 1.3rem;
    font-family: system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI",
      Roboto, Ubuntu, "Helvetica Neue", sans-serif;
  }

  img {
    height: 40px;
    width: 40px;
    border-radius: 60%;
  }

  .people-offers-text-containers {
    display: flex;
    justify-content: center;
    flex-direction: column;
    position: relative;
    left: 10px;
  }
  .spinner {
    display: flex;
    justify-content: center;
    height: 100px;
    background-color: var(--background);
    font-size: 14px;
    font-weight: bold;
    padding: 20px 10px;
  }
  .search-warning {
    font-size: 1.5rem;
    color: grey;
    text-align: center;
  }
  .no-result {
    color: red;
    font-size: 1.5rem;
    text-align: center;
  }
`;
const ChosenMembers = styled.div`
  width: 100%;
  display: flex;
  overflow-y: auto;
  height: 40px;
  padding: 0 2rem;
  .member {
    display: flex;
    padding: 4px 7px;
    background-color: var(--secondary);
    border-radius: 100vw;
    align-self: center;
    font-size: 1.2rem;
    margin-right: 1rem;
  }
  .delete-member {
    place-content: center;
    display: flex;
    cursor: pointer;
    margin-right: 0.7rem;
    height: 1.7rem;
    width: 1.7rem;
    background-color: var(--secondary);
    border-radius: 50%;
    border: 1px solid rgba(0, 0, 0, 0.3);
    :hover {
      background-color: rgba(0, 0, 0, 0.2);
    }
  }
  .delete-member svg {
    fill: white;
    width: 1rem;
  }
`;
function SearchBar(props) {
  const [inputContent, setInputContent] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [offers, setOffers] = useState([]);
  const { modal, setModal, closeModal } = useContext(ModalContext);
  const sendPostRequest = (nextValue) => {
    setIsLoading(true);
    Axios.post(
      process.env.REACT_APP_API_URL +
        ":" +
        process.env.REACT_APP_PORT +
        "/api/search/post",
      { query: nextValue }
    )
      .then((resp) => {
        if (resp.data.length < 1) {
          setOffers([]);
        } else {
          setOffers(resp.data);
        }
        setIsLoading(false);
      })
      .catch((err) => {
        setIsLoading(false);
        console.log(err);
      });
  };

  const debouncedSave = useCallback(
    debounce((nextValue) => sendPostRequest(nextValue), 300),
    []
  );

  const clearInputField = (e) => {
    setInputContent("");
    e.currentTarget.previousElementSibling.value = "";
    setOffers([]);
    e.currentTarget.previousElementSibling.focus();
  };

  const onEnterPress = (e) => {
    if (e.key === "Enter") {
      console.log(e.target.value);
    }
  };

  const onChange = (e) => {
    setInputContent(e.target.value);
    if (e.target.value.length >= 3) {
      debouncedSave(e.target.value);
    } else {
      setOffers([]);
    }
  };

  const { chosenMembers, setChosenMembers } = props;
  const addToChat = (offer) => {
    if (!_.includes(chosenMembers, offer)) {
      setChosenMembers([...chosenMembers, offer]);
    }
    console.log(chosenMembers);
  };
  return (
    <>
      <SearchBarStyle>
        <div className="icon">{icons.search}</div>
        <input
          autoFocus
          autocomplete="off"
          id="searchbar-input"
          onKeyDown={onEnterPress}
          onChange={onChange}
          className="search-input"
          type="text"
          placeholder="Search Twatter"
        />
        {inputContent !== "" && (
          <div className="clear-input-button" onClick={clearInputField}>
            {icons.close}
          </div>
        )}
      </SearchBarStyle>
      <ChosenMembers>
        {chosenMembers.map((member) => {
          return (
            <div className="member">
              {!props.chatData && (
                <div
                  onClick={() =>
                    setChosenMembers(
                      _.filter(chosenMembers, function (o) {
                        return o.name !== member.name;
                      })
                    )
                  }
                  className="delete-member"
                >
                  {icons.close}
                </div>
              )}
              {member.name}
            </div>
          );
        })}
      </ChosenMembers>
      <SearchResults>
        {isLoading && (
          <div className="spinner">
            <Spinner />
          </div>
        )}
        {inputContent.length < 3 && (
          <div className="search-warning">At least 3 characters</div>
        )}
        {!isLoading && offers.length === 0 && inputContent.length >= 3 && (
          <div className="no-result">Sorry didn't find anyone</div>
        )}

        {offers.map((offer) => {
          return (
            <div onClick={() => addToChat(offer)} className="people-offers">
              <img src={profilepic} alt="" />
              <div className="people-offers-text-containers">
                <div className="people-offers-names">{offer.name}</div>
                <div className="people-offers-emails">@{offer.name}</div>
              </div>
            </div>
          );
        })}
      </SearchResults>
    </>
  );
}

const NewChatModalStyled = styled.div`
  @media screen and (max-width: 772px) {
    height: 100vh;
    width: 100vw;
    border-radius: 0;
  }
  position: fixed;
  z-index: 2000;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  width: 600px;
  height: 660px;
  background-color: var(--background);
  border-radius: 4rem;

  .header {
    justify-content: space-between;
    display: flex;
    width: 100%;
    height: 50px;
    margin-top: 1rem;
    border-bottom: 1px solid var(--borderColor);
  }
  .title {
    font-size: 2rem;
    align-self: center;
  }
  .close {
    align-self: center;
    cursor: pointer;
    margin-left: 1rem;
    border-radius: 50%;
    padding: 5px;
    width: 35px;
    height: 35px;
    min-width: 30px;
    min-height: 30px;
    :hover {
      background-color: var(--secondaryTransparent);
    }
  }
  .close svg {
    fill: var(--secondary);
  }
  .start-chat {
    font-weight: bold;
    display: flex;
    place-items: center;
    margin-right: 2rem;
    align-self: center;
    font-size: 1.5rem;
    cursor: pointer;
    height: 30px;
    width: fit-content;
    padding: 6px 12px;
    color: white;
    background-color: var(--secondary);
    border-radius: 100vw;
    text-align: center;

    :hover {
      opacity: 0.9;
    }
  }
`;
let FadeBackground = styled.div`
  position: fixed;
  background-color: rgba(110, 118, 125, 0.4);
  width: 100vw;
  height: 100vh;
  z-index: 1500;
`;
function NewChatModal(props) {
  const { chatData } = props;
  console.log("chatdata");
  console.log(chatData);
  const [chosenMembers, setChosenMembers] = useState(
    chatData ? chatData.users : []
  );
  const { user } = useContext(UserContext);

  const { modal, setModal, closeModals } = useContext(ModalContext);

  const newChat = (members) => {
    const names = members.map((member) => member.name);

    const Typing = names.reduce((o, k) => _.set(o, k, false), {});
    return {
      lastMsg: "",
      lastMsgDate: 0,
      messages: [],
      users: [...names, user.username],
      typing: Typing,
    };
  };
  const chatRef = useFirestore().collection("chats");

  const createNewChat = (members) => {
    if (chatData) {
      chatRef.doc(chatData.id).update({
        users: [
          user.username,
          ...chosenMembers.map((user) => {
            return user.name;
          }),
        ],
        messages: firebase.firestore.FieldValue.arrayUnion({
          content: `${user.username} added new user(s) to the chat`,
          sender: user.username,
          timestamp: Math.floor(new Date().getTime() / 1000),
        }),
      });
    } else {
      if (!_.isEmpty(members)) {
        const newChatJson = newChat(members);
        chatRef.add(newChatJson);
      }
    }
  };

  return (
    <>
      <FadeBackground
        onClick={() => {
          closeModals(["addChatUser", "newChat"]);
        }}
      />
      <NewChatModalStyled>
        <div className="header">
          <span
            onClick={() => {
              closeModals(["addChatUser", "newChat"]);
            }}
            className="close"
          >
            {icons.close}
          </span>{" "}
          <div className="title">
            {chatData ? " Add New User" : "Create New Chat"}
          </div>{" "}
          <div
            onClick={() => {
              createNewChat(chosenMembers);
              closeModals(["addChatUser", "newChat"]);
            }}
            className="start-chat"
          >
            {chatData ? "Update Chat" : "Start Chat"}
          </div>
        </div>
        <div className="content">
          <SearchBar
            chatData={chatData}
            chosenMembers={chosenMembers}
            setChosenMembers={setChosenMembers}
          />
        </div>
      </NewChatModalStyled>
    </>
  );
}

export default NewChatModal;
