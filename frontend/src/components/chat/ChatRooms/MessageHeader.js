import React, { useContext } from "react";
import icons from "../../shared/icons";
import styled from "styled-components";
import { ModalContext } from "components/Context/ModalConext";

const HeaderStyle = styled.div`
  height: 40px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-bottom: 1px solid var(--borderColor);
  padding-left: 10px;
  padding-right: 10px;
  .new-chat svg {
    cursor: pointer;
    fill: var(--secondary);
    height: 19px;
    width: 19px;
  }
  .new-chat {
    border-radius: 50%;
    padding: 5px;
    :hover {
      background-color: var(--secondaryTransparent);
    }
  }
  p {
    font-size: 1.5rem;
    font-weight: bold;
  }
`;

function MessageHeader() {
  const { modal, setModal, openModal } = useContext(ModalContext);

  return (
    <HeaderStyle>
      <p>Messages</p>{" "}
      <span className="new-chat" onClick={() => openModal("newChat")}>
        {icons.newChat}
      </span>
    </HeaderStyle>
  );
}

export default MessageHeader;
