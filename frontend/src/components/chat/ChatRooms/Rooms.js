import React, { useContext, useState, useEffect } from "react";
import Room from "./Room";
import MessageHeader from "./MessageHeader";
import styled from "styled-components";
import { useFirestore, useFirestoreDoc } from "reactfire";
import { UserContext } from "components/Context/UserContext";
import _ from "lodash";

const RoomsStyle = styled.div`
  width: 570px;
  height: 100vh;
  border: 1px solid var(--borderColor);
  @media screen and (max-width: 1090px) {
    width: auto;
    height: auto;
    max-height: 300px;
    overflow-y: auto;
  }
`;
function Rooms() {
  const chatsRef = useFirestore().collection("chats");
  const { user } = useContext(UserContext);
  const [rooms, setRooms] = useState(null);

  let reactfireRoomsData = useFirestoreDoc(
    chatsRef.where("users", "array-contains", user.username)
  );
  useEffect(() => {
    let roomsData = [];

    reactfireRoomsData.forEach((room) => {
      const roomData = { ...room.data(), id: room.id };
      roomsData.push(roomData);
    });
    setRooms(
      _.sortBy(roomsData, [
        function (o) {
          return parseInt(o.lastMsgDate);
        },
      ]).reverse()
    );
  }, [reactfireRoomsData]);

  // useEffect(() => {
  //   chatsRef
  //     .where("users", "array-contains", user.username)
  //     .get()
  //     .then(function (querySnapshot) {
  //       let roomsData = [];
  //       querySnapshot.forEach(function (doc) {
  //         roomsData.push(doc.data());
  //       });
  //       console.log(roomsData);
  //       setRooms(roomsData);
  //     })
  //     .catch(function (error) {
  //       console.log("Error getting documents: ", error);
  //     });
  // }, []);

  return (
    <RoomsStyle>
      <MessageHeader />
      {rooms &&
        rooms.map((room) => {
          return (
            <Room
              id={room.id}
              messages={room.messages}
              lastMsg={room.lastMsg}
              lastMsgDate={room.lastMsgDate || 0}
              username={_.filter(room.users, function (o) {
                return o !== user.username;
              }).join(",")}
            />
          );
        })}
    </RoomsStyle>
  );
}

export default React.memo(Rooms);
