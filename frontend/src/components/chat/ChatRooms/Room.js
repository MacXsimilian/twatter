import React, { useContext } from "react";
import profilepic from "../../shared/defaultprofilepic.jpg";
import styled from "styled-components";
import { ChatContext } from "components/Context/ChatContext";

const RoomStyle = styled.div`
  align-items: center;
  height: 60px;
  display: flex;
  flex-direction: row;
  padding: 0 0 0 5px;
  border-bottom: 1px solid var(--borderColor);
  border-right: ${({ username, partnerUsername }) =>
    partnerUsername === username ? "2px solid var(--secondary)" : ""};

  cursor: pointer;

  :hover {
    background-color: rgba(0, 0, 0, 0.1);
  }
  .details {
    font-size: 1.5rem;
    margin-left: 8px;
    flex-direction: column;
    width: 100%;
  }
  .profile-pic img {
    width: 4rem;
    height: 4rem;
    border-radius: 50%;
  }
  .header {
    display: flex;
    justify-content: space-between;
  }
  .last-msg-date {
    margin-right: ${({ username, partnerUsername }) =>
      partnerUsername === username ? "calc(2rem - 2px)" : "2rem"};
    font-size: 1.2rem;
  }
  .name {
    font-weight: bold;
    font-size: 1.2rem;
    margin-right: 5px;
  }
  .username {
    font-size: 1.3rem;
    color: var(--secondary);
  }
  .last-msg {
    font-size: 1.25rem;
    text-overflow: ellipsis;
    max-width: 270px;
    white-space: nowrap;
    overflow: hidden;
  }
`;

const getLastMsgDate = (postedAt) => {
  const monthNames = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];
  postedAt = parseInt(postedAt);
  const secdiff = Math.floor(
    (new Date().getTime() - new Date(postedAt * 1000)) / 1000
  );
  if (postedAt === 0) {
    return "";
  }
  if (secdiff <= 10) {
    return "now";
  }
  if (secdiff < 60) {
    return secdiff + "s";
  }
  if (secdiff < 3600) {
    return Math.floor(secdiff / 60) + "m";
  }
  if (secdiff < 86400) {
    return Math.floor(secdiff / 60 / 60) + "h";
  }
  const postedDate = new Date(postedAt * 1000);
  return monthNames[postedDate.getMonth()] + " " + postedDate.getDate();
};
function Room(props) {
  const { username, lastMsgDate, lastMsg, id } = props;
  const { chat, setChat } = useContext(ChatContext);
  return (
    <RoomStyle
      partnerUsername={chat.partnerUsername}
      username={username}
      onClick={() => setChat({ partnerUsername: username, roomId: id })}
    >
      <div className="profile-pic">
        <img src={profilepic} />
      </div>
      <div className="details">
        <div className="header">
          <div className="user-details">
            <span className="name">{username}</span>
            <span className="username">
              {username.split(",").length > 1 ? "" : "@" + username}
            </span>
          </div>
          <div className="last-msg-date">{getLastMsgDate(lastMsgDate)}</div>
        </div>
        <div className="last-msg">{lastMsg}</div>
      </div>
    </RoomStyle>
  );
}

export default Room;
