import React from "react";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import styled from "styled-components";
import data from "emoji-mart/data/twitter.json";
import { NimblePicker } from "emoji-mart";

const EmojiPickerStyle = styled.div`
  .emoji-mart-scroll {
    /* width */
    ::-webkit-scrollbar {
      width: 10px;
    }

    /* Track */
    ::-webkit-scrollbar-track {
      background: var(--secondaryTransparent);
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
      background: var(--secondary);
      border-radius: 100vh;
    }

    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
      background: var(--secondaryHover);
    }
  }
  .emoji-mart-anchor-selected {
    color: var(--secondary) !important;
    span {
      background-color: var(--secondary) !important;
    }
  }
  .emoji-mart-search {
    :focus-within {
      input {
        border: 1px solid var(--secondary);
      }
      .emoji-mart-search-icon svg {
        fill: var(--secondary) !important;
      }
    }
  }
`;
function EmojiPickerComponent({
  openEmojiPicker,
  setOpenEmojiPicker,
  setNewMessage,
}) {
  const onEmojiClick = (emojiObject) => {
    insertAtCaret("new-message", emojiObject.native);
  };

  function insertAtCaret(areaId, text) {
    var txtarea = document.getElementById(areaId);
    var scrollPos = txtarea.scrollTop;
    var strPos = 0;
    var br =
      txtarea.selectionStart || txtarea.selectionStart == "0"
        ? "ff"
        : document.selection
        ? "ie"
        : false;
    if (br == "ie") {
      txtarea.focus();
      var range = document.selection.createRange();
      range.moveStart("character", -txtarea.value.length);
      strPos = range.text.length;
    } else if (br == "ff") strPos = txtarea.selectionStart;

    var front = txtarea.value.substring(0, strPos);
    var back = txtarea.value.substring(strPos, txtarea.value.length);
    txtarea.value = front + text + back;
    strPos = strPos + text.length;
    if (br == "ie") {
      txtarea.focus();
      var range = document.selection.createRange();
      range.moveStart("character", -txtarea.value.length);
      range.moveStart("character", strPos);
      range.moveEnd("character", 0);
      range.select();
    } else if (br == "ff") {
      txtarea.selectionStart = strPos;
      txtarea.selectionEnd = strPos;
      txtarea.focus();
    }
    txtarea.scrollTop = scrollPos;
    setNewMessage(txtarea.value);
  }
  return (
    <>
      {openEmojiPicker && (
        <ClickAwayListener onClickAway={() => setOpenEmojiPicker(false)}>
          <EmojiPickerStyle>
            <NimblePicker
              title="Twatter"
              onSelect={(emoji) => onEmojiClick(emoji)}
              style={{
                position: "absolute",
                bottom: "50px",
                width: "23%",
                right: "10%",
              }}
              theme="auto"
              set="twitter"
              data={data}
            />
          </EmojiPickerStyle>
        </ClickAwayListener>
      )}
    </>
  );
}

export default EmojiPickerComponent;
