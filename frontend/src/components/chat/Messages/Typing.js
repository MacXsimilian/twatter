import React from "react";
import styled from "styled-components";
import Message from "./Message";
import defaultPic from "../../shared/default_profile_200x200.png";

import TypingAnimation from "./TypingAnimation";

const MessageStyle = styled.div`
  margin-top: 5px;
  display: flex;
  flex-direction: row;
  .username {
    left: 1rem;
    position: absolute;
    bottom: -5px;
    color: #909090;
  }
  .container {
    display: flex;
    flex-direction: column;
    align-items: center;
    position: relative;
  }
`;
const ProfilePicture = styled.img`
  width: 3.5rem;
  height: 3.5rem;
  border-radius: 50%;
  place-self: flex-end;
`;
function Typing(props) {
  return (
    <MessageStyle>
      <ProfilePicture src={defaultPic} />
      <div className="container">
        <TypingAnimation />
        <div className="username">{props.member}</div>
      </div>
    </MessageStyle>
  );
}

export default Typing;
