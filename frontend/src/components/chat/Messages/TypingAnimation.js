import React from "react";
import styled from "styled-components";

const Animation = styled.div`
  display: flex;
  flex-direction: row;
  align-self: flex-start;
  margin-bottom: 2px;
  background-color: var(--offSetBackground);
  width: fit-content;
  padding: 5px 10px;
  border-radius: 1.25rem;
  border-bottom-left-radius: 0;
  align-self: center;
  left: 1rem;
  position: relative;

  .spinner {
  }

  .spinner > div {
    width: 15px;
    height: 15px;
    background-color: var(--secondary);

    border-radius: 100%;
    display: inline-block;
    -webkit-animation: sk-bouncedelay 1s infinite ease-in-out both;
    animation: sk-bouncedelay 1s infinite ease-in-out both;
  }

  .spinner .bounce1 {
    -webkit-animation-delay: -0.32s;
    animation-delay: -0.32s;
  }

  .spinner .bounce2 {
    -webkit-animation-delay: -0.16s;
    animation-delay: -0.16s;
  }

  @-webkit-keyframes sk-bouncedelay {
    0%,
    80%,
    100% {
      -webkit-transform: scale(0.4);
      background-color: var(--foreground);
    }
    40% {
      -webkit-transform: scale(0.8);
      background-color: var(--secondary);
    }
  }

  @keyframes sk-bouncedelay {
    0%,
    80%,
    100% {
      -webkit-transform: scale(0.4);
      transform: scale(0.4);
      background-color: var(--foreground);
    }
    40% {
      -webkit-transform: scale(0.8);
      transform: scale(0.8);
      background-color: var(--secondary);
    }
  }
`;
function TypingAnimation() {
  return (
    <Animation>
      <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
      </div>
    </Animation>
  );
}

export default TypingAnimation;
