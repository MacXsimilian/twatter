import React, { useState, useContext } from "react";
import styled from "styled-components";
import icons from "../../shared/icons";
import { useFirestoreDoc, useFirestoreDocData, useFirestore } from "reactfire";
import { UserContext } from "components/Context/UserContext";
import firebase from "firebase";
import "emoji-mart/css/emoji-mart.css";
import GifPicker from "react-giphy-component";
import Spinner from "components/shared/Spinner";
import EmojiPickerComponent from "./EmojiPickerComponent";

const NewMessageStyle = styled.div`
  padding: 10px 15px;
  height: 50px;
  display: flex;
  align-items: center;
  border-top: 1px solid var(--borderColor);
  border-right: 1px solid var(--borderColor);
  justify-content: space-between;
  .icon {
    height: 33px;
    width: 33px;
    border-radius: 50%;
    padding: 5px;
    cursor: pointer;
    :hover {
      background-color: var(--secondaryTransparent);
    }
  }
  span svg {
    fill: var(--secondary);
  }
  .message-box {
    margin-top: -3px;
    color: var(--foreground);
    width: 100%;
    border-radius: 100vw;
    padding: 7px 30px 6px 12px;
    border: none;
    background-color: var(--searchBackground);
    outline: none;
    padding-right: 40px;

    :focus-within {
      border: 1px solid var(--secondary);
      background-color: var(--background);
    }
  }
  .emoji {
    margin-left: -35px;
  }
  .msg-container {
    width: 80%;
    display: flex;
    align-items: center;
  }
`;

function NewMessage(props) {
  const [newMessage, setNewMessage] = useState("");
  const { user } = useContext(UserContext);
  const { chatRef, chatData } = props;
  const [openEmojiPicker, setOpenEmojiPicker] = useState(false);

  const sendTyping = (text) => {
    if (text !== "") {
      chatRef.update({ typing: { ...chatData.typing, [user.username]: true } });
    } else {
      chatRef.update({
        typing: { ...chatData.typing, [user.username]: false },
      });
    }
  };

  return (
    <NewMessageStyle>
      <EmojiPickerComponent
        openEmojiPicker={openEmojiPicker}
        setOpenEmojiPicker={setOpenEmojiPicker}
        setNewMessage={setNewMessage}
      />
      <span className="pics icon">{icons.pics}</span>
      <span className="gif icon">{icons.gif}</span>
      <div className="msg-container">
        <form
          style={{ width: "100%" }}
          onSubmit={(e) => {
            e.preventDefault();
            sendMessage();
          }}
        >
          <input
            id="new-message"
            autoComplete="off"
            value={newMessage}
            type="text"
            className="message-box"
            name="new-message"
            placeholder="Start a new message"
            onChange={(e) => {
              sendTyping(e.target.value);
              setNewMessage(e.target.value);
            }}
          />
        </form>

        <span
          onClick={() => setOpenEmojiPicker(!openEmojiPicker)}
          className="emoji icon"
        >
          {icons.emoji}
        </span>
      </div>
      <span
        className="send icon"
        onClick={() => {
          sendMessage();
        }}
      >
        {icons.send}
      </span>
    </NewMessageStyle>
  );

  function sendMessage() {
    if (newMessage.trim() !== "") {
      chatRef.update({
        lastMsg: newMessage,
        lastMsgDate: Math.floor(new Date().getTime() / 1000),
        messages: firebase.firestore.FieldValue.arrayUnion({
          content: newMessage,
          sender: user.username,
          timestamp: Math.floor(new Date().getTime() / 1000),
        }),
      });
    }

    setNewMessage("");
    sendTyping("");
  }
}

export default NewMessage;
