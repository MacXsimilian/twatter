import React, { useContext } from "react";
import styled from "styled-components";
import defaultPic from "../../shared/default_profile_200x200.png";
import { UserContext } from "components/Context/UserContext";
import { ChatContext } from "components/Context/ChatContext";

const MessageStyle = styled.div`
  display: flex;
  flex-direction: row;
  align-self: ${(props) => (props.mine ? "flex-end" : "flex-start")};
  margin-bottom: ${(props) => (props.nextSameSender ? "2px" : "2rem")};
  z-index: -1;
  .username {
    left: 1rem;
    position: absolute;
    bottom: -5px;
    color: #909090;
  }
  .container {
    display: flex;
    flex-direction: column;
    align-items: center;
    position: relative;
  }
`;
const MessageContent = styled.div`
  position: relative;
  left: ${({ mine, nextSameSender }) =>
    mine ? "0" : !mine && !nextSameSender ? "1rem" : "4.5rem"};
  width: fit-content;
  font-size: 1.4rem;
  background-color: ${(props) =>
    props.mine ? "var(--secondary)" : "var(--offSetBackground)"};
  padding: 5px 10px;
  max-width: 204px;
  word-break: break-word;
  border-radius: 1.25rem;

  border-bottom-left-radius: ${(props) => (props.mine ? "" : "0")};
  border-bottom-right-radius: ${(props) => (props.mine ? "0" : "")};

  align-self: center;
`;
const ProfilePicture = styled.img`
  width: 3.5rem;
  height: 3.5rem;
  border-radius: 50%;
  place-self: flex-end;
`;

function Message(props) {
  const { mine, content, sender, nextSameSender } = props;
  const { chat } = useContext(ChatContext);
  return (
    <>
      <MessageStyle nextSameSender={nextSameSender} mine={mine || false}>
        {!mine && !nextSameSender && <ProfilePicture src={defaultPic} />}
        <div className="container">
          <MessageContent nextSameSender={nextSameSender} mine={mine || false}>
            {content}
          </MessageContent>
          {!mine &&
            !nextSameSender &&
            chat.partnerUsername.split(",").length > 1 && (
              <div className="username">{sender}</div>
            )}
        </div>
      </MessageStyle>
    </>
  );
}

export default Message;
