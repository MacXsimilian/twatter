import React, { useState, useEffect, useContext, useRef, useMemo } from "react";
import styled from "styled-components";
import icons from "../../shared/icons";
import Message from "./Message";
import NewMessage from "./NewMessage";
import { useFirestore, useFirestoreDoc } from "reactfire";
import Spinner from "components/shared/Spinner";
import Typing from "./Typing";
import { UserContext } from "../../Context/UserContext";
import { ChatContext } from "components/Context/ChatContext";
import TypingAnimation from "./TypingAnimation";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import _ from "lodash";
import { ModalContext } from "components/Context/ModalConext";

const MessagesStyle = styled.div`
  @media screen and (max-width: 1090px) {
    width: auto;
    height: calc(100vh - 350px);
    max-height: calc(100vh - 350px);
  }

  border-right: 1px solid var(--borderColor);
  display: flex;
  flex-direction: column;
  padding-left: 6px;
  padding-right: 6px;
  overflow: auto;
  height: calc(100vh - 90px);

  /* width */
  ::-webkit-scrollbar {
    width: 10px;
  }

  /* Track */
  ::-webkit-scrollbar-track {
    background: var(--secondaryTransparent);
  }

  /* Handle */
  ::-webkit-scrollbar-thumb {
    background: var(--secondary);
    border-radius: 100vh;
  }

  /* Handle on hover */
  ::-webkit-scrollbar-thumb:hover {
    background: var(--secondaryHover);
  }
`;
const Header = styled.div`
  position: sticky;
  width: 100%;
  height: 40px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-bottom: 1px solid var(--borderColor);
  border-right: 1px solid var(--borderColor);
  padding-left: 10px;
  padding-right: 10px;
  .info svg {
    cursor: pointer;
    fill: var(--secondary);
    height: 20px;
    width: 20px;
  }
  .name {
    font-weight: bold;
    font-size: 1.6rem;
  }
  .username {
    font-size: 1.1rem;
    color: var(--secondary);
  }
  .chat-detail {
    box-shadow: rgba(136, 153, 166, 0.2) 0px 0px 15px,
      rgba(136, 153, 166, 0.15) 0px 0px 3px 1px;
    color: var(--foreground);
    background-color: var(--background);
    position: absolute;
    top: 1rem;
    right: 3rem;
    display: flex;
    flex-direction: column;
    padding: 10px 0;
    border-radius: 1rem;
    font-size: 1.4rem;
    z-index: 500;
    width: 12rem;
  }
  .chat-detail-li {
    cursor: pointer;
    padding: 5px 7px;
    display: flex;
    :hover {
      background-color: var(--secondaryTransparent);
      svg {
        fill: var(--secondary);
      }
    }
  }
  .chat-detail-li svg {
    margin-right: 1rem;
  }
  .chat-detail-li.leave svg {
    width: 1.3rem;
  }
  .chat-detail-li.plus svg {
    width: 1.6rem;
  }
`;

const Container = styled.div`
  width: 100%;
  height: 100vh;
  @media screen and (max-width: 1090px) {
    width: auto;
    height: auto;
  }
`;
function ChatDetails(props) {
  const { chat, setChat } = useContext(ChatContext);
  const { openModal } = useContext(ModalContext);
  const { setOpenChatDetail, chatData, chatRef, user } = props;
  const leaveChat = () => {
    let removedUsers = chatData.users.filter((chatUser) => {
      return chatUser !== user.username;
    });
    chatRef.update({
      users: [...removedUsers],
    });
    setChat({});
  };
  return (
    <ClickAwayListener onClickAway={() => setOpenChatDetail(false)}>
      <div className="chat-detail">
        <div className="chat-detail-li leave" onClick={leaveChat}>
          {icons.logout} Leave Chat
        </div>
        <div
          className="chat-detail-li plus"
          onClick={() => openModal("addChatUser")}
        >
          {icons.plus} Add User
        </div>
      </div>
    </ClickAwayListener>
  );
}
function Messages() {
  const [typing, setTyping] = useState({ isTyping: false, typingMembers: [] });
  const messagesEndRef = useRef(null);
  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView({ behavior: "smooth" });
  };
  const { user } = useContext(UserContext);
  const { chat } = useContext(ChatContext);

  const chatRef = useFirestore().collection("chats").doc(chat.roomId);
  const chatDoc = useFirestoreDoc(chatRef);
  const chatData = useMemo(() => chatDoc.data(), [chatDoc]);
  const checkForTyping = () => {
    let typingMembers = [];
    let isTyping = false;
    for (var i in chatData.typing) {
      if (chatData.typing[i] === true && i !== user.username) {
        isTyping = true;
        typingMembers.push(i);
      }
    }
    return { isTyping, typingMembers };
  };

  useEffect(() => {
    setTyping(checkForTyping());
  }, [chatData.typing]);

  useEffect(scrollToBottom, [chatData.messages]);
  const [openChatDetail, setOpenChatDetail] = useState(false);
  return (
    <Container>
      <Header>
        <div className="user-detail">
          <div className="name">{chat.partnerUsername}</div>
          <div className="username">
            {chat.partnerUsername.split(",").length > 1
              ? ""
              : "@" + chat.partnerUsername}
          </div>
        </div>
        <div
          onClick={() => setOpenChatDetail(!openChatDetail)}
          className="info"
        >
          {icons.info}
        </div>{" "}
        {openChatDetail && (
          <ChatDetails
            chatRef={chatRef}
            chatData={chatData}
            user={user}
            setOpenChatDetail={setOpenChatDetail}
          />
        )}
      </Header>
      <MessagesStyle>
        {chatData.messages &&
          chatData.messages.map((msg, i, arr) => {
            const nextOne = arr[i + 1] || { sender: user.username };
            return (
              <Message
                sender={msg.sender}
                nextSameSender={nextOne.sender == msg.sender}
                mine={msg.sender == user.username}
                content={msg.content}
              />
            );
          })}
        {typing.isTyping &&
          typing.typingMembers.map((member) => {
            return <Typing member={member} />;
          })}
        <div ref={messagesEndRef} />
      </MessagesStyle>

      <NewMessage chatData={chatData} chatRef={chatRef} />
    </Container>
  );
}

export default Messages;
