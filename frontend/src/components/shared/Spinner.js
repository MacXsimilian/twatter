import React from "react";
import styled from "styled-components";

function Spinner() {
  const SpinnerStyle = styled.svg`
    -webkit-animation: spin 0.6s linear infinite;
    -moz-animation: spin 0.6s linear infinite;
    animation: spin 0.6s linear infinite;
    margin: auto;
    .background {
      stroke: var(--secondaryTransparent);
    }
    .foreground {
      stroke: var(--secondary);
      stroke-dasharray: 80;
      stroke-dashoffset: 60;
    }
    @-moz-keyframes spin {
      100% {
        -moz-transform: rotate(360deg);
      }
    }
    @-webkit-keyframes spin {
      100% {
        -webkit-transform: rotate(360deg);
      }
    }
    @keyframes spin {
      100% {
        -webkit-transform: rotate(360deg);
        transform: rotate(360deg);
      }
    }
  `;

  return (
    <SpinnerStyle height="30px" viewBox="0 0 32 32" display="block">
      <circle
        cx="16"
        cy="16"
        fill="none"
        r="14"
        strokeWidth="4"
        className="background"
      ></circle>
      <circle
        cx="16"
        cy="16"
        fill="none"
        r="14"
        strokeWidth="4"
        className="foreground"
      ></circle>
    </SpinnerStyle>
  );
}

export default Spinner;
