import React, { useState, useEffect } from "react";
import Axios from "axios";
import Tweet from "../main-page/Tweet/Tweet";
import Spinner from "components/shared/Spinner";
import styled from "styled-components";

const TrendingTweetsStyle = styled.div`
  .hashtag-name {
    font-size: 2rem;
    font-weight: 900;
    padding: 10px 15px;
  }
  .csiik {
    width: 100%;
    height: 10px;
    background-color: var(--searchBackground);
  }
`;

export default function TrendingTweets() {
  const [trendingTweets, setTrendingTweets] = useState();
  let hashtag = window.location.pathname.split("/").slice(-1)[0];

  useEffect(() => {
    Axios.get(
      process.env.REACT_APP_API_URL +
        ":" +
        process.env.REACT_APP_PORT +
        `/api/search/tweets?hashtag=${hashtag}`
    )
      .then((resp) => {
        setTrendingTweets(resp.data);
      })
      .catch((resp) => {
        return console.log(resp);
      });
  }, [hashtag]);

  return (
    <TrendingTweetsStyle>
      <div className="hashtag-name">#{hashtag}</div>
      <div className="csiik"></div>
      <div className="middle-bar-container">
        {trendingTweets == null ? (
          <div style={{ height: "100%", display: "flex" }}>
            <Spinner />
          </div>
        ) : (
          trendingTweets.map((tweet) => {
            return <Tweet tweet={tweet} key={tweet.id} />;
          })
        )}
      </div>
    </TrendingTweetsStyle>
  );
}
