import React, { useContext } from "react";
import styled from "styled-components";
import { ModalContext } from "components/Context/ModalConext";

const InfoModalStyled = styled.div`
  @media screen and (max-width: 772px) {
    height: 100vh;
    width: 100vw;
    display: block;
    border-radius: 0;
  }

  position: fixed;
  z-index: 2000;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  width: 600px;
  height: 660px;
  background-color: var(--background);
  border-radius: 4rem;
  padding: 2rem;
  font-size:1.7rem;
  overflow-y:auto;
  overflow-x:hidden;

  
    div {
      border-radius: 50%;
      width: 20px;
      height: 20px;
      padding: 25px;
      margin: 5px;
    }
  }

  .modes {
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    background-color: var(--offSetBackground);
    border-radius: 2rem;
    padding: 1rem;
    margin: 10px 0;
    @media screen and (max-width: 772px) {
      flex-direction: column;
    }
  }

  .button-div {
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
  }

  .done {
    cursor: pointer;
    color: white;
    font-size: 1.4rem;
    font-weight: 900;
    text-decoration: none;
    background-color: var(--secondary);
    border: none;
    border-radius: 3rem;
    padding: 10px;
    transition: opacity 0.25s ease;
    :hover {
      opacity: 0.8;
    }
  }
  h3 {
    margin-top: 1rem;
    margin-bottom: 0.5rem;
  }
  ul {
    padding: 0 25px;
  }
  ul li {
    padding-left: 0px;
  }
  em{
      font-size:1.4rem;
  }
  p{
      line-height:1.7rem;
  }
  .footer{
      margin-top:1rem;
      display:contents;
      width:100%;
      a{
          color:var(--secondary)
      }
      a:visited{
          color:var(--secondary)
      }
  }
  .container{
      width:100%;
      display:flex;
        flex-direction:column;
  }
  .margin-bottom{
      margin-bottom:1rem;
  }
`;
let FadeBackground = styled.div`
  position: fixed;
  background-color: rgba(110, 118, 125, 0.4);
  width: 100vw;
  height: 100vh;
  z-index: 1500;
`;
function InfoModal() {
  const { modal, setModal, closeModal } = useContext(ModalContext);

  return (
    <>
      <FadeBackground onClick={() => closeModal("twatterInfo")} />
      <InfoModalStyled>
        <div className="container">
          <h3>Release Notes: September 9th, 2020</h3>

          <h3>Working features:</h3>
          <ul className="margin-bottom">
            <li>Home timeline</li>
            <li>Posting a twat</li>
            <li>Search bar to look up users</li>
            <li>Follow/Unfollow users</li>
            <li>
              Messages:
              <ul>
                <li>Create chat with multiple people</li>
                <li>Emoji picker</li>
                <li>Leave chat</li>
                <li>Add new user to chat</li>
              </ul>
            </li>
            <li>Profile page</li>
            <li>Display</li>
            <li>QR icon</li>
            <li>
              Trending:
              <ul>
                <li>Show more</li>
                <li>Filter daily/weekly/monthly</li>
              </ul>
            </li>
          </ul>
          <div className="footer">
            <p>
              <em>
                This is a Twitter copy. This site was made for only educational
                purposes.
              </em>
            </p>
            <p>
              <em>
                You might experience that a lot of things are not working on the
                site.
              </em>
              <em>
                Yes they are not implemented yet but we are working on it :).
              </em>
            </p>
            <p>
              <em>
                Thank you so much for visiting the site. We are aware that there
                are a lot of bugs so if you want to report one you can do
                that&nbsp;
                <a
                  href="https://bitbucket.org/MacXsimilian/twatter/issues"
                  target="_blank"
                  rel="noopener"
                >
                  here
                </a>
                .&nbsp;
              </em>
            </p>
          </div>
          <div className="button-div">
            <button
              className="done"
              id="done-button"
              onClick={() => closeModal("twatterInfo")}
            >
              Ok
            </button>
          </div>
        </div>
      </InfoModalStyled>
    </>
  );
}

export default InfoModal;
