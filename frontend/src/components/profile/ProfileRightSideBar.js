import React from "react";
import styled from "styled-components";
import SearchBar from "../main-page/right-side-bar/SearchBar";
import TrendsForYou from "../main-page/right-side-bar/TrendsForYou";
import WhoToFollow from "../main-page/right-side-bar/WhoToFollow";
import SidebarFooter from "../main-page/right-side-bar/SidebarFooter";

const ProfileRightSideBarStyle = styled.div`
  grid-area: rightBar;
  width: max(350px);
  margin: 0 20px;

  @media screen and (max-width: 1090px) {
    width: max(280px);
  }

  @media screen and (max-width: 1000px) {
    display: none;
  }
`;

function ProfileRightSideBar() {
  return (
    <ProfileRightSideBarStyle>
      <SearchBar />
      <WhoToFollow title="You might like" />
      <TrendsForYou />
      <SidebarFooter />
    </ProfileRightSideBarStyle>
  );
}

export default ProfileRightSideBar;
