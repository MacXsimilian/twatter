import React, {useContext} from "react";
import styled from "styled-components";
import {ModalContext} from "../Context/ModalConext";
import InputField from "../login/InputField";
import {ErrorMessage} from "@hookform/error-message";
import {useForm} from "react-hook-form";
import {AuthContext} from "../Context/AuthContext";
import icons from "../shared/icons";

const ProfileSettingsModalStyle = styled.div`
    @media screen and (max-width: 772px) {
    height: 100vh;
    width: 100vw;
    display: block;
    border-radius: 0;
  }
  position: fixed;
  z-index: 2000;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  width: 600px;
  height: 660px;
  background-color: var(--background);
  border-radius: 4rem;
  
  padding: .4rem 2rem 2rem 2rem;
  
  .top-bar {
      position: sticky;
      top: 0;
      display: flex;
      justify-content: space-between;
      align-items: center;
      height: 53px;
      width: 100%;
      button { 
        display: flex;
        align-items: center;
        justify-content: center;
        height: 30px;
        width: 64px;
        font-size: 15px;
        color: var(--foreground);
        background-color: var(--secondary);
        font-weight: bold;
        font-family: system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI",
          Roboto, Ubuntu, "Helvetica Neue", sans-serif;
        border: var(--secondary) 1px solid;
        border-radius: 99999px;
        padding: 0 1rem;
        cursor: pointer;
        transition: 0.4s ease background-color;
        :hover {
          background-color: var(--secondaryHover);
        }
      }
  }
  
  .title {
      
  }
  
  .button-with-title {
      position: relative;
      display: flex;
      flex-direction: row;
      justify-content: space-between;
      align-items: center;
      width: 150px;
  }
  
  .close-button {
    position: relative;
    top: 14px;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 60%;
    transform: translateY(-50%);
    height: 27px;
    width: 27px;
    transition: opacity 0.24s ease, background-color 0.24s ease;
    svg {
      width: 22px;
      height: 22px;
      fill: var(--secondary);
    }
    :hover {
      background-color: var(--secondaryHover);
      cursor: pointer;
      opacity: 0.8;
    }
  }
  
`

let FadeBackground = styled.div`
  position: fixed;
  background-color: rgba(110, 118, 125, 0.4);
  width: 100vw;
  height: 100vh;
  z-index: 1500;
`;

export default function ProfileSettingsModal() {
    const {closeModal} = useContext(ModalContext);

    const { handleSubmit, register, errors } = useForm();

    const onSubmit = (values) => {

    };

    return (
        <>
            <FadeBackground onClick={() => closeModal("profileSettings")}/>
            <ProfileSettingsModalStyle>
                <form className="form" onSubmit={handleSubmit(onSubmit)}>
                    <div className="top-bar">
                        <div className="button-with-title">
                            <div className="close-button" onClick={() => closeModal("profileSettings")}>
                                {icons.close}
                            </div>
                            <h1 className="edit-title">Edit profile</h1>
                        </div>

                        <button className="button" type="submit">
                            <span>Save</span>
                        </button>
                    </div>
                    <InputField
                        defaultColor
                        withCounter
                        label="Name"
                        type="text"
                        name="profileName"
                        feature="settings"
                    />
                    <InputField className="bio"
                        defaultColor
                        withCounter
                        label="Bio"
                        type="text"
                        name="bio"
                        placeholder="asd"
                        feature="settings"
                    />

                    <InputField
                        defaultColor
                        withCounter
                        label="Location"
                        type="text"
                        name="location"
                        feature="settings"
                    />

                    <InputField
                        defaultColor
                        withCounter
                        label="Website"
                        type="text"
                        name="website"
                        feature="settings"
                    />

                </form>
            </ProfileSettingsModalStyle>
        </>
    )

}