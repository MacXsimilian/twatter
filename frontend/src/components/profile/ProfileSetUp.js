import React, { useContext, useEffect, useState } from "react";
import styled from "styled-components";
import { UserContext } from "../Context/UserContext";
import { useParams } from "react-router-dom";
import profilepic from "../shared/default_profile_200x200.png";
import icons from "../shared/icons";
import Axios from "axios";
import { parse } from "dotenv";
import _ from "lodash";
import {ModalContext} from "../Context/ModalConext";
const ProfileSetUpStyle = styled.div`
  display: flex;
  flex-direction: column;

  .top-container {
    height: 200px;
    background-color: rgb(61, 84, 102);
  }

  .middle-container {
    position: relative;
    height: 49px;
    padding: 10px 15px 0 15px;
    margin-bottom: 15px;
  }

  .middle-container-upper-part {
    height: 49px;
    position: relative;
    display: flex;
    flex-direction: row;
    justify-content: end;
    align-items: center;
  }

  .middle-container-profile-picture {
    display: flex;
    position: absolute;
    left: 0;
    bottom: 0;
    height: 142px;
    width: 142px;
    border-radius: 60%;
    border: var(--background) 4px solid;
    cursor: pointer;
  }

  .right-upper-buttons {
    display: flex;
    flex-direction: row;
    width: 100%;
    justify-content: flex-end;
  }

  .set-profile-button {
    display: flex;
    align-items: center;
    height: 37px;
    color: var(--secondary);
    font-size: 15px;
    font-weight: bold;
    font-family: system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI",
      Roboto, Ubuntu, "Helvetica Neue", sans-serif;
    border: var(--secondary) 1px solid;
    border-radius: 99999px;
    padding: 0 1rem;
    cursor: pointer;
    transition: 0.4s ease background-color;
    :hover {
      background-color: var(--secondaryTransparent);
    }
  }
  .set-profile-following {
    background-color: var(--secondary);
    color: var(--foreGround);
    :hover {
      background-color: var(--secondary);
    }
  }

  .right-upper-buttons svg {
    cursor: pointer;
    height: 37px;
    fill: var(--secondary);
    color: var(--secondary);
    border-radius: 999999px;
    border: var(--secondary) 1px solid;
    transition: 0.4s ease background-color;
    margin-right: 10px;
    :hover {
      background-color: var(--secondaryTransparent);
    }
  }

  .bottom-container {
    padding: 10px 15px 0 15px;
    margin-bottom: 15px;
  }

  .bottom-container-name {
    font-size: 19px;
    font-weight: 800;
    font-family: system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI",
      Roboto, Ubuntu, "Helvetica Neue", sans-serif;
  }

  .bottom-container-email {
    font-size: 15px;
    font-weight: 400;
    font-family: system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI",
      Roboto, Ubuntu, "Helvetica Neue", sans-serif;
    color: rgb(136, 153, 166);
    margin-bottom: 10px;
  }

  .bottom-container-short-description {
    font-weight: 400;
    font-size: 15px;
    font-family: system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI",
      Roboto, Ubuntu, "Helvetica Neue", sans-serif;
    margin-bottom: 10px;
  }

  .bottom-container-availabilities {
    display: flex;
    flex-direction: row;
  }

  .bottom-container-availability {
    display: flex;
    flex-direction: row;
  }

  .bottom-container-availability-icons svg {
    height: 18px;
    width: 18px;
    fill: rgb(136, 153, 166);
    color: rgb(136, 153, 166);
    margin-right: 5px;
  }

  .bottom-container-availability-texts {
    color: rgb(136, 153, 166);
    font-size: 15px;
    font-weight: 400;
    font-family: system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI",
      Roboto, Ubuntu, "Helvetica Neue", sans-serif;
    margin-right: 10px;
    margin-bottom: 10px;
  }

  .bottom-container-availability-texts-with-links {
    color: var(--secondary);
    font-size: 15px;
    font-weight: 400;
    font-family: system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI",
      Roboto, Ubuntu, "Helvetica Neue", sans-serif;
    margin-right: 10px;
    :hover {
      text-decoration: underline;
      cursor: pointer;
    }
  }

  .bottom-container-follows {
    display: flex;
    flex-direction: row;
    font-size: 15px;
    font-weight: 400;
    flex-wrap: wrap;
    margin-bottom: 10px;
    font-family: system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI",
      Roboto, Ubuntu, "Helvetica Neue", sans-serif;
    span {
      font-weight: bold;
      font-family: system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI",
        Roboto, Ubuntu, "Helvetica Neue", sans-serif;
    }
  }

  .bottom-container-follows-following {
    margin-right: 25px;
  }

  .bottom-container-popular-followers {
    font-size: 13px;
    font-weight: 400;
    font-family: system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI",
      Roboto, Ubuntu, "Helvetica Neue", sans-serif;
    color: rgb(136, 153, 166);
    svg {
      height: 18px;
      width: 18px;
      fill: rgb(136, 153, 166);
      color: rgb(136, 153, 166);
      margin-right: 5px;
    }
  }
`;

function ProfileSetUp(props) {
    const str = props.profile.registrationDate.split("-");
    const months = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "june",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
    ];
    const month = str[1].split("")[0] === "0" ? str[1].split("")[1] : str[1];
    const { refreshUser, user } = useContext(UserContext);
    const [isItFollowed, setisItFollowed] = useState(false);
    const { modal, setModal, openModal } = useContext(ModalContext);

    const isFollowed = () => {
        return _.includes(props.user.followeeIds, props.profile.id);
    };

    useEffect(() => {
        setisItFollowed(isFollowed());
    }, []);

    const follow = () => {
        Axios.post(
            `${
                process.env.REACT_APP_API_URL + ":" + process.env.REACT_APP_PORT
            }/api/users/followees`,
            { followeeId: props.profile.id }
        )
            .then((resp) => {
                refreshUser();
                setisItFollowed(true);
            })
            .catch((err) => console.log(err));
    };
    const unfollow = () => {
        Axios.delete(
            `${
                process.env.REACT_APP_API_URL + ":" + process.env.REACT_APP_PORT
            }/api/users/followees/${props.profile.id}`
        )
            .then((resp) => {
                setisItFollowed(false);
                refreshUser();
            })
            .catch((err) => console.log(err));
    };

    return (
        <ProfileSetUpStyle>
            <div className="top-container"></div>
            <div className="middle-container">
                <div className="middle-container-upper-part">
                    <img
                        className="middle-container-profile-picture"
                        src={profilepic}
                        alt=""
                    />
                    {props.user.username === props.profileVariable ? (
                        <div className="right-upper-buttons">
                            <div className="set-profile-button" onClick={() => openModal("profileSettings")}>Edit profile</div>
                        </div>
                    ) : (
                        <div className="right-upper-buttons">
                            {icons.more}
                            {isItFollowed ? (
                                <div
                                    onMouseEnter={(e) => {
                                        e.target.innerText = "Unfollow";
                                    }}
                                    onMouseLeave={(e) => {
                                        e.target.innerText = "Following";
                                    }}
                                    className="set-profile-button set-profile-following"
                                    onClick={unfollow}
                                >
                                    Following
                                </div>
                            ) : (
                                <div className="set-profile-button" onClick={follow}>
                                    Follow
                                </div>
                            )}
                        </div>
                    )}
                </div>
            </div>
            <div className="bottom-container">
                <div className="bottom-container-name">{props.profile.username}</div>
                <div className="bottom-container-email">@{props.profile.username}</div>
                {/*<div className="bottom-container-short-description"> Hey ho wats upp my man how are you doing today ???</div>*/}
                <div className="bottom-container-availabilities">
                    {/*<div className="bottom-container-availability" >*/}
                    {/*    <div className="bottom-container-availability-icons">{icons.lists}</div>*/}
                    {/*    <div className="bottom-container-availability-texts">Redmond, WA</div>*/}
                    {/*</div>*/}
                    {/*<div className="bottom-container-availability" >*/}
                    {/*    <div className="bottom-container-availability-icons">{icons.lists}</div>*/}
                    {/*    <div className="bottom-container-availability-texts-with-links">Xbox.com</div>*/}
                    {/*</div>*/}
                    <div className="bottom-container-availability">
                        <div className="bottom-container-availability-icons">
                            {icons.lists}
                        </div>
                        <div className="bottom-container-availability-texts">
                            Joined {months[month - 1]} {str[0]}
                        </div>
                    </div>
                </div>
                <div className="bottom-container-follows">
                    <div className="bottom-container-follows-following">
                        <span>{props.profile.following}</span> Following
                    </div>
                    <div className="bottom-container-follows-followers">
                        <span>{props.profile.followers}</span> Followers
                    </div>
                </div>
                {/*<div className="bottom-container-popular-followers">*/}
                {/*    {icons.lists}Followed by Electronic Arts*/}
                {/*</div>*/}
            </div>
        </ProfileSetUpStyle>
    );
}

export default ProfileSetUp;
