import React from "react";
import styled from "styled-components";

const ProfileBarOptionsStyled = styled.div`
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-around;
    
    
    .options {
        display: flex;
        justify-content: center;
        align-items: center;
        color: rgb(101, 119, 134);
        height: 53px;
        width: 100%;
        font-size: 15px;
        font-weight: bold;        
        font-family: system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Ubuntu, "Helvetica Neue", sans-serif;
        border-bottom: 2px rgb(136, 153, 166) solid;
        :hover {
            color: var(--secondary);
            background-color: var(--secondaryTransparent);
            cursor: pointer;
        }
    }
    
    .active {
        color: var(--secondary);
        border-bottom: 2px var(--secondary) solid;
    }
    
    
    
    
`


export default function ProfileBarOptions() {

    return (
        <ProfileBarOptionsStyled>
            <div className="options active">Tweets</div>
            <div className="options">Tweets & replies</div>
            <div className="options">Media</div>
            <div className="options">Likes</div>
        </ProfileBarOptionsStyled>
    )

}