import React, { useContext } from "react";
import QRCode from "qrcode.react";
import styled from "styled-components";
import profilepic from "../shared/default_profile_200x200.png";
import { UserContext } from "components/Context/UserContext";
import { ModalContext } from "components/Context/ModalConext";

const QRCodeStyle = styled(QRCode)`
  position: fixed;
  z-index: 2000;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);

  border-radius: 1rem;
  path {
    fill: white;
  }
  path + path {
    fill: black;
  }
  image {
    border-radius: 50%;
  }
`;
let FadeBackground = styled.div`
  position: fixed;
  background-color: rgba(110, 118, 125, 0.4);
  width: 100vw;
  height: 100vh;
  z-index: 1500;
`;
function ProfileQR() {
  const { user } = useContext(UserContext);
  const { closeModal } = useContext(ModalContext);
  return (
    <>
      <FadeBackground onClick={() => closeModal("qr")} />

      <QRCodeStyle
        includeMargin={true}
        level="Q"
        renderAs="svg"
        value={"https://twatterbook.eu/profile/" + user.username}
        size={300}
      />
    </>
  );
}

export default ProfileQR;
