import React, { useContext, useEffect, useState } from "react";
import styled from "styled-components";
import WhoToFollow from "../main-page/right-side-bar/WhoToFollow";
import ProfileSetUp from "./ProfileSetUp";
import ProfileBarOptions from "./ProfileBarOptions";
import { UserContext } from "../Context/UserContext";
import { useParams } from "react-router-dom";
import Axios from "axios";
import Spinner from "../shared/Spinner";
import TweetFeed from "components/main-page/middle-bar/TweetFeed";

const ProfileMidlebarStyle = styled.div`
  grid-area: middleBar;
  display: flex;
  flex-direction: column;
  position: relative;
  border-style: solid;
  width: 100%;
  border-width: 0 1.5px;
  border-color: rgb(37, 51, 65);

  .home-bar {
    z-index: 800;
    background-color: var(--background);
    position: sticky;
    top: 0;
    display: flex;
    align-items: center;
    padding-left: 60px;
    padding-right: 15px;
    width: inherit;
    height: 53px;
    border-bottom: 1px solid rgb(56, 68, 77);
  }

  .arrow {
    color: rgb(29, 161, 242);
    position: absolute;
    left: 20px;
    top: 50%;
    transform: translateY(-50%);
    font-size: 2rem;
    transition: background-color 0.24s ease;
    padding: 5px;
    border-radius: 50%;
    :hover {
      background-color: rgba(58, 92, 135, 0.2);
    }
  }

  .user-name {
    color: white;
    font-size: 1.4rem;
    font-weight: 800;
  }

  .user-tweet-counter {
    color: rgb(136, 153, 166);
    font-size: 1.2rem;
  }
`;

const Spinnere = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100vh;
`;

function ProfileMidlebar() {
  const [profile, setProfile] = useState(null);
  const user = JSON.parse(localStorage.getItem("user"));
  const { profileVariable } = useParams();

  useEffect(() => {
    console.log(user);
    Axios.get(
      `${
        process.env.REACT_APP_API_URL + ":" + process.env.REACT_APP_PORT
      }/api/users/byUsername/${profileVariable}`
    )
      .then((resp) => {
        setProfile(resp.data);
      })
      .catch((err) => console.log(err));
  }, []);

  return (
    <>
      {profile === null ? (
        <Spinnere>
          <Spinner />
        </Spinnere>
      ) : (
        <ProfileMidlebarStyle>
          <div className="home-bar">
            <div className="arrow">
              <i className="fas fa-arrow-left"></i>
            </div>
            <div className="home-bar-content">
              <div className="user-name">{profile.username}</div>
              <div className="user-tweet-counter">0 Tweets</div>
            </div>
          </div>
          <ProfileSetUp
            user={user}
            profile={profile}
            profileVariable={profileVariable}
          />
          <ProfileBarOptions />
          <TweetFeed src={{ path: "user", userid: profile.id }} />
          <WhoToFollow title="Who to follow" />
        </ProfileMidlebarStyle>
      )}
    </>
  );
}

export default ProfileMidlebar;
