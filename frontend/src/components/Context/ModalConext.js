import React, { createContext, useState } from "react";
import _ from "lodash";
export const ModalContext = createContext();

function ModalContextProvider(props) {
  const [modal, setModal] = useState({
    QR: false,
    displaySettings: false,
    newChat: false,
    addChatUser: false,
    twatterInfo: false,
    profileSettings: false,
  });
  const openModal = (modalname) => {
    setModal({ ...modal, [modalname]: true });
  };
  const closeModals = (modalnames) => {
    setModal({
      ...modal,
      ...modalnames.reduce((o, k) => _.set(o, k, false), {}),
    });
  };
  const closeModal = (modalname) => {
    setModal({ ...modal, [modalname]: false });
  };
  return (
    <ModalContext.Provider
      value={{ modal, setModal, closeModal, openModal, closeModals }}
    >
      {props.children}
    </ModalContext.Provider>
  );
}

export default ModalContextProvider;
