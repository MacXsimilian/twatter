import React, { createContext, useState } from "react";

export const ChatContext = createContext();

function ChatContextProvider(props) {
  const [chat, setChat] = useState({});

  return (
    <ChatContext.Provider value={{ chat, setChat }}>
      {props.children}
    </ChatContext.Provider>
  );
}

export default ChatContextProvider;
