import React, { createContext, useState, useEffect } from "react";
import Axios from "axios";
import storage from "local-storage-fallback";

export const UserContext = createContext();

function UserContextProvider(props) {
  const [user, setUser] = useState({});
  useEffect(() => {
    let savedUser = storage.getItem("user");
    let parsedUser = user ? JSON.parse(savedUser) : { username: "no user" };
    setUser(parsedUser);
  }, []);

  const refreshUser = () => {
    Axios.get(
      `${
        process.env.REACT_APP_API_URL + ":" + process.env.REACT_APP_PORT
      }/api/users/${user.id}`
    ).then(({ data }) => {
      storage.setItem("user", JSON.stringify(data));
      setUser(data);
    });
  };
  return (
    <UserContext.Provider value={{ user, setUser, refreshUser }}>
      {props.children}
    </UserContext.Provider>
  );
}

export default UserContextProvider;
