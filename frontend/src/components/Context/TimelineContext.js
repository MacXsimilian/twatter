import React, {createContext, useState} from "react";


export const TimelineContext = createContext();

function TimelineContextProvider(props) {
    const [tweets, setTweets] = useState(null);

    return (
        <TimelineContext.Provider value={{ tweets, setTweets }}>
            {props.children}
        </TimelineContext.Provider>
    );
}

export default TimelineContextProvider;