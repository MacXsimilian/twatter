package com.codecool.twatterspring.repository;

import com.codecool.twatterspring.model.Tweet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TweetRepository extends JpaRepository<Tweet, UUID> {

    List<Tweet> findAllByUserIdOrderByDateDesc(Long id);

    @Query("SELECT t FROM Tweet t WHERE t.userId IN :followeeIds OR t.userId = :userId ORDER BY t.date DESC")
    List<Tweet> findLimitedNumberOfTweetsByFolloweeIds(@Param("followeeIds") Collection<Long> followeeIds, @Param("userId") Long userId);

    List<Tweet> findAllByIdInOrderByDateDesc(List<UUID> ids);

    @Query("SELECT COUNT(t.id) FROM Tweet t WHERE t.userId = :userId")
    Optional<Long> getTweetCountBy(@Param("userId") Long userId);
}
