package com.codecool.twatterspring.repository;

import com.codecool.twatterspring.model.TwatterUser;
import com.codecool.twatterspring.model.dto.SizesDTO;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface TwatterUserRepository extends CrudRepository<TwatterUser, Long> {
    Optional<TwatterUser> findByName(String username);

    @EntityGraph(attributePaths = "followers", type = EntityGraph.EntityGraphType.LOAD)
    @Query("SELECT u FROM TwatterUser u WHERE u.id = :userId")
    Optional<TwatterUser> findByIdWithFollowers(@Param("userId") Long userId);

    @EntityGraph(attributePaths = "followees", type = EntityGraph.EntityGraphType.LOAD)
    @Query("SELECT u FROM TwatterUser u WHERE u.id = :userId")
    Optional<TwatterUser> findByIdWithFollowees(@Param("userId") Long userId);

    @Query("SELECT u.name FROM TwatterUser u WHERE u.id = :userId")
    String getUsernameByUserId(@Param("userId") Long userId);

    @Query("SELECT u.followees FROM TwatterUser u WHERE u.id = :userId")
    List<TwatterUser> getFolloweesByUserId(@Param("userId") Long userId);

    @Query("SELECT f.id FROM TwatterUser u JOIN u.followees f WHERE u.id = :userId")
    Set<Long> getFolloweeIdsByUserId(@Param("userId") Long userId);

    @Query("SELECT u.followers FROM TwatterUser u WHERE u.id = :userId")
    List<TwatterUser> getFollowersByUserId(@Param("userId") Long userId);

    @Query("SELECT f.id FROM TwatterUser u JOIN u.followers f WHERE u.id = :userId")
    Set<Long> getFollowerIdsByUserId(@Param("userId") Long userId);

    @Query("SELECT u FROM TwatterUser u WHERE LOWER(u.name) LIKE CONCAT(CONCAT('%', LOWER(:queryString)),'%') ORDER BY SIZE(u.followers) DESC")
    List<TwatterUser> findUsersOrderedByPopularityContainingIgnoreCase(String queryString, Pageable pageable);

    @Query("SELECT new com.codecool.twatterspring.model.dto.SizesDTO(SIZE(u.followees), SIZE(u.followers)) FROM TwatterUser u WHERE u.id = :id")
    Optional<SizesDTO> getFolloweeAndFollowerCountBy(Long id);
}
