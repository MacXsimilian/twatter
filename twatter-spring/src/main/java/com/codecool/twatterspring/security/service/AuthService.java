package com.codecool.twatterspring.security.service;

import com.codecool.twatterspring.model.dto.AuthDTO;
import com.codecool.twatterspring.service.dao.TwatterUserDao;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseCookie;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class AuthService {

    private final AuthenticationManager authenticationManager;
    private final JwtService jwtService;
    private final TwatterUserDao userDao;

    public static final String TOKEN_COOKIE_NAME = "JWT";

    public Long tryLogin(AuthDTO loginDTO, HttpServletResponse response) {
        Authentication authentication = tryAuthenticate(loginDTO);
        if (authentication == null)
            return -1L;
        return login(authentication, response);
    }

    private Authentication tryAuthenticate(AuthDTO loginDTO) {
        Authentication authentication = null;
        try {
            authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginDTO.getUsername(), loginDTO.getPassword())
            );
        } catch (AuthenticationException ignored) {
        }
        return authentication;
    }

    @SuppressWarnings("OptionalGetWithoutIsPresent")
    private Long login(Authentication authentication, HttpServletResponse response) {
        List<String> roles = getRolesFrom(authentication);
        String username = authentication.getName();
        Long userId = userDao.findBy(username).get().getId();
        String token = jwtService.createToken(username, roles, Long.toString(userId));
        ResponseCookie tokenCookie = createTokenCookie(token, Duration.ofSeconds(-1L));
        response.addHeader("Set-Cookie", tokenCookie.toString());
        return userId;
    }

    private List<String> getRolesFrom(Authentication authentication) {
        return authentication.getAuthorities().stream()
            .map(GrantedAuthority::getAuthority)
            .collect(Collectors.toList());
    }

    public void logout(HttpServletResponse response) {
        invalidateTokenCookie(response);
    }

    private void invalidateTokenCookie(HttpServletResponse response) {
        log.info("Invalidating JWT cookie...");
        ResponseCookie cookie = createTokenCookie("", Duration.ZERO);
        response.addHeader("Set-Cookie", cookie.toString());
    }

    public boolean tryRegister(AuthDTO registrationDTO) {
        if (userDao.exists(registrationDTO.getUsername())) {
            log.warn("Username '" + registrationDTO.getUsername() + "' already exists!");
            return false;
        }
        userDao.save(registrationDTO);
        return true;
    }

    public ResponseCookie createTokenCookie(String jwt, Duration maxAge) {
        return ResponseCookie.from(TOKEN_COOKIE_NAME, jwt)
            .sameSite("Strict")
            .httpOnly(true)
            .path("/")
            .maxAge(maxAge)
            .build();
    }
}
