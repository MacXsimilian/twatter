package com.codecool.twatterspring.security.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

@Profile("test & !security")
@Configuration
@RequiredArgsConstructor
@Slf4j
public class NoOpSecurityConfig extends BaseSecurityConfig implements InitializingBean {
    private final Environment env;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        var cors = http.cors();
        if (!isCorsEnabled())
            cors.disable();
        http
            .csrf().disable()
            .authorizeRequests()
            .anyRequest().permitAll();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println();
        log.warn("NoOpSecurityConfig initialized. There will be no authentication.");
        log.info("Cors enabled: " + isCorsEnabled());
        System.out.println();
    }

    private boolean isCorsEnabled() {
        return env.acceptsProfiles(Profiles.of("cors"));
    }
}
