package com.codecool.twatterspring.controller;

import com.codecool.twatterspring.model.dto.IncomingTweetDTO;
import com.codecool.twatterspring.model.dto.TimelineTweetDTO;
import com.codecool.twatterspring.service.TweetService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/tweets")
@RequiredArgsConstructor
public class TweetController {

    private final TweetService tweetService;

    @PostMapping
    public ResponseEntity<TimelineTweetDTO> add(@RequestBody IncomingTweetDTO dto,
                                                @CookieValue("JWT") String jwt) {
        UUID tweetId = UUID.randomUUID();
        return ResponseEntity.ok(tweetService.handleNewTweet(dto, tweetId, jwt));
    }
}
