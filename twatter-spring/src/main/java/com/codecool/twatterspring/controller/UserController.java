package com.codecool.twatterspring.controller;

import com.codecool.twatterspring.model.dto.TwatterUserProfileDTO;
import com.codecool.twatterspring.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RequiredArgsConstructor
@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    @GetMapping("/{userId}")
    public ResponseEntity<TwatterUserProfileDTO> getProfile(@PathVariable Long userId) {
        return ResponseEntity.ok(userService.getUserProfileBy(userId));
    }

    @GetMapping("/byUsername/{username}")
    public ResponseEntity<TwatterUserProfileDTO> getProfile(@PathVariable String username) {
        return ResponseEntity.ok(userService.getUserProfileBy(username));
    }

    @PostMapping("/followees")
    public ResponseEntity<?> followUser(@RequestBody Map<String, Long> followee, @CookieValue("JWT") String jwt) {
        System.out.println(followee.get("followeeId"));
        userService.followUser(jwt, followee.get("followeeId"));
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/followees/{followeeId}")
    public ResponseEntity<?> unfollowUser(@PathVariable Long followeeId, @CookieValue("JWT") String jwt) {
        userService.unfollowUser(jwt, followeeId);
        return ResponseEntity.ok().build();
    }
}
