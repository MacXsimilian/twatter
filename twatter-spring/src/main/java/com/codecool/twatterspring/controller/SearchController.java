package com.codecool.twatterspring.controller;


import com.codecool.twatterspring.model.dto.TimelineTweetDTO;
import com.codecool.twatterspring.model.dto.UserSearchDTO;
import com.codecool.twatterspring.service.ISearchService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
@RestController
@RequestMapping("/search")
@Slf4j
public class SearchController {

    private final ISearchService searchService;

    @GetMapping("")
    public ResponseEntity<List<UserSearchDTO>> searchFromSearchBox(@RequestParam(name = "q") String query) {
        return ResponseEntity.ok(searchService.searchUsersBy(query));
    }

    @PostMapping("/post")
    public ResponseEntity<List<UserSearchDTO>> searchFromSearchBoxPost(@RequestBody Map<String, String> query) {
        return ResponseEntity.ok(searchService.searchUsersBy(query.get("query")));
    }

    @GetMapping("/tweets")
    public ResponseEntity<List<TimelineTweetDTO>> searchTweetsByHashtag(@RequestParam(name = "hashtag") String hashtag) {
        log.info("Frontend request arrived with param: " + hashtag);
        return ResponseEntity.ok(searchService.searchTweetsBy(hashtag));
    }
}
