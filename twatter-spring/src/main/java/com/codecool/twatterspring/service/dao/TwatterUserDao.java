package com.codecool.twatterspring.service.dao;

import com.codecool.twatterspring.model.TwatterUser;
import com.codecool.twatterspring.model.dto.AuthDTO;
import com.codecool.twatterspring.model.dto.SizesDTO;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public interface TwatterUserDao {

    Optional<TwatterUser> findById(Long id);
    Optional<TwatterUser> findBy(String username);
    String getUsernameByUserId(Long userId);
    Set<Long> getFollowerIdsByUserId(Long userId);
    Set<Long> getFolloweeIdsByUserId(Long userId);
    boolean exists(String username);
    void save(AuthDTO userDTO);
    void followUser(Long userId, Long followerId);
    void unfollowUser(Long userId, Long followerId);
    Optional<SizesDTO> getAssociationSizesForUserBy(Long id);
}
