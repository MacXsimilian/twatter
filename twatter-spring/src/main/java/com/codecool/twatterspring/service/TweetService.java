package com.codecool.twatterspring.service;

import com.codecool.twatterspring.model.Tweet;
import com.codecool.twatterspring.model.dto.IncomingTweetDTO;
import com.codecool.twatterspring.model.dto.TimelineTweetDTO;
import com.codecool.twatterspring.model.dto.TrendingTweetDTO;
import com.codecool.twatterspring.redis.service.TimelineOwnerDao;
import com.codecool.twatterspring.security.service.JwtService;
import com.codecool.twatterspring.service.dao.TweetDao;
import com.codecool.twatterspring.util.Utils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class TweetService {

    private final TweetDao tweetDao;
    private final JwtService jwtService;
    private final ITrendingApiService trending;
    private final TimelineOwnerDao timelineOwnerDao;

    public TimelineTweetDTO handleNewTweet(IncomingTweetDTO dto, UUID tweetId, String jwt) {
        Long userId = jwtService.parseUserIdFromToken(jwt);
        String username = jwtService.parseUsernameFromToken(jwt);
        var postedAt = LocalDateTime.now();
        timelineOwnerDao.saveNewTweet(dto, tweetId, userId, Utils.convertDate(postedAt));
        Tweet tweet = tweetDao.save(dto, tweetId, userId, username, postedAt);
        trending.postNewTweet(TrendingTweetDTO.fromEntity(tweet));
        return TimelineTweetDTO.fromEntity(tweet);
    }

    public List<TimelineTweetDTO> provideTweetsForUserTimelineBy(Long userId) {
        return tweetDao.provideTweetsForUserTimelineBy(userId);
    }

    public List<TimelineTweetDTO> provideTweetsForHomeTimelineBy(Long userId) {
        return tweetDao.provideTweetsForHomeTimelineBy(userId);
    }

    public List<TimelineTweetDTO> provideTweetsForTrendingHashtag(String hashtag) {
        log.info("In tweetService provideTweetsForTrendingHashtag called with param: " + hashtag);
        List<UUID> tweetIds = Arrays
                .stream(trending.getTweetIDsByTrendingHashtag(hashtag))
                .map(UUID::fromString)
                .collect(Collectors.toList());

        return tweetDao.provideTweetsForTrendingHashtagBy(tweetIds);
    }
}
