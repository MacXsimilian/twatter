package com.codecool.twatterspring.service.dao;

import com.codecool.twatterspring.model.Tweet;
import com.codecool.twatterspring.model.dto.IncomingTweetDTO;
import com.codecool.twatterspring.model.dto.TimelineTweetDTO;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public interface TweetDao {
    Tweet save(IncomingTweetDTO dto, UUID tweetId, Long userId, String username, LocalDateTime postedAt);
    List<TimelineTweetDTO> provideTweetsForUserTimelineBy(Long userId);
    List<TimelineTweetDTO> provideTweetsForHomeTimelineBy(Long userId);
    List<TimelineTweetDTO> provideTweetsForTrendingHashtagBy(List<UUID> ids);
    Long getTweetCountBy(Long userId);
}
