package com.codecool.twatterspring.service;

import com.codecool.twatterspring.model.dto.TrendingHashtagsDTO;
import com.codecool.twatterspring.model.dto.TrendingTweetDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Profile;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

@Service
@Profile("trending_api")
@Slf4j
public class TrendingApiService implements ITrendingApiService {

    private final String baseUrl;

    private final RestTemplate template;

    public TrendingApiService(RestTemplateBuilder builder, @Value("${twatter.trending-api.base-url}") String baseUrl) {
        this.template = builder.build();
        this.baseUrl = baseUrl;
    }

    @Override
    public HttpStatus postNewTweet(TrendingTweetDTO tweet) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<TrendingTweetDTO> request = new HttpEntity<>(tweet, headers);
        ResponseEntity<?> response = template.postForEntity(
                baseUrl + "/trending",
                request,
                Void.class
        );
        return response.getStatusCode();
    }

    @Override
    public TrendingHashtagsDTO getTradingHashtagsByTimeInterval(String interval) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.ALL));
        HttpEntity<String> request = new HttpEntity<>(null, headers);
        ResponseEntity<TrendingHashtagsDTO> response = template.exchange(
                baseUrl + "/trending/" + interval,
                HttpMethod.GET,
                request,
                TrendingHashtagsDTO.class
        );
        if (response.getBody() != null) log.info("response with body: " + response.getBody().toString());
        else log.error("response without body: " + response.getHeaders().toString());

        return response.getBody();
    }

    @Override
    public String[] getTweetIDsByTrendingHashtag(String hashtag) {
        log.info("Get param hashtag as: " + hashtag);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(baseUrl + "/trending-tweets")
                .queryParam("hashtag", hashtag);
        log.info("URI was built as: " + builder.toUriString());

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.ALL));
        HttpEntity<String> request = new HttpEntity<>(null, headers);

        ResponseEntity<String[]>response = template.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                request,
                String[].class
        );
        if (response.getBody() != null) log.info("response with body: " + Arrays.toString(response.getBody()));
        else log.info("response without body: " + response.getHeaders().toString());

        return response.getBody();
    }
}
