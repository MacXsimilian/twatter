package com.codecool.twatterspring.service;

import com.codecool.twatterspring.model.dto.TimelineTweetDTO;
import com.codecool.twatterspring.model.dto.UserSearchDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@ConditionalOnMissingBean(SearchService.class)
@Slf4j
public class NoOpSearchService implements ISearchService {

    @Override
    public List<UserSearchDTO> searchUsersBy(String query) {
        return List.of();
    }

    @Override
    public List<TimelineTweetDTO> searchTweetsBy(String hashtag) {
        return List.of();
    }
}
