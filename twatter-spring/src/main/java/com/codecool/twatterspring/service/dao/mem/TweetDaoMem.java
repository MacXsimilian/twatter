package com.codecool.twatterspring.service.dao.mem;

import com.codecool.twatterspring.model.Tweet;
import com.codecool.twatterspring.model.dto.IncomingTweetDTO;
import com.codecool.twatterspring.model.dto.TimelineTweetDTO;
import com.codecool.twatterspring.service.dao.TwatterUserDao;
import com.codecool.twatterspring.service.dao.TweetDao;
import com.codecool.twatterspring.service.dao.TweetDaoDb;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;

@Service
@RequiredArgsConstructor
@ConditionalOnMissingBean(TweetDaoDb.class)
@Slf4j
public class TweetDaoMem implements TweetDao {

    private final Map<Long, List<Tweet>> tweets = new HashMap<>();
    private final TwatterUserDao userDao;
    private final Comparator<Tweet> tweetComparator = comparing(Tweet::getDate).reversed();

    @Override
    public Tweet save(IncomingTweetDTO dto, UUID tweetId, Long userId, String username, LocalDateTime postedAt) {
        var tweet = Tweet.fromDTO(dto, tweetId, userId, username, postedAt);
        tweets.putIfAbsent(userId, new LinkedList<>());
        tweets.get(userId).add(tweet);
        return tweet;
    }

    @Override
    public List<TimelineTweetDTO> provideTweetsForUserTimelineBy(Long userId) {
        return tweets.get(userId).stream()
            .sorted(tweetComparator)
            .map(TimelineTweetDTO::fromEntity).collect(Collectors.toList());
    }

    @Override
    public List<TimelineTweetDTO> provideTweetsForHomeTimelineBy(Long userId) {
        return userDao.getFolloweeIdsByUserId(userId).stream()
            .map(tweets::get)
            .reduce(new LinkedList<>(), (t1, t2) -> {
                t1.addAll(t2);
                return t1;
            }).stream()
            .sorted(tweetComparator)
            .map(TimelineTweetDTO::fromEntity)
            .collect(Collectors.toList());
    }

    @Override
    public List<TimelineTweetDTO> provideTweetsForTrendingHashtagBy(List<UUID> ids) {
        return List.of();
    }

    @Override
    public Long getTweetCountBy(Long userId) {
        return null;
    }
}
