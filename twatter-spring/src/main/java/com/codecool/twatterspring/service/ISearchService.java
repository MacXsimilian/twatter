package com.codecool.twatterspring.service;

import com.codecool.twatterspring.model.dto.TimelineTweetDTO;
import com.codecool.twatterspring.model.dto.UserSearchDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ISearchService {
    List<UserSearchDTO> searchUsersBy(String query);
    List<TimelineTweetDTO> searchTweetsBy(String hashtag);
}
