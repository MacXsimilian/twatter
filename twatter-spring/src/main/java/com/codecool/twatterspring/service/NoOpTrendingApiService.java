package com.codecool.twatterspring.service;

import com.codecool.twatterspring.model.dto.Hashtag;
import com.codecool.twatterspring.model.dto.TrendingHashtagsDTO;
import com.codecool.twatterspring.model.dto.TrendingTweetDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@ConditionalOnMissingBean(TrendingApiService.class)
@Slf4j
public class NoOpTrendingApiService implements ITrendingApiService {

    @Override
    public HttpStatus postNewTweet(TrendingTweetDTO tweet) {
        return HttpStatus.OK;
    }

    @Override
    public TrendingHashtagsDTO getTradingHashtagsByTimeInterval(String interval) {
        var t1 = Hashtag.builder().hashtag("test1").count(100000).build();
        var t2 = Hashtag.builder().hashtag("test2").count(89654).build();
        return TrendingHashtagsDTO.builder()
            .trendingHashtags(new Hashtag[]{t1, t2})
            .build();
    }

    @Override
    public String[] getTweetIDsByTrendingHashtag(String hashtag) {
        return new String[0];
    }
}
