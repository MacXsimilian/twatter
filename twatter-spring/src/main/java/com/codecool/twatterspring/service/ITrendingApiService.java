package com.codecool.twatterspring.service;

import com.codecool.twatterspring.model.dto.TrendingHashtagsDTO;
import com.codecool.twatterspring.model.dto.TrendingTweetDTO;
import org.springframework.http.HttpStatus;

public interface ITrendingApiService {
    HttpStatus postNewTweet(TrendingTweetDTO tweet);
    TrendingHashtagsDTO getTradingHashtagsByTimeInterval(String interval);
    String[] getTweetIDsByTrendingHashtag(String hashtag);
}
