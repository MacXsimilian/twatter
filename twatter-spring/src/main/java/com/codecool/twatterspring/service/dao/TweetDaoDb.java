package com.codecool.twatterspring.service.dao;

import com.codecool.twatterspring.model.Tweet;
import com.codecool.twatterspring.model.dto.IncomingTweetDTO;
import com.codecool.twatterspring.model.dto.TimelineTweetDTO;
import com.codecool.twatterspring.repository.TweetRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Transactional
@Profile({"prod", "local_db", "test"})
public class TweetDaoDb implements TweetDao {

    private final TweetRepository tweets;
    private final @Qualifier("twatterUserDaoDb") TwatterUserDao userDao;
    private final EntityManager entityManager;

    @Override
    public Tweet save(IncomingTweetDTO dto, UUID tweetId, Long userId, String username, LocalDateTime postedAt) {
        var tweet = Tweet.fromDTO(dto, tweetId, userId, username, postedAt);
        entityManager.persist(tweet);
        return tweet;
    }

    @Override
    public List<TimelineTweetDTO> provideTweetsForUserTimelineBy(Long userId) {
        var userTweets = tweets.findAllByUserIdOrderByDateDesc(userId);
        return userTweets.stream().map(TimelineTweetDTO::fromEntity).collect(Collectors.toList());
    }

    @Override
    public List<TimelineTweetDTO> provideTweetsForHomeTimelineBy(Long userId) {
        var followeeIds = userDao.getFolloweeIdsByUserId(userId);
        var followeeTweets = tweets.findLimitedNumberOfTweetsByFolloweeIds(followeeIds, userId);
        return followeeTweets.stream().map(TimelineTweetDTO::fromEntity).collect(Collectors.toList());
    }

    @Override
    public List<TimelineTweetDTO> provideTweetsForTrendingHashtagBy(List<UUID> ids) {
        return tweets.findAllByIdInOrderByDateDesc(ids)
            .stream()
            .map(TimelineTweetDTO::fromEntity)
            .collect(Collectors.toList());
    }

    @Override
    public Long getTweetCountBy(Long userId) {
        return tweets.getTweetCountBy(userId).orElse(0L);
    }
}
