package com.codecool.twatterspring.service;

import com.codecool.twatterspring.model.dto.TimelineTweetDTO;
import com.codecool.twatterspring.redis.model.TimelineOwner;
import com.codecool.twatterspring.redis.service.TimelineOwnerDao;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;

@Service
@RequiredArgsConstructor
@Profile({"prod_redis", "dev_redis", "test"})
@Slf4j
public class ProductionTimelineService implements ITimelineService {

    private final TweetService tweetService;
    private final TimelineOwnerDao timelineOwnerDao;
    private final UserService userService;
    private final TransactionTemplate tt;

    @Override
    public List<TimelineTweetDTO> getUserTimeline(Long userId) {
        loadTimelinesIntoRedisIfNotPresent(userId);
        return timelineOwnerDao.getUserTimelineBy(userId);
    }

    @Override
    public List<TimelineTweetDTO> getHomeTimeline(Long userId) {
        loadTimelinesIntoRedisIfNotPresent(userId);
        return timelineOwnerDao.getHomeTimelineBy(userId);
    }

    void loadTimelinesIntoRedisIfNotPresent(Long userId) {
        if (timelineOwnerDao.existsBy(userId))
            return;
        var timelineOwner = loadTimelineOwnerFromDatabase(userId);
        timelineOwnerDao.persist(timelineOwner);
    }

    TimelineOwner loadTimelineOwnerFromDatabase(Long userId) {
        return tt.execute(transactionStatus -> {
            var username = userService.getUsernameByUserId(userId);
            var followerIds = userService.getFollowerIdsByUserId(userId);
            var userTweets = tweetService.provideTweetsForUserTimelineBy(userId);
            var homeTweets = tweetService.provideTweetsForHomeTimelineBy(userId);

            return TimelineOwner.builder()
                .userId(userId)
                .username(username)
                .userTweetDTOS(userTweets)
                .homeTweetDTOS(homeTweets)
                .followers(followerIds)
                .build();
        });
    }
}
