package com.codecool.twatterspring.service.dao.mem;

import com.codecool.twatterspring.model.TwatterUser;
import com.codecool.twatterspring.model.dto.AuthDTO;
import com.codecool.twatterspring.model.dto.SizesDTO;
import com.codecool.twatterspring.service.dao.TwatterUserDao;
import com.codecool.twatterspring.service.dao.TwatterUserDaoDb;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@ConditionalOnMissingBean(TwatterUserDaoDb.class)
@Slf4j
@RequiredArgsConstructor
public class TwatterUserDaoMem implements TwatterUserDao {

    private static long idCounter = 0;

    private final Map<Long, TwatterUser> users = new HashMap<>();
    private final PasswordEncoder passwordEncoder;
    private final Environment environment;

    @Override
    public Optional<TwatterUser> findById(Long id) {
        return Optional.ofNullable(users.get(id));
    }

    @Override
    public Optional<TwatterUser> findBy(String username) {
        return users.values().stream().filter(u -> username.equals(u.getName())).findFirst();
    }

    @Override
    public String getUsernameByUserId(Long userId) {
        return Optional.ofNullable(users.get(userId)).map(TwatterUser::getName).orElse(null);
    }

    @Override
    public Set<Long> getFollowerIdsByUserId(Long userId) {
        return Optional.ofNullable(users.get(userId))
            .map(TwatterUser::getFollowers)
            .orElseThrow()
            .stream().map(TwatterUser::getId).collect(Collectors.toSet());
    }

    @Override
    public Set<Long> getFolloweeIdsByUserId(Long userId) {
        return Optional.ofNullable(users.get(userId))
            .map(TwatterUser::getFollowees)
            .orElseThrow()
            .stream().map(TwatterUser::getId).collect(Collectors.toSet());
    }

    @Override
    public boolean exists(String username) {
        return users.values().stream().anyMatch(u -> username.equals(u.getName()));
    }

    @Override
    public void save(AuthDTO userDTO) {
        var user = createUser(userDTO.getUsername(), userDTO.getPassword(), userDTO.getEmail(), "USER");
        users.put(user.getId(), user);
    }

    @Override
    public void followUser(Long userId, Long followerId) {
        users.get(userId).addFollower(users.get(followerId));
    }

    @Override
    public void unfollowUser(Long userId, Long followerId) {
        users.get(userId).removeFollower(users.get(followerId));
    }

    @Override
    public Optional<SizesDTO> getAssociationSizesForUserBy(Long id) {
        return Optional.empty();
    }

    private TwatterUser createUser(String username, String password, String email, String... roles) {
        return TwatterUser.builder()
            .id(++idCounter)
            .name(username)
            .password(passwordEncoder.encode(password))
            .email(email)
            .registrationDate(LocalDateTime.now())
            .build();
    }

    @PostConstruct
    void afterInit() {
        if (environment.acceptsProfiles(Profiles.of("dev"))) {
            save(AuthDTO.builder()
                .username("user")
                .password(passwordEncoder.encode("password"))
                .email("user@user.com")
                .build());
            log.info("Saved test user " + users.values().stream().findFirst().orElseThrow());
        }
    }
}
