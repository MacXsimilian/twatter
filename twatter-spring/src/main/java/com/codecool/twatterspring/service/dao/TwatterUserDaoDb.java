package com.codecool.twatterspring.service.dao;

import com.codecool.twatterspring.model.TwatterUser;
import com.codecool.twatterspring.model.dto.AuthDTO;
import com.codecool.twatterspring.model.dto.SizesDTO;
import com.codecool.twatterspring.repository.TwatterUserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
@RequiredArgsConstructor
@Profile({"prod", "local_db", "test"})
@Slf4j
public class TwatterUserDaoDb implements TwatterUserDao {

    private final TwatterUserRepository repository;
    private final PasswordEncoder passwordEncoder;
    private final EntityManager entityManager;

    @Override
    public Optional<TwatterUser> findById(Long id) {
        return repository.findById(id);
    }

    @Override
    public Optional<TwatterUser> findBy(String username) {
        return repository.findByName(username);
    }

    @Override
    public String getUsernameByUserId(Long userId) {
        return repository.getUsernameByUserId(userId);
    }

    @Override
    public Set<Long> getFollowerIdsByUserId(Long userId) {
        return repository.getFollowerIdsByUserId(userId);
    }

    @Override
    public Set<Long> getFolloweeIdsByUserId(Long userId) {
        return repository.getFolloweeIdsByUserId(userId);
    }

    @Override
    public boolean exists(String username) {
        return repository.findByName(username).isPresent();
    }

    @Override
    public void save(AuthDTO userDTO) {
        TwatterUser user = TwatterUser.builder()
            .name(userDTO.getUsername())
            .email(userDTO.getEmail())
            .password(passwordEncoder.encode(userDTO.getPassword()))
            .registrationDate(LocalDateTime.now())
            .build();
        repository.save(user);
    }

    @Override
    public void followUser(Long userId, Long followerId) {
        var user = entityManager.getReference(TwatterUser.class, userId);
        var follower = entityManager.getReference(TwatterUser.class, followerId);
        user.addFollower(follower);
    }

    @Override
    public void unfollowUser(Long userId, Long followerId) {
        var user = entityManager.getReference(TwatterUser.class, userId);
        var follower = entityManager.getReference(TwatterUser.class, followerId);
        user.removeFollower(follower);
    }

    @Override
    public Optional<SizesDTO> getAssociationSizesForUserBy(Long id) {
        return repository.getFolloweeAndFollowerCountBy(id);
    }
}
