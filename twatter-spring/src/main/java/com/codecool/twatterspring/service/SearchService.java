package com.codecool.twatterspring.service;

import com.codecool.twatterspring.model.dto.TimelineTweetDTO;
import com.codecool.twatterspring.model.dto.UserSearchDTO;
import com.codecool.twatterspring.repository.TwatterUserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Profile({"prod", "local_db", "test"})
@Slf4j
public class SearchService implements ISearchService {

    private final TwatterUserRepository users;
    private final TweetService tweetService;

    @Override
    public List<UserSearchDTO> searchUsersBy(String query) {
        Pageable pageable = PageRequest.of(0, 10);
        return users.findUsersOrderedByPopularityContainingIgnoreCase(query, pageable)
                .stream()
                .map(UserSearchDTO::fromEntity)
                .collect(Collectors.toList());
    }

    @Override
    public List<TimelineTweetDTO> searchTweetsBy(String hashtag) {
        return tweetService.provideTweetsForTrendingHashtag(hashtag);
    }
}
