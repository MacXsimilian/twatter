package com.codecool.twatterspring.service;

import com.codecool.twatterspring.model.dto.SizesDTO;
import com.codecool.twatterspring.model.dto.TwatterUserProfileDTO;
import com.codecool.twatterspring.redis.service.TimelineOwnerDao;
import com.codecool.twatterspring.security.service.JwtService;
import com.codecool.twatterspring.service.dao.TwatterUserDao;
import com.codecool.twatterspring.service.dao.TweetDao;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class UserService {

    private final TwatterUserDao userDao;
    private final TweetDao tweetDao;
    private final JwtService jwtService;
    private final TimelineOwnerDao timelineOwnerDao;

    private static final String USER_ID_NOT_FOUND = "The given userId does not exist!";

    @Transactional
    public TwatterUserProfileDTO getUserProfileBy(Long userId) {
        var user = userDao.findById(userId).orElseThrow(() -> new EntityNotFoundException(USER_ID_NOT_FOUND));
        SizesDTO sizesDTO = userDao.getAssociationSizesForUserBy(user.getId()).orElseThrow();
        Set<Long> followeeIds = userDao.getFolloweeIdsByUserId(userId);
        return TwatterUserProfileDTO.fromEntity(user, sizesDTO, followeeIds, tweetDao.getTweetCountBy(userId));
    }

    @Transactional
    public TwatterUserProfileDTO getUserProfileBy(String name) {
        var user = userDao.findBy(name).orElseThrow(() -> new EntityNotFoundException(USER_ID_NOT_FOUND));
        SizesDTO sizesDTO = userDao.getAssociationSizesForUserBy(user.getId()).orElseThrow();
        Set<Long> followeeIds = userDao.getFolloweeIdsByUserId(user.getId());
        return TwatterUserProfileDTO.fromEntity(user, sizesDTO, followeeIds, tweetDao.getTweetCountBy(user.getId()));
    }

    public void followUser(String jwt, Long userId) {
        Long followerId = jwtService.parseUserIdFromToken(jwt);
        timelineOwnerDao.followUser(userId, followerId);
        userDao.followUser(userId, followerId);
    }

    public void unfollowUser(String jwt, Long userId) {
        Long followerId = jwtService.parseUserIdFromToken(jwt);
        timelineOwnerDao.unFollowUser(userId, followerId);
        userDao.unfollowUser(userId, followerId);
    }

    public String getUsernameByUserId(Long userId) {
        return userDao.getUsernameByUserId(userId);
    }

    public Set<Long> getFollowerIdsByUserId(Long userId) {
        return userDao.getFollowerIdsByUserId(userId);
    }
}
