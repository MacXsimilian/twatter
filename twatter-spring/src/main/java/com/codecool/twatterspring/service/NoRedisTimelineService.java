package com.codecool.twatterspring.service;

import com.codecool.twatterspring.model.dto.TimelineTweetDTO;
import com.codecool.twatterspring.service.dao.TweetDao;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@ConditionalOnMissingBean(ProductionTimelineService.class)
@RequiredArgsConstructor
@Slf4j
public class NoRedisTimelineService implements ITimelineService {

    private final TweetDao tweetDao;

    @Override
    public List<TimelineTweetDTO> getUserTimeline(Long userId) {
        return tweetDao.provideTweetsForUserTimelineBy(userId);
    }

    @Override
    public List<TimelineTweetDTO> getHomeTimeline(Long userId) {
        return tweetDao.provideTweetsForHomeTimelineBy(userId);
    }
}
