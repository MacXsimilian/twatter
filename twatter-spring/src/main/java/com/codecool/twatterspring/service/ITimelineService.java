package com.codecool.twatterspring.service;

import com.codecool.twatterspring.model.dto.TimelineTweetDTO;

import java.util.List;

public interface ITimelineService {
    List<TimelineTweetDTO> getUserTimeline(Long userId);
    List<TimelineTweetDTO> getHomeTimeline(Long userId);
}
