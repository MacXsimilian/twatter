package com.codecool.twatterspring.util;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class Utils {

    public static String convertDate(LocalDateTime dateTime) {
        return Long.toString(dateTime.toEpochSecond(ZoneOffset.UTC));
    }
}
