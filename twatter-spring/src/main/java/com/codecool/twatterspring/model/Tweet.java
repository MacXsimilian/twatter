package com.codecool.twatterspring.model;

import com.codecool.twatterspring.model.dto.IncomingTweetDTO;
import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Tweet {

    @Id
    private UUID id;

    private Long userId;
    private String username;

    @Column(columnDefinition = "varchar(280)")
    private String content;

    @Column(nullable = false)
    @EqualsAndHashCode.Exclude
    private LocalDateTime date;

    public static Tweet fromDTO(IncomingTweetDTO dto, UUID tweetId, Long userId, String username, LocalDateTime postedAt) {
        return dto == null ? null : Tweet.builder()
            .id(tweetId)
            .userId(userId)
            .username(username)
            .content(dto.getContent())
            .date(postedAt)
            .build();
    }

}
