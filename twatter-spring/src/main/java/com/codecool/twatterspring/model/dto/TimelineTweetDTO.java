package com.codecool.twatterspring.model.dto;

import com.codecool.twatterspring.model.Tweet;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.ZoneOffset;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TimelineTweetDTO {
    private String id;
    private String content;
    private Long userId;
    private String username;
    private String postedAt;

    public static TimelineTweetDTO fromDto(IncomingTweetDTO dto, UUID tweetId, Long userId, String username, String postedAt) {
        return TimelineTweetDTO.builder()
            .id(tweetId.toString())
            .userId(userId)
            .username(username)
            .content(dto.getContent())
            .postedAt(postedAt)
            .build();
    }

    public static TimelineTweetDTO fromEntity(Tweet tweet) {
        return tweet == null ? null : TimelineTweetDTO.builder()
                .id(tweet.getId().toString())
                .content(tweet.getContent())
                .userId(tweet.getUserId())
                .username(tweet.getUsername())
                .postedAt(Long.toString(tweet.getDate().toEpochSecond(ZoneOffset.UTC)))
                .build();
    }
}
