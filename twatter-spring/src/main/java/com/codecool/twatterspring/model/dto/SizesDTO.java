package com.codecool.twatterspring.model.dto;

import lombok.Data;


@Data
public class SizesDTO {
    private final int followeeCount;
    private final int followerCount;
}
