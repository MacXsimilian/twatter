package com.codecool.twatterspring.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class TwatterUser {

    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true)
    private String name;

    @Column(unique = true)
    private String email;

    @Column(nullable = false)
    private LocalDateTime registrationDate;

    @Column(nullable = false)
    private String password;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany
    private Set<TwatterUser> followers;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @ManyToMany(mappedBy = "followers")
    private Set<TwatterUser> followees;


    public void addFollower(TwatterUser user) {
        if (followers == null) followers = new HashSet<>();
        followers.add(user);
        user.addFollowee(this);
    }

    public void removeFollower(TwatterUser follower) {
        followers.removeIf(follower::equals);
        follower.removeFollowee(this);
    }

    void addFollowee(TwatterUser user) {
        if (followees == null) followees = new HashSet<>();
        followees.add(user);
    }

    void removeFollowee(TwatterUser followee) {
        followees.removeIf(followee::equals);
    }
}
