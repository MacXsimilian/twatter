package com.codecool.twatterspring.model.dto;

import com.codecool.twatterspring.model.TwatterUser;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Getter
@EqualsAndHashCode
@Builder
public class TwatterUserProfileDTO {
    private Long id;
    private String username;
    private String email;
    private LocalDateTime registrationDate;
    private Set<Long> followeeIds;
    private int following;
    private int followers;
    private Long tweetCount;

    public static TwatterUserProfileDTO fromEntity(TwatterUser twatterUser, SizesDTO sizesDTO, Set<Long> followeeIds, Long tweetCount) {
        return TwatterUserProfileDTO.builder()
                .id(twatterUser.getId())
                .username(twatterUser.getName())
                .email(twatterUser.getEmail())
                .registrationDate(twatterUser.getRegistrationDate())
                .followeeIds(followeeIds)
                .following(sizesDTO.getFolloweeCount())
                .followers(sizesDTO.getFollowerCount())
                .tweetCount(tweetCount)
                .build();
    }
}
