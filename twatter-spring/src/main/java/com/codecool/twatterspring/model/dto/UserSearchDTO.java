package com.codecool.twatterspring.model.dto;

import com.codecool.twatterspring.model.TwatterUser;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserSearchDTO {

    private Long id;
    private String name;


    public static UserSearchDTO fromEntity(TwatterUser user) {
        return builder().id(user.getId()).name(user.getName()).build();
    }
}
