package com.codecool.twatterspring.config;

import com.codecool.twatterspring.redis.config.Config;
import com.codecool.twatterspring.redis.config.RedisProperties;
import com.codecool.twatterspring.redis.config.TestConfig;
import com.codecool.twatterspring.redis.repository.TimelineOwnerRepository;
import com.codecool.twatterspring.redis.service.TimelineOwnerDao;
import com.codecool.twatterspring.security.config.DevelopmentSecurityConfig;
import com.codecool.twatterspring.security.config.NoOpSecurityConfig;
import com.codecool.twatterspring.security.config.ProductionSecurityConfig;
import com.codecool.twatterspring.service.ISearchService;
import com.codecool.twatterspring.service.ITimelineService;
import com.codecool.twatterspring.service.ITrendingApiService;
import com.codecool.twatterspring.service.dao.TwatterUserDao;
import com.codecool.twatterspring.service.dao.TweetDao;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
@RequiredArgsConstructor
@Slf4j
public class ApplicationContextDiagnostics implements CommandLineRunner {

    private final ApplicationContext context;
    private final Environment env;
    private final List<Class<?>> redisBeans = List.of(
        Config.class, RedisProperties.class, TestConfig.class,
        TimelineOwnerRepository.class,
        TimelineOwnerDao.class
    );
    private final List<Class<?>> securityBeans = List.of(
        DevelopmentSecurityConfig.class, NoOpSecurityConfig.class, ProductionSecurityConfig.class
    );
    private final List<Class<?>> daoBeans = List.of(
        TwatterUserDao.class, TweetDao.class
    );
    private final List<Class<?>> serviceBeans = List.of(
        ISearchService.class, ITimelineService.class, ITrendingApiService.class
    );

    @Override
    public void run(String... args) throws Exception {
        System.out.println("\n" + "-".repeat(50) + "  " +
                           String.join("  ", "DIAGNOSTICS".split("")) +
                           "  " + "-".repeat(50) + "\n");

        log.info("Beginning ApplicationContext diagnostics...");
        log.info("Active profiles: " + Arrays.toString(env.getActiveProfiles()));

        System.out.println();
        log.info("Checking FakeDataInitializer...");
        logIfLoaded(FakeDataInitializer.class);

        System.out.println();
        log.info("Checking Redis beans...");
        redisBeans.forEach(this::logIfLoaded);

        System.out.println();
        log.info("Checking security beans...");
        securityBeans.forEach(this::logIfLoaded);

        System.out.println();
        log.info("Checking DAO beans...");
        daoBeans.forEach(this::logIfLoaded);

        System.out.println();
        log.info("Checking @Service beans...");
        serviceBeans.forEach(this::logIfLoaded);

        System.out.println("\n" + "-".repeat(50) + "  " +
                           String.join("  ", "END OF DIAGNOSTICS".split("")) +
                           "  " + "-".repeat(50) + "\n");
    }

    private boolean hasRepositoryInterface(Class<?> cls) {
        return Arrays.stream(cls.getInterfaces())
            .map(Class::getSimpleName).anyMatch("CrudRepository"::equals);
    }

    private void logIfLoaded(Class<?> cls) {
        var bean = getBeanByClass(cls);
        if (bean == null) return;
        if (hasRepositoryInterface(cls))
            log.info("Loaded " + cls.getName());
        else
            log.info("Loaded " + bean.getClass().getName());
    }

    private <T> T getBeanByClass(Class<T> beanClass) {
        T bean = null;
        try {
            bean = context.getBean(beanClass);
        } catch (NoSuchBeanDefinitionException ignored) {
        }
        return bean;
    }
}
