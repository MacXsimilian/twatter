package com.codecool.twatterspring.config;

import com.codecool.twatterspring.model.TwatterUser;
import com.codecool.twatterspring.model.dto.IncomingTweetDTO;
import com.codecool.twatterspring.repository.TwatterUserRepository;
import com.codecool.twatterspring.service.dao.TweetDao;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@Slf4j
@Component
@Profile("local_db")
public class FakeDataInitializer implements CommandLineRunner {

    private final TwatterUserRepository users;
    private final TweetDao tweetDao;
    private final PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) throws Exception {

        var trump = buildUser("trump");
        var obama = buildUser("obama");
        var alexJones = buildUser("alexjones");

        trump.addFollower(obama);
        trump.addFollower(alexJones);
        alexJones.addFollower(trump);

        users.saveAll(List.of(trump, obama, alexJones));

        trump = users.findByIdWithFollowers(1L).orElseThrow();

        log.info("Persisted user " + trump + " with followers.");
        log.info("Followers: " + trump.getFollowers());

        var incoming1 = IncomingTweetDTO.builder().content("Test tweet 1 in application runner").build();
        var incoming2 = IncomingTweetDTO.builder().content("Test tweet 2 in application runner").build();
        tweetDao.save(incoming1, UUID.randomUUID(), trump.getId(), trump.getName(), LocalDateTime.now());
        tweetDao.save(incoming2, UUID.randomUUID(), trump.getId(), trump.getName(), LocalDateTime.now());
        var tweets = tweetDao.provideTweetsForUserTimelineBy(trump.getId());

        log.info("Persisted two tweets: " + tweets);
    }

    private TwatterUser buildUser(String name) {
        return TwatterUser.builder()
            .name(name)
            .password(passwordEncoder.encode(name))
            .registrationDate(LocalDateTime.now())
            .build();
    }
}
