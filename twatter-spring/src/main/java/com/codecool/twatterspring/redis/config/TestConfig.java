package com.codecool.twatterspring.redis.config;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import redis.clients.jedis.Jedis;
import redis.embedded.RedisServer;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@TestConfiguration
@Profile("test")
public class TestConfig {

    private final RedisProperties redisProperties;
    private final RedisServer redisServer;

    public TestConfig(RedisProperties redisProperties) {
        this.redisProperties = redisProperties;
        this.redisServer = new RedisServer(redisProperties.getRedisPort());
    }

    @PostConstruct
    public void postConstruct() {
        redisServer.start();
    }

    @PreDestroy
    public void preDestroy() {
        redisServer.stop();
    }

    @Bean
    Jedis jedis() {
        return new Jedis(redisProperties.getRedisHost(), redisProperties.getRedisPort());
    }
}
