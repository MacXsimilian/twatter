package com.codecool.twatterspring.redis.service;

import com.codecool.twatterspring.model.dto.IncomingTweetDTO;
import com.codecool.twatterspring.model.dto.TimelineTweetDTO;
import com.codecool.twatterspring.redis.model.TimelineOwner;
import com.codecool.twatterspring.redis.repository.TimelineOwnerRepository;
import com.codecool.twatterspring.service.dao.TweetDao;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Profile({"prod_redis", "dev_redis", "test"})
@Slf4j
public class TimelineOwnerDaoRedis implements TimelineOwnerDao {

    private final static String NO_SUCH_USER_MESSAGE = "No user exists with given ID.";

    private final TimelineOwnerRepository timelineOwnerRepository;
    private final TweetDao tweetDao;

    @Override
    public void persist(Long userId, String username) {
        var timeline = TimelineOwner.builder().userId(userId).username(username).build();
        timelineOwnerRepository.save(timeline);
    }

    @Override
    public void persist(TimelineOwner timelineOwner) {
        timelineOwnerRepository.save(timelineOwner);
    }

    @Override
    public boolean existsBy(Long userId) {
        return timelineOwnerRepository.existsById(userId);
    }

    @Override
    public Optional<TimelineOwner> findByUserId(Long userId) {
        return timelineOwnerRepository.findById(userId);
    }

    @Override
    public void followUser(Long userId, Long followerId) {
        if (existsBy(userId)) {
            followUserInRedis(userId, followerId);
        } else {
            complementHomeTimelineThroughDbQuery(userId, followerId);
        }
    }

    @Override
    public void unFollowUser(Long userId, Long followerId) {
        var follower = getUserBy(followerId);
        follower.removeHomeTweetsBy(userId);
        if (existsBy(userId)) {
            unfollowUserInRedis(userId, follower);
        } else {
            timelineOwnerRepository.save(follower);
        }
    }

    @Override
    public void saveNewTweet(IncomingTweetDTO incomingTweetDTO, UUID tweetId, Long userId, String postedAt) {
        var tweeter = getUserBy(userId);
        var timelineTweet = TimelineTweetDTO
            .fromDto(incomingTweetDTO, tweetId, userId, tweeter.getUsername(), postedAt);
        tweeter.addUserTweet(timelineTweet);
        timelineOwnerRepository.save(tweeter);
        updateHomeTimelinesOfFollowers(timelineTweet, tweeter.getFollowers());
    }

    @Override
    public List<TimelineTweetDTO> getUserTimelineBy(Long userId) {
        return sorted(getUserBy(userId).getUserTweetDTOS());
    }

    @Override
    public List<TimelineTweetDTO> getHomeTimelineBy(Long userId) {
        return sorted(getUserBy(userId).getHomeTweetDTOS());
    }

    private void followUserInRedis(Long userId, Long followerId) {
        var user = getUserBy(userId);
        user.addFollower(followerId);
        var follower = getUserBy(followerId);
        follower.addHomeTweets(user.getUserTweetDTOS());
        timelineOwnerRepository.saveAll(List.of(user, follower));
    }

    private void complementHomeTimelineThroughDbQuery(Long userId, Long followerId) {
        var follower = getUserBy(followerId);
        var userTweets = tweetDao.provideTweetsForUserTimelineBy(userId);
        follower.addHomeTweets(userTweets);
        timelineOwnerRepository.save(follower);
    }

    private void unfollowUserInRedis(Long userId, TimelineOwner follower) {
        var user = getUserBy(userId);
        user.removeFollower(follower.getUserId());
        timelineOwnerRepository.saveAll(List.of(user, follower));
    }

    private TimelineOwner getUserBy(Long userId) {
        return timelineOwnerRepository.findById(userId)
            .orElseThrow(() -> new EntityNotFoundException(NO_SUCH_USER_MESSAGE));
    }

    private void updateHomeTimelinesOfFollowers(TimelineTweetDTO timelineTweetDTO, Set<Long> followerIds) {
        var timelines = timelineOwnerRepository.findAllById(followerIds);
        timelines.forEach(timelineOwner -> timelineOwner.addHomeTweet(timelineTweetDTO));
        timelineOwnerRepository.saveAll(timelines);
    }

    private List<TimelineTweetDTO> sorted(List<TimelineTweetDTO> timeline) {
        return timeline.stream()
            .sorted((o1, o2) -> (int) (Long.parseLong(o2.getPostedAt()) - Long.parseLong(o1.getPostedAt())))
            .collect(Collectors.toList());
    }
}
