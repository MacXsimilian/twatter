package com.codecool.twatterspring.redis.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Getter
@Profile({"prod_redis", "dev_redis", "test"})
public class RedisProperties {

    private final int redisPort;
    private final String redisHost;

    public RedisProperties(@Value("${spring.redis.port}") int redisPort,
                           @Value("${spring.redis.host}") String redisHost)
    {
        this.redisPort = redisPort;
        this.redisHost = redisHost;
    }

}
