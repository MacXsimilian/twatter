package com.codecool.twatterspring.redis.repository;

import com.codecool.twatterspring.redis.model.TimelineOwner;
import org.springframework.context.annotation.Profile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@Profile({"prod_redis", "dev_redis", "test"})
public interface TimelineOwnerRepository extends CrudRepository<TimelineOwner, Long> {
}
