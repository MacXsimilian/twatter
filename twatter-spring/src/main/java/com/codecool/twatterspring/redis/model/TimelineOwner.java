package com.codecool.twatterspring.redis.model;

import com.codecool.twatterspring.model.dto.TimelineTweetDTO;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;
import java.util.*;

@AllArgsConstructor
@Data
@Builder
@RedisHash(value = "timeline", timeToLive = 7 * 24 * 60 * 60)
public class TimelineOwner implements Serializable {

    private final static String NO_SUCH_FOLLOWER_MESSAGE = "No follower exists with given ID.";

    @Id
    private Long userId;
    private String username;

    /*
    Tell lombok not to generate getters for these collection fields,
        so that we can implement custom getters.
    
    Reason: If a collection inside a @RedisHash is empty, it will be
        represented as 'null' in Java, but we don't want to deal with
        'null' values, so we shall return an unmodifiable empty list.
     */
    @Getter(AccessLevel.NONE)
    private List<TimelineTweetDTO> homeTweetDTOS;

    @Getter(AccessLevel.NONE)
    private List<TimelineTweetDTO> userTweetDTOS;

    @Getter(AccessLevel.NONE)
    private Set<Long> followers;

    public void addHomeTweets(List<TimelineTweetDTO> timelineTweetDTOS) {
        if (Objects.isNull(homeTweetDTOS))
            homeTweetDTOS = new LinkedList<>();
        homeTweetDTOS.addAll(timelineTweetDTOS);
    }

    public void addHomeTweet(TimelineTweetDTO timelineTweetDTO) {
        addHomeTweets(List.of(timelineTweetDTO));
    }

    public void removeHomeTweetsBy(Long userId) {
        if (homeTweetDTOS != null)
            homeTweetDTOS.removeIf(tweet -> userId.equals(tweet.getUserId()));
    }

    public void addUserTweet(TimelineTweetDTO timelineTweetDTO) {
        if (Objects.isNull(userTweetDTOS))
            userTweetDTOS = new LinkedList<>();
        userTweetDTOS.add(timelineTweetDTO);

        if (Objects.isNull(homeTweetDTOS))
            homeTweetDTOS = new LinkedList<>();
        homeTweetDTOS.add(timelineTweetDTO);
    }

    public void addFollower(Long followerId) {
        if (Objects.isNull(followers))
            followers = new HashSet<>();
        followers.add(followerId);
    }

    public void removeFollower(Long followerId) {
        if (hasFollower(followerId)) followers.remove(followerId);
        else throw new IllegalArgumentException(NO_SUCH_FOLLOWER_MESSAGE);
    }

    public boolean hasFollower(Long followerId) {
        return !Objects.isNull(followers) && followers.contains(followerId);
    }

    public List<TimelineTweetDTO> getHomeTweetDTOS() {
        return Optional.ofNullable(homeTweetDTOS).orElseGet(List::of);
    }

    public List<TimelineTweetDTO> getUserTweetDTOS() {
        return Optional.ofNullable(userTweetDTOS).orElseGet(List::of);
    }

    public Set<Long> getFollowers() {
        return followers != null ? followers : Set.of();
    }
}
