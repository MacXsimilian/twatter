package com.codecool.twatterspring.redis.service;

import com.codecool.twatterspring.model.dto.IncomingTweetDTO;
import com.codecool.twatterspring.model.dto.TimelineTweetDTO;
import com.codecool.twatterspring.redis.model.TimelineOwner;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@ConditionalOnMissingBean(TimelineOwnerDaoRedis.class)
@Slf4j
public class NoOpTimelineOwnerDao implements TimelineOwnerDao {

    @Override
    public void persist(Long userId, String username) {

    }

    @Override
    public void persist(TimelineOwner timelineOwner) {

    }

    @Override
    public boolean existsBy(Long userId) {
        return false;
    }

    @Override
    public Optional<TimelineOwner> findByUserId(Long userId) {
        return Optional.empty();
    }

    @Override
    public void followUser(Long userId, Long followerId) {

    }

    @Override
    public void unFollowUser(Long userId, Long followerId) {

    }

    @Override
    public void saveNewTweet(IncomingTweetDTO incomingTweetDTO, UUID tweetId, Long userId, String postedAt) {

    }

    @Override
    public List<TimelineTweetDTO> getUserTimelineBy(Long userId) {
        return null;
    }

    @Override
    public List<TimelineTweetDTO> getHomeTimelineBy(Long userId) {
        return null;
    }
}
