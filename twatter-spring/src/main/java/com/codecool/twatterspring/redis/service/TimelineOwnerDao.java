package com.codecool.twatterspring.redis.service;

import com.codecool.twatterspring.model.dto.IncomingTweetDTO;
import com.codecool.twatterspring.model.dto.TimelineTweetDTO;
import com.codecool.twatterspring.redis.model.TimelineOwner;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public interface TimelineOwnerDao {

    void persist(Long userId, String username);
    void persist(TimelineOwner timelineOwner);
    boolean existsBy(Long userId);
    Optional<TimelineOwner> findByUserId(Long userId);
    void followUser(Long userId, Long followerId);
    void unFollowUser(Long userId, Long followerId);
    void saveNewTweet(IncomingTweetDTO incomingTweetDTO, UUID tweetId, Long userId, String postedAt);
    List<TimelineTweetDTO> getUserTimelineBy(Long userId);
    List<TimelineTweetDTO> getHomeTimelineBy(Long userId);
}
