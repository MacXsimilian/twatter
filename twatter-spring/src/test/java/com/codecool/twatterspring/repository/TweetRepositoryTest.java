package com.codecool.twatterspring.repository;

import com.codecool.twatterspring.redis.repository.TimelineOwnerRepository;
import com.codecool.twatterspring.util.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.UUID;

import static com.codecool.twatterspring.util.TestUtils.buildTweet;
import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class TweetRepositoryTest {

    @Autowired
    private EntityManager entityManager;
    
    @Autowired
    private TweetRepository tweets;

    @Autowired
    private TwatterUserRepository users;

    @MockBean
    private TimelineOwnerRepository timelineOwnerRepository;

    @BeforeEach
    public void init() {
        var elon = TestUtils.buildUserWithEmail("elon", "password");
        var trump = TestUtils.buildUserWithEmail("trump", "password");
        var gergo = TestUtils.buildUserWithEmail("gergo", "password");
        var elek = TestUtils.buildUserWithEmail("elek", "password");

        elon.addFollower(gergo);
        elon.addFollower(elek);
        trump.addFollower(gergo);
        gergo.addFollower(elek);

        users.saveAll(List.of(elon, trump, gergo, elek));
        entityManager.flush();
    }

    @Test
    public void initTest() {
        assertThat(users).isNotNull();
        assertThat(tweets).isNotNull();
        assertThat(users.findAll()).hasSize(4);
    }

    @Test
    public void saveNewTweetTest() {
        var tweet = buildTweet(1L, 0L);
        tweet = tweets.save(tweet);
        entityManager.flush();
        assertThat(tweets.findAll()).hasSize(1).containsExactly(tweet);
    }

    @Test
    public void getAllTweetsOfGivenUserTest() {
        var tweet1 = buildTweet(1L, 0L);
        var tweet2 = buildTweet(1L, 1L);
        var tweet3 = buildTweet(2L, 0L);
        tweets.saveAll(List.of(tweet1, tweet2, tweet3));
        entityManager.flush();

        assertThat(tweets.findAllByUserIdOrderByDateDesc(1L)).hasSize(2).containsExactlyInAnyOrder(tweet1, tweet2);
        assertThat(tweets.findAllByUserIdOrderByDateDesc(2L)).hasSize(1).containsExactlyInAnyOrder(tweet3);
        assertThat(tweets.findAllByUserIdOrderByDateDesc(3L)).hasSize(0);
        assertThat(tweets.findAllByUserIdOrderByDateDesc(4L)).hasSize(0);

    }

    @Test
    public void getAllTweetsOfUsersFolloweesTest() {
        var tweet1 = buildTweet(1L, 0L);
        var tweet2 = buildTweet(1L, 1L);
        var tweet3 = buildTweet(2L, 0L);
        var tweet4 = buildTweet(2L, 1L);
        
        tweets.saveAll(List.of(tweet1, tweet2, tweet3, tweet4));
        entityManager.flush();

        List<Long> followeeIDs1 = List.of(1L, 2L, 3L);
        List<Long> followeeIDs2 = List.of(1L, 3L);
        List<Long> followeeIDs3 = List.of(4L, 3L);

        assertThat(tweets.findLimitedNumberOfTweetsByFolloweeIds(followeeIDs1, 4L))
                .hasSize(4)
                .containsExactlyInAnyOrder(tweet1, tweet2, tweet3, tweet4);
        assertThat(tweets.findLimitedNumberOfTweetsByFolloweeIds(followeeIDs2, 3L))
                .hasSize(2)
                .containsExactlyInAnyOrder(tweet1, tweet2);
        assertThat(tweets.findLimitedNumberOfTweetsByFolloweeIds(followeeIDs3, 1L))
                .hasSize(2)
                .containsExactlyInAnyOrder(tweet1, tweet2);
    }

    @Test
    public void findByIdInWhenIdListIsEmptyTest() {
        var tweet1 = buildTweet(1L, 0L);
        var tweet2 = buildTweet(1L, 1L);
        var tweet3 = buildTweet(2L, 0L);
        var tweet4 = buildTweet(2L, 1L);

        tweets.saveAll(List.of(tweet1, tweet2, tweet3, tweet4));
        entityManager.flush();

        assertThat(tweets.findAllByIdInOrderByDateDesc(List.of())).hasSize(0);
    }

    @Test
    public void findByIdInWhenIdListContainsInvalidIdsTest() {
        var tweet1 = buildTweet(1L, 0L);
        var tweet2 = buildTweet(1L, 1L);
        var tweet3 = buildTweet(2L, 0L);
        var tweet4 = buildTweet(2L, 1L);

        tweets.saveAll(List.of(tweet1, tweet2, tweet3, tweet4));
        entityManager.flush();

        assertThat(tweets.findAllByIdInOrderByDateDesc(List.of(UUID.randomUUID(), UUID.randomUUID()))).hasSize(0);
    }

    @Test
    public void findByIdInWhenIdListContainsValidIdsTest() {
        var tweet1 = buildTweet(1L, 0L);
        var tweet2 = buildTweet(1L, 1L);
        var tweet3 = buildTweet(2L, 0L);
        var tweet4 = buildTweet(2L, 1L);

        tweets.saveAll(List.of(tweet1, tweet2, tweet3, tweet4));
        entityManager.flush();

        assertThat(tweets.findAllByIdInOrderByDateDesc(List.of(tweet1.getId(), tweet4.getId())))
                .hasSize(2)
                .containsExactlyInAnyOrder(tweet1, tweet4);
    }

    @Test
    public void testTweetCountIfNoMatch() {
        var tweet1 = buildTweet(1L, 0L);
        var tweet2 = buildTweet(1L, 1L);
        var tweet3 = buildTweet(2L, 0L);
        var tweet4 = buildTweet(2L, 1L);

        tweets.saveAll(List.of(tweet1, tweet2, tweet3, tweet4));
        entityManager.flush();

        assertThat(tweets.getTweetCountBy(3L).get()).isEqualTo(0L);

    }

    @Test
    public void testTweetCountIfTwoMatches() {
        var tweet1 = buildTweet(1L, 0L);
        var tweet2 = buildTweet(1L, 1L);
        var tweet3 = buildTweet(2L, 0L);
        var tweet4 = buildTweet(2L, 1L);

        tweets.saveAll(List.of(tweet1, tweet2, tweet3, tweet4));
        entityManager.flush();

        assertThat(tweets.getTweetCountBy(1L).get()).isEqualTo(2L);
    }

}
