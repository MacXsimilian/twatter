package com.codecool.twatterspring.repository;

import com.codecool.twatterspring.model.TwatterUser;
import com.codecool.twatterspring.redis.repository.TimelineOwnerRepository;
import com.codecool.twatterspring.util.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class TwatterUserRepositoryTest {

    @Autowired
    private TwatterUserRepository users;

    @MockBean
    private TimelineOwnerRepository timelineOwnerRepository;

    @Autowired
    private EntityManager entityManager;

    @BeforeEach
    public void init() {

        var elon = TestUtils.buildUserWithEmail("elon", "password");
        var trump = TestUtils.buildUserWithEmail("trump", "password");
        var gergo = TestUtils.buildUserWithEmail("gergo", "password");
        var elek = TestUtils.buildUserWithEmail("elek", "password");

        elon.addFollower(gergo);
        elon.addFollower(elek);
        trump.addFollower(gergo);
        gergo.addFollower(elek);

        users.saveAll(List.of(elon, trump, gergo, elek));
        entityManager.flush();
    }

    @Test
    public void initTest() {
        assertThat(users).isNotNull();
        assertThat(users.findAll()).hasSize(4);
    }

    @Test
    public void testFindByName() {
        TwatterUser user = users.findByName("elon").orElse(new TwatterUser());
        assertThat(user.getId()).isEqualTo(1L);
    }

    @Test
    public void testGetUsernameByUserId() {
        assertThat(users.getUsernameByUserId(1L)).isEqualTo("elon");
    }

    @Test
    public void testUserHasTwoFollowees() {
        assertThat(users.getFolloweesByUserId(3L)).hasSize(2);
    }

    @Test
    void testGetFolloweeIdsById() {
        assertThat(users.getFolloweeIdsByUserId(3L)).containsExactlyInAnyOrder(1L, 2L);
    }

    @Test
    void testGetFolloweeAndFollowerCountById() {
        var optionalSizesDTO = users.getFolloweeAndFollowerCountBy(1L);
        assertThat(optionalSizesDTO).isPresent();
        var sizesDTO = optionalSizesDTO.get();
        assertThat(sizesDTO.getFolloweeCount()).isEqualTo(0);
        assertThat(sizesDTO.getFollowerCount()).isEqualTo(2);
    }
}