package com.codecool.twatterspring.controller;

import com.codecool.twatterspring.model.TwatterUser;
import com.codecool.twatterspring.redis.config.TestConfig;
import com.codecool.twatterspring.repository.TwatterUserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import redis.clients.jedis.Jedis;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static com.codecool.twatterspring.util.TestUtils.*;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(classes = TestConfig.class, properties = {"spring.redis.host=localhost", "spring.redis.port=6370"})
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class TweetControllerIntegrationTest {

    @Autowired
    private Jedis jedis;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private TwatterUserRepository userRepository;
    @Autowired
    private EntityManager entityManager;

    private Map<Long, TwatterUser> userMap;

    @BeforeEach
    void setUp() {
        var arnold = buildUser("arnold", enc("arnold1"));
        var sly = buildUser("sly", enc("sly1234"));
        var jason = buildUser("jason", enc("jason12"));

        arnold.addFollower(sly);
        arnold.addFollower(jason);
        sly.addFollower(arnold);
        sly.addFollower(jason);
        jason.addFollower(arnold);

        userRepository.saveAll(List.of(arnold, sly, jason));

        var users = (Collection<TwatterUser>) userRepository.findAll();
        userMap = users.stream().collect(toMap(TwatterUser::getId, identity()));
        assertEquals("arnold", userMap.get(1L).getName());
        assertEquals("sly", userMap.get(2L).getName());
        assertEquals("jason", userMap.get(3L).getName());

        var arnoldFollowers = userRepository.getFollowersByUserId(1L);
        var arnoldFollowees = userRepository.getFolloweesByUserId(1L);
        var slyFollowers = userRepository.getFollowersByUserId(2L);
        var slyFollowees = userRepository.getFolloweesByUserId(2L);
        var jasonFollowers = userRepository.getFollowersByUserId(3L);
        var jasonFollowees = userRepository.getFolloweesByUserId(3L);

        assertThat(mapped(arnoldFollowers, NAME_MAPPER)).containsExactly("sly", "jason");
        assertThat(mapped(arnoldFollowees, NAME_MAPPER)).containsExactly("sly", "jason");
        assertThat(mapped(slyFollowers, NAME_MAPPER)).containsExactly("arnold", "jason");
        assertThat(mapped(slyFollowees, NAME_MAPPER)).containsExactly("arnold");
        assertThat(mapped(jasonFollowers, NAME_MAPPER)).containsExactly("arnold");
        assertThat(mapped(jasonFollowees, NAME_MAPPER)).containsExactly("arnold", "sly");
    }

    @AfterEach
    void tearDown() {
        jedis.flushDB();
    }

    @Test
    void smokeTest() {

    }

    private String enc(String password) {
        return passwordEncoder.encode(password);
    }
}