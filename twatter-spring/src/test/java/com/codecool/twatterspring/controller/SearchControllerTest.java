package com.codecool.twatterspring.controller;

import com.codecool.twatterspring.config.TwatterWebMvcTest;
import com.codecool.twatterspring.model.dto.TimelineTweetDTO;
import com.codecool.twatterspring.model.dto.UserSearchDTO;
import com.codecool.twatterspring.security.service.JwtService;
import com.codecool.twatterspring.service.SearchService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import javax.servlet.http.Cookie;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TwatterWebMvcTest(controllers = SearchController.class)
@ActiveProfiles("test")
public class SearchControllerTest {

    private static final String urlPrefix = "/api/search";

    @Autowired
    MockMvc mockMvc;

    @MockBean
    SearchService searchService;

    @MockBean
    JwtService jwtService;

    private String queryString;
    private List<UserSearchDTO> hits;
    private String hitsInJson;
    private List<TimelineTweetDTO> tweetHits;
    private String tweetHitsInJson;

    private final ObjectMapper mapper = new ObjectMapper();

    @BeforeEach
    public void init() throws JsonProcessingException {
        this.queryString = "Trump";

        var hit1 = UserSearchDTO.builder().id(1L).name("theRealDonaldTrump").build();
        var hit2 = UserSearchDTO.builder().id(3L).name("TrumpOrganization").build();
        this.hits = List.of(hit1, hit2);

        var tweetHit1 = TimelineTweetDTO.builder()
                .content("#Trump is funny")
                .id(UUID.randomUUID().toString())
                .build();
        var tweetHit2 = TimelineTweetDTO.builder()
                .content("#Trump Makes America Great Again")
                .id(UUID.randomUUID().toString())
                .build();
        this.tweetHits = List.of(tweetHit1, tweetHit2);

        this.hitsInJson = mapper.writeValueAsString(this.hits);
        this.tweetHitsInJson = mapper.writeValueAsString(this.tweetHits);

    }

    @Test
    public void testSearchFromSearchBox() throws Exception {
        when(searchService.searchUsersBy(queryString)).thenReturn(hits);

        mockMvc.perform(get(urlPrefix + "?q=" + queryString)
                    .cookie(new Cookie("JWT", "tóken")))
                .andExpect(status().isOk())
                .andExpect(content().json(hitsInJson));
    }

    @Test
    public void testSearchFromSearchBoxPost() throws Exception {
        when(searchService.searchUsersBy(queryString)).thenReturn(hits);

        var incoming = new HashMap<>();
        incoming.put("query", queryString);
        mockMvc.perform(post(urlPrefix + "/post")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsString(incoming))
                    .cookie(new Cookie("JWT", "tóken")))
                .andExpect(status().isOk())
                .andExpect(content().json(hitsInJson));
    }

    @Test
    public void testTweetSearchByHashtag() throws Exception {
        when(searchService.searchTweetsBy(queryString)).thenReturn(tweetHits);

        mockMvc.perform(get(urlPrefix + "/tweets")
                .accept(MediaType.APPLICATION_JSON)
                .queryParam("hashtag", queryString)
                .cookie(new Cookie("JWT", "tóken")))
                .andExpect(status().isOk())
                .andExpect(content().json(tweetHitsInJson));
    }
}
