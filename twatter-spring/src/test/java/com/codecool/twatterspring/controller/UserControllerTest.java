package com.codecool.twatterspring.controller;

import com.codecool.twatterspring.config.TwatterWebMvcTest;
import com.codecool.twatterspring.model.dto.SizesDTO;
import com.codecool.twatterspring.model.dto.TwatterUserProfileDTO;
import com.codecool.twatterspring.security.service.JwtService;
import com.codecool.twatterspring.service.UserService;
import com.codecool.twatterspring.util.TestUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import javax.servlet.http.Cookie;
import java.util.HashMap;
import java.util.Set;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TwatterWebMvcTest(controllers = UserController.class)
@ActiveProfiles("test")
public class UserControllerTest {

    private static final String urlPrefix = "/api/users/";

    @Autowired
    MockMvc mockMvc;

    @MockBean
    UserService userService;

    @MockBean
    JwtService jwtService;

    private TwatterUserProfileDTO profileHit;
    private ObjectMapper mapper = new ObjectMapper();

    @BeforeEach
    public void init() {
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        var profile = TestUtils
                .buildUserWithEmail("elon_musk", "tesla");
        this.profileHit = TwatterUserProfileDTO
                .fromEntity(
                        profile,
                        new SizesDTO(4,25),
                        Set.of(1L, 3L, 10L, 12L),
                        20L
                );
    }

    @Test
    public void testGetProfileById() throws Exception {
        when(userService.getUserProfileBy(1L)).thenReturn(profileHit);

        mockMvc.perform(get(urlPrefix + "1")
                    .accept(MediaType.APPLICATION_JSON)
                    .cookie(new Cookie("JWT", "tóken")))
                .andExpect(status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(profileHit)));
    }

    @Test
    public void testGetProfileByName() throws Exception {
        when(userService.getUserProfileBy("elon_musk")).thenReturn(profileHit);

        mockMvc.perform(get(urlPrefix + "byUsername/elon_musk")
                    .cookie(new Cookie("JWT", "tóken")))
                .andExpect(status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(profileHit)));
    }

    @Test
    public void testNewFollowing() throws Exception {
        var incoming = new HashMap<String, Long>();
        incoming.put("followeeId", 1L);

        mockMvc.perform(post(urlPrefix + "followees")
                    .cookie(new Cookie("JWT", "tóken"))
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(mapper.writeValueAsString(incoming)))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteFollowing() throws Exception {
        mockMvc.perform(delete(urlPrefix + "followees/1")
                    .cookie(new Cookie("JWT", "tóken")))
                .andExpect(status().isOk());
    }
}
