package com.codecool.twatterspring.controller;

import com.codecool.twatterspring.config.TwatterWebMvcTest;
import com.codecool.twatterspring.model.dto.IncomingTweetDTO;
import com.codecool.twatterspring.model.dto.TimelineTweetDTO;
import com.codecool.twatterspring.security.service.JwtService;
import com.codecool.twatterspring.service.TweetService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import javax.servlet.http.Cookie;
import java.time.LocalDateTime;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TwatterWebMvcTest(controllers = TweetController.class)
@ActiveProfiles("test")
class TweetControllerTest {

    private static final String urlPrefix = "/api/tweets";

    @Autowired
    MockMvc mockMvc;

    @MockBean
    TweetService tweetService;

    @MockBean
    JwtService jwtService;

    private final ObjectMapper mapper = new ObjectMapper();

    @Test
    void whenNewTweetArrivesInCorrectFormat_ThenReturnsOutgoingTweet_WithStatusOK() throws Exception {
        var incoming = IncomingTweetDTO.builder()
            .content("content")
            .build();

        UUID tweetId = UUID.randomUUID();

        var outgoing = TimelineTweetDTO.builder()
            .id(tweetId.toString())
            .postedAt(LocalDateTime.now().toString())
            .build();
        when(tweetService.handleNewTweet(incoming, tweetId, "tóken")).thenReturn(outgoing);

        when(tweetService.handleNewTweet(any(IncomingTweetDTO.class), any(UUID.class), anyString())).thenReturn(outgoing);

        var incomingAsJson = mapper.writeValueAsString(incoming);

        mockMvc
            .perform(post(urlPrefix)
                .contentType(MediaType.APPLICATION_JSON)
                .content(incomingAsJson)
                .cookie(new Cookie("JWT", "tóken")))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").exists())
            .andExpect(jsonPath("$.postedAt").exists());
    }
}