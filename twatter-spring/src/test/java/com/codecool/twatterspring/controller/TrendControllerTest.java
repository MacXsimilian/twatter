package com.codecool.twatterspring.controller;

import com.codecool.twatterspring.config.TwatterWebMvcTest;
import com.codecool.twatterspring.model.dto.Hashtag;
import com.codecool.twatterspring.model.dto.TrendingHashtagsDTO;
import com.codecool.twatterspring.security.service.JwtService;
import com.codecool.twatterspring.service.TrendingApiService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import javax.servlet.http.Cookie;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@TwatterWebMvcTest(controllers = TrendController.class)
@ActiveProfiles("test")
public class TrendControllerTest {

    private static final String urlPrefix = "/api/trends";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TrendingApiService trendingApiService;

    @MockBean
    private JwtService jwtService;

    private ObjectMapper mapper = new ObjectMapper();

    @Test
    public void testGetTrendingHashtagsBy() throws Exception {
        var hashtag1 = Hashtag.builder().hashtag("#esbringa").count(100).build();
        var hashtag2 = Hashtag.builder().hashtag("szép").count(67).build();
        var result = TrendingHashtagsDTO.builder()
                .timeFilter("daily")
                .trendingHashtags(new Hashtag[]{hashtag1, hashtag2})
                .build();

        when(trendingApiService.getTradingHashtagsByTimeInterval("daily")).thenReturn(result);

        mockMvc.perform(get(urlPrefix + "/daily")
                        .cookie(new Cookie("JWT", "tóken")))
                .andExpect(status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(result)));
    }

}
