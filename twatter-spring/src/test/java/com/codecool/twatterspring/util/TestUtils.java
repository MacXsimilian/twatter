package com.codecool.twatterspring.util;

import com.codecool.twatterspring.model.TwatterUser;
import com.codecool.twatterspring.model.Tweet;
import com.codecool.twatterspring.model.dto.TimelineTweetDTO;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TestUtils {

    public static final Function<TwatterUser, String> NAME_MAPPER = TwatterUser::getName;

    public static TwatterUser buildUser(String name, String password) {
        return TwatterUser.builder()
            .name(name)
            .password(password)
            .registrationDate(LocalDateTime.now())
            .build();
    }

    public static TwatterUser buildUserWithEmail(String name, String password) {
        return TwatterUser.builder()
            .name(name)
            .password(password)
            .registrationDate(LocalDateTime.now())
            .email(name + "@" + name + ".com")
            .build();
    }

    public static TwatterUser buildUserWithEmail(String name, String password, PasswordEncoder encoder) {
        var user = buildUserWithEmail(name, null);
        user.setPassword(encoder.encode(password));
        return user;
    }

    public static <T> List<T> mapped(List<TwatterUser> users, Function<TwatterUser, T> mapper) {
        return users.stream().map(mapper).collect(Collectors.toList());
    }

    public static Tweet buildTweet(TwatterUser user, long plusSeconds) {
        var tweetId = UUID.randomUUID();
        return Tweet.builder()
            .content(user.getName() + "-" + tweetId.toString())
            .date(LocalDateTime.now().plusSeconds(plusSeconds))
            .userId(user.getId())
            .username(user.getName())
            .id(tweetId)
            .build();
    }

    public static Tweet buildTweet(Long userId, long plusSeconds) {
        var tweetId = UUID.randomUUID();
        return Tweet.builder()
            .content("user" + userId + "-" + tweetId.toString())
            .date(LocalDateTime.now().plusSeconds(plusSeconds))
            .userId(userId)
            .username("user" + userId)
            .id(tweetId)
            .build();
    }

    public static List<TimelineTweetDTO> toDTOs(List<Tweet> tweets) {
        return tweets.stream().map(TimelineTweetDTO::fromEntity).collect(Collectors.toList());
    }

}
