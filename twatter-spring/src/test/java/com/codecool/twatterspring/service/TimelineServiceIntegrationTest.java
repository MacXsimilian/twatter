package com.codecool.twatterspring.service;

import com.codecool.twatterspring.model.Tweet;
import com.codecool.twatterspring.model.dto.TimelineTweetDTO;
import com.codecool.twatterspring.redis.config.TestConfig;
import com.codecool.twatterspring.redis.service.TimelineOwnerDao;
import com.codecool.twatterspring.repository.TwatterUserRepository;
import com.codecool.twatterspring.repository.TweetRepository;
import com.codecool.twatterspring.security.service.JwtService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import redis.clients.jedis.Jedis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.codecool.twatterspring.util.TestUtils.buildTweet;
import static com.codecool.twatterspring.util.TestUtils.buildUser;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assumptions.assumeThat;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest(classes = TestConfig.class, properties = {"spring.redis.host=localhost", "spring.redis.port=6370"})
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@Slf4j
class TimelineServiceIntegrationTest {

    @Autowired
    private TwatterUserRepository userRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private TweetRepository tweetRepository;

    @SpyBean
    private ProductionTimelineService timelineService;
    @Autowired
    private UserService userService;
    @Autowired
    private JwtService jwtService;
    @SpyBean
    private TimelineOwnerDao timelineOwnerDao;

    @Autowired
    private Jedis jedis;

    private final Map<Long, List<Tweet>> tweetMap = new HashMap<>();

    private static final Map<Long, String> usernameByIdMap = Map.of(1L, "trump", 2L, "obama");

    @BeforeEach
    void setUp() {
        var trump = buildUser("trump", passwordEncoder.encode("trump"));
        var obama = buildUser("obama", passwordEncoder.encode("obama"));
        trump.addFollower(obama);

        userRepository.saveAll(List.of(trump, obama));
        logOperation("saved 2 users: trump[1], obama[2] with obama[2] following trump[1]");

        var trumpTweets = List.of(buildTweet(trump, 2L), buildTweet(trump, 0L));
        var obamaTweets = List.of(buildTweet(obama, 3L), buildTweet(obama, 1L));

        tweetMap.put(1L, trumpTweets);
        tweetMap.put(2L, obamaTweets);

        tweetRepository.saveAll(trumpTweets);
        logOperation("saved 2 tweets for trump[1]");

        tweetRepository.saveAll(obamaTweets);
        logOperation("saved 2 tweets for obama[2]");
    }

    @AfterEach
    void tearDown() {
        jedis.flushDB();
    }

    @Test
    void smokeTest() {
        assertThat(tweetRepository).isNotNull();
    }

    @Test
    void retrievesUserTimelineCorrectly() {
        assumeThat(timelineOwnerDao.existsBy(1L)).isFalse();
        logAssumption("user 1 did not exist in Redis");

        var trumpUserTimeline = timelineService.getUserTimeline(1L);
        assertThat(trumpUserTimeline).containsExactlyElementsOf(toDTOs(tweetMap.get(1L)));
        logAssertion("user timeline of trump[1] was retrieved");

        verify(timelineService).loadTimelineOwnerFromDatabase(1L);
        logAssertion("method loadTimelineOwnerFromDatabase() was called once");

        assumeThat(timelineOwnerDao.existsBy(1L)).isTrue();
        logAssumption("user 1 did already exist in Redis");

        trumpUserTimeline = timelineService.getUserTimeline(1L);
        assertThat(trumpUserTimeline).containsExactlyElementsOf(toDTOs(tweetMap.get(1L)));
        logAssertion("user timeline of trump[1] was retrieved");

        verify(timelineService, times(1)).loadTimelineOwnerFromDatabase(anyLong());
        logAssertion("method loadTimelineOwnerFromDatabase() wasn't called again");
    }

    @Test
    void retrievesHomeTimelineCorrectly() {
        assumeThat(timelineOwnerDao.existsBy(1L)).isFalse();
        assumeThat(timelineOwnerDao.existsBy(2L)).isFalse();
        var obamaHomeTimeline = timelineService.getHomeTimeline(2L);
        assertThat(obamaHomeTimeline).containsExactlyElementsOf(toDTOs(List.of(
                tweetMap.get(2L).get(0),
                tweetMap.get(1L).get(0),
                tweetMap.get(2L).get(1),
                tweetMap.get(1L).get(1)
        )));
        verify(timelineService).loadTimelineOwnerFromDatabase(2L);

        assumeThat(timelineOwnerDao.existsBy(2L)).isTrue();
        obamaHomeTimeline = timelineService.getHomeTimeline(2L);
        assertThat(obamaHomeTimeline).containsExactlyElementsOf(toDTOs(List.of(
                tweetMap.get(2L).get(0),
                tweetMap.get(1L).get(0),
                tweetMap.get(2L).get(1),
                tweetMap.get(1L).get(1)
        )));
        verify(timelineService, times(1)).loadTimelineOwnerFromDatabase(anyLong());
    }

    @ParameterizedTest(name = "{index} - {0}")
    @MethodSource("provideFollowingTestArguments")
    void whenUserFollowsOrUnfollowsSomeone_ThenHomeTimelineOfFollowerGetsUpdated(String displayName,
                                                                                 boolean following,
                                                                                 boolean receiverActive,
                                                                                 Long followerId,
                                                                                 Long receiverId) {
        assumeThat(timelineOwnerDao.existsBy(1L)).isFalse();
        assumeThat(timelineOwnerDao.existsBy(2L)).isFalse();

        var followerUsername = usernameByIdMap.get(followerId);

        timelineService.loadTimelinesIntoRedisIfNotPresent(followerId);
        if (receiverActive)
            timelineService.loadTimelinesIntoRedisIfNotPresent(receiverId);

        var jwt = jwtService.createToken(followerUsername, List.of("USER"), Long.toString(followerId));
        if (following)
            userService.followUser(jwt, receiverId);
        else
            userService.unfollowUser(jwt, receiverId);

        var timelineOfFollower = timelineService.getHomeTimeline(followerId);

        List<TimelineTweetDTO> timelineOfFollowingReceiver;

        if (followerId == 1L)
            timelineOfFollowingReceiver = toDTOs(List.of(
                tweetMap.get(2L).get(0),
                tweetMap.get(1L).get(0),
                tweetMap.get(2L).get(1),
                tweetMap.get(1L).get(1)
            ));
        else timelineOfFollowingReceiver = toDTOs(List.of(
                tweetMap.get(2L).get(0),
                tweetMap.get(2L).get(1)
        ));

        if (following)
            assertThat(timelineOfFollower).containsExactlyElementsOf(timelineOfFollowingReceiver);
        else
            assertThat(timelineOfFollower).containsExactlyElementsOf(timelineOfFollowingReceiver);
    }

    private static Stream<Arguments> provideFollowingTestArguments() {
        return Stream.of(
            Arguments.of("Trump follows active Obama", true, true, 1L, 2L),
            Arguments.of("Trump follows inactive Obama", true, false, 1L, 2L),
            Arguments.of("Obama unfollows active Trump", false, true, 2L, 1L),
            Arguments.of("Obama unfollows inactive Trump", false, false, 2L, 1L)
        );
    }

    private List<TimelineTweetDTO> toDTOs(List<Tweet> tweets) {
        return tweets.stream().map(TimelineTweetDTO::fromEntity).collect(Collectors.toList());
    }

    private static void logAssumption(String assumption) {
        log.info("Correctly assumed that " + assumption + ".\n");
    }

    private static void logOperation(String operation) {
        log.info("Operation done: " + operation + ".\n");
    }

    private static void logAssertion(String assertion) {
        log.info("Correctly asserted that " + assertion + ".\n");
    }
}