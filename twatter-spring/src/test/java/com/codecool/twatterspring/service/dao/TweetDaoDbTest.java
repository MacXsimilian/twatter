package com.codecool.twatterspring.service.dao;

import com.codecool.twatterspring.model.Tweet;
import com.codecool.twatterspring.model.dto.IncomingTweetDTO;
import com.codecool.twatterspring.repository.TweetRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@ActiveProfiles("test")
@Import({TweetDaoDb.class, TwatterUserDaoDb.class})
class TweetDaoDbTest {

    @Autowired
    private TweetRepository tweetRepository;

    @Autowired
    private TweetDao tweetDao;

    @Autowired
    private EntityManager entityManager;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @Test
    void testSave() {
        var incoming = IncomingTweetDTO.builder().content("test").build();
        var tweet = Tweet.fromDTO(incoming, UUID.randomUUID(), 1L, "user", LocalDateTime.now());
        tweetDao.save(incoming, tweet.getId(), tweet.getUserId(), tweet.getUsername(), tweet.getDate());
        entityManager.flush();

        assertEquals(tweet, tweetRepository.getOne(tweet.getId()));
    }
}