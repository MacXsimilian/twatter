package com.codecool.twatterspring.service;

import com.codecool.twatterspring.model.Tweet;
import com.codecool.twatterspring.model.dto.TimelineTweetDTO;
import com.codecool.twatterspring.model.dto.UserSearchDTO;
import com.codecool.twatterspring.repository.TwatterUserRepository;
import com.codecool.twatterspring.util.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import javax.persistence.EntityManager;
import java.util.List;

import static com.codecool.twatterspring.util.TestUtils.buildUserWithEmail;
import static org.assertj.core.api.Assertions.as;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@DataJpaTest
@ActiveProfiles({"test"})
@Import({SearchService.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class SearchServiceTest {

    @Autowired
    ISearchService searchService;

    @Autowired
    TwatterUserRepository users;

    @Autowired
    private EntityManager entityManager;

    @MockBean
    private TweetService tweetService;

    @BeforeEach
    public void init() {

        var elon = buildUserWithEmail("elonmusk", "password");
        var trump = buildUserWithEmail("donaldtrump", "password");
        var gergo = buildUserWithEmail("gergo", "password");
        var elek = buildUserWithEmail("teszt", "password");
        var duck = buildUserWithEmail("donaldduck", "password");
        var rumsfeld = buildUserWithEmail("donaldrumsfeld", "password");
        var don = buildUserWithEmail("DONcorleone", "password");

        elon.addFollower(gergo);
        elon.addFollower(elek);
        trump.addFollower(gergo);
        gergo.addFollower(elek);

        users.saveAll(List.of(elon, trump, gergo, elek, duck, rumsfeld, don));
        entityManager.flush();
    }

    @Test
    public void testSearchNamesIfNoMatch() {
        assertThat(searchService.searchUsersBy("snow")).hasSize(0);
    }

    @Test
    public void testSearchNamesIfOneMatch() {
        assertThat(searchService.searchUsersBy("trump"))
            .hasSize(1)
            .containsExactly(new UserSearchDTO(2L, "donaldtrump"));
    }

    @Test
    public void testSearchNamesIfTwoMatches() {
        assertThat(searchService.searchUsersBy("rUm"))
            .hasSize(2)
            .containsExactly(
                new UserSearchDTO(2L, "donaldtrump"),
                new UserSearchDTO(6L, "donaldrumsfeld")
            );
    }

    @Test
    public void testSearchNamesIfFourMatches() {
        assertThat(searchService.searchUsersBy("DON"))
            .hasSize(4)
            .containsExactlyInAnyOrder(
                new UserSearchDTO(2L, "donaldtrump"),
                new UserSearchDTO(6L, "donaldrumsfeld"),
                new UserSearchDTO(5L, "donaldduck"),
                new UserSearchDTO(7L, "DONcorleone")
            );
    }

    @Test
    public void testSearchingTweetsByHashtag() {
        List<TimelineTweetDTO> hits = List.of(
                TimelineTweetDTO.fromEntity(TestUtils.buildTweet(3L, 3L)),
                TimelineTweetDTO.fromEntity(TestUtils.buildTweet(4L, 0L))
        );
        when(tweetService.provideTweetsForTrendingHashtag("tesla")).thenReturn(hits);
        assertThat(searchService.searchTweetsBy("tesla")).containsExactlyElementsOf(hits);
    }
}
