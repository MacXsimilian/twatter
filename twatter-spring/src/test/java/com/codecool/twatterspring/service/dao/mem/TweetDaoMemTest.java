package com.codecool.twatterspring.service.dao.mem;

import com.codecool.twatterspring.model.TwatterUser;
import com.codecool.twatterspring.model.Tweet;
import com.codecool.twatterspring.model.dto.IncomingTweetDTO;
import com.codecool.twatterspring.service.DaoMemTestConfig;
import com.codecool.twatterspring.service.dao.TwatterUserDao;
import com.codecool.twatterspring.service.dao.TweetDao;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.*;

import static com.codecool.twatterspring.util.TestUtils.*;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(properties = "testing.memdao=true")
@ContextConfiguration(classes = DaoMemTestConfig.class)
@ActiveProfiles({"test"})
class TweetDaoMemTest {

    @Autowired
    private TweetDao tweetDao;

    @Autowired
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    private TwatterUserDao userDao;

    private final Map<Long, List<Tweet>> tweetsById = new HashMap<>();

    @BeforeEach
    void setUp() {
        var users = getUsers();
        assertThat(users).isEmpty();
        var tweets = getTweets();
        assertThat(tweets).isEmpty();

        var trump = buildUserWithEmail("trump", "password");
        trump.setId(1L);
        var elon = buildUserWithEmail("elon", "password");
        elon.setId(2L);
        var gergo = buildUserWithEmail("gergo", "password");
        gergo.setId(3L);

        trump.addFollower(elon);
        trump.addFollower(gergo);
        elon.addFollower(gergo);

        users.put(1L, trump);
        users.put(2L, elon);
        users.put(3L, gergo);

        tweetsById.put(1L, new LinkedList<>());
        tweetsById.put(2L, new LinkedList<>());
        tweetsById.put(3L, new LinkedList<>());

        tweetsById.get(1L).addAll(List.of(buildTweet(trump, -5L), buildTweet(trump, -10L)));
        tweetsById.get(2L).addAll(List.of(buildTweet(elon, -3L)));
        tweets.put(1L, tweetsById.get(1L));
        tweets.put(2L, tweetsById.get(2L));
    }

    @AfterEach
    void tearDown() {
        getUsers().clear();
        getTweets().clear();
    }

    @Test
    void savesNewTweet() {
        var incoming = IncomingTweetDTO.builder().content("tweet").build();
        var tweetId = UUID.randomUUID();
        var userId = 1L;
        var username = "trump";
        var postedAt = LocalDateTime.now();

        tweetDao.save(incoming, tweetId, userId, username, postedAt);

        Tweet saved = Tweet.fromDTO(incoming, tweetId, userId, username, postedAt);

        assertThat(getTweets().get(1L)).contains(saved);
    }

    @Test
    void retrievesUserTimelineCorrectly() {
        var tweets = tweetDao.provideTweetsForUserTimelineBy(1L);
        assertThat(tweets).containsExactlyElementsOf(toDTOs(tweetsById.get(1L)));
    }

    @Test
    void retrievesHomeTimelineCorrectly() {
        var timeline = new LinkedList<Tweet>();
        timeline.addAll(tweetsById.get(1L));
        timeline.addAll(tweetsById.get(2L));
        timeline.sort(Comparator.comparing(Tweet::getDate).reversed());

        var tweets = tweetDao.provideTweetsForHomeTimelineBy(3L);

        assertThat(tweets).containsExactlyElementsOf(toDTOs(timeline));
    }

    @Test
    void contextLoaded() {
        assertThat(tweetDao.getClass()).isEqualTo(TweetDaoMem.class);
        assertThat(userDao.getClass()).isEqualTo(TwatterUserDaoMem.class);
    }

    @SuppressWarnings("unchecked")
    private Map<Long, List<Tweet>> getTweets() {
        return (Map<Long, List<Tweet>>) getFieldValue(tweetDao, "tweets");
    }

    @SuppressWarnings("unchecked")
    private Map<Long, TwatterUser> getUsers() {
        return (Map<Long, TwatterUser>) getFieldValue(userDao, "users");
    }

    private Object getFieldValue(Object object, String fieldName) {
        Object value = null;
        try {
            Field field = object.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            value = field.get(object);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return value;
    }
}