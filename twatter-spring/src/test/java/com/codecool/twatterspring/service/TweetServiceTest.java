package com.codecool.twatterspring.service;

import com.codecool.twatterspring.model.Tweet;
import com.codecool.twatterspring.model.dto.IncomingTweetDTO;
import com.codecool.twatterspring.model.dto.TimelineTweetDTO;
import com.codecool.twatterspring.redis.service.TimelineOwnerDao;
import com.codecool.twatterspring.security.service.JwtService;
import com.codecool.twatterspring.service.dao.TweetDao;
import com.codecool.twatterspring.util.TestUtils;
import com.codecool.twatterspring.util.Utils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@SpringBootTest(properties = {"spring.redis.host=localhost", "spring.redis.port=6370"})
@ExtendWith(MockitoExtension.class)
@ActiveProfiles("test")
public class TweetServiceTest {

    private TweetService service;

    @MockBean
    TweetDao tweetDao;
    @MockBean
    ITrendingApiService trendingApi;
    @MockBean
    private JwtService jwtService;
    @MockBean
    TimelineOwnerDao timelineOwnerDao;

    @BeforeEach
    public void init() {
        service = new TweetService(tweetDao, jwtService, trendingApi, timelineOwnerDao);
    }

    @Test
    public void initTest() {
        assertThat(service).isNotNull();
    }

    @Test
    public void handleNewTweetTest() {

        IncomingTweetDTO incoming = IncomingTweetDTO.builder()
            .content("Test tweet")
            .build();
        UUID tweetId = UUID.randomUUID();

        Tweet tweet1 = Tweet.builder()
            .id(tweetId)
            .content("Test tweet")
            .date(LocalDateTime.now())
            .userId(1L)
            .build();

        TimelineTweetDTO outgoing = TimelineTweetDTO.builder()
            .id(tweetId.toString())
            .content(tweet1.getContent())
            .userId(tweet1.getUserId())
            .postedAt(Utils.convertDate(tweet1.getDate()))
            .build();

        when(jwtService.parseUserIdFromToken(anyString())).thenReturn(1L);
        when(jwtService.parseUsernameFromToken(anyString())).thenReturn("");
        when(tweetDao.save(
            any(IncomingTweetDTO.class), any(UUID.class), anyLong(), anyString(), any(LocalDateTime.class))
        ).thenReturn(tweet1);
        assertThat(service.handleNewTweet(incoming, tweetId, "tóken")).isEqualTo(outgoing);
    }

    @Test
    public void testHandlingTweetSearchByHashtag() {
        List<TimelineTweetDTO> hits = List.of(
                TimelineTweetDTO.fromEntity(TestUtils.buildTweet(1L, 0L)),
                TimelineTweetDTO.fromEntity(TestUtils.buildTweet(2L, 0L)),
                TimelineTweetDTO.fromEntity(TestUtils.buildTweet(1L, 3L))
        );
        String[] tweetIds = hits.stream()
                .map(TimelineTweetDTO::getId)
                .toArray(String[]::new);

        List<UUID> uuidIds = Arrays.stream(tweetIds)
                .map(UUID::fromString)
                .collect(Collectors.toList());

        when(trendingApi.getTweetIDsByTrendingHashtag("Trump")).thenReturn(tweetIds);
        when(tweetDao.provideTweetsForTrendingHashtagBy(uuidIds)).thenReturn(hits);

        assertThat(service.provideTweetsForTrendingHashtag("Trump")).containsExactlyElementsOf(hits);

    }
}
