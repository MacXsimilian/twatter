package com.codecool.twatterspring.service;

import com.codecool.twatterspring.model.TwatterUser;
import com.codecool.twatterspring.model.dto.SizesDTO;
import com.codecool.twatterspring.model.dto.TwatterUserProfileDTO;
import com.codecool.twatterspring.redis.service.TimelineOwnerDao;
import com.codecool.twatterspring.repository.TwatterUserRepository;
import com.codecool.twatterspring.security.service.JwtService;
import com.codecool.twatterspring.service.dao.TwatterUserDaoDb;
import com.codecool.twatterspring.service.dao.TweetDao;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import static com.codecool.twatterspring.util.TestUtils.buildUserWithEmail;
import static com.codecool.twatterspring.util.TestUtils.mapped;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assumptions.assumeThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@DataJpaTest
@ActiveProfiles("test")
@Import({UserService.class, TwatterUserDaoDb.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@Slf4j
class UserServiceIntegrationTest {

    private @MockBean TimelineOwnerDao timelineOwnerDao;
    private @MockBean JwtService jwtService;
    private @MockBean PasswordEncoder passwordEncoder;
    private @MockBean TweetDao tweetDao;

    private @Autowired TwatterUserRepository users;
    private @Autowired EntityManager entityManager;

    private @SpyBean UserService userService;

    private static final Function<TwatterUser, String> MAPPER = TwatterUser::getName;

    private TwatterUser userFound;

    @BeforeEach
    public void init() {
        var elon = buildUserWithEmail("elon", "password");
        var trump = buildUserWithEmail("trump", "password");
        var gergo = buildUserWithEmail("gergo", "password");
        var lajos = buildUserWithEmail("lajos", "password");

        elon.addFollower(gergo);
        elon.addFollower(lajos);

        trump.addFollower(gergo);

        gergo.addFollower(lajos);

        this.userFound= elon;
        users.saveAll(List.of(elon, trump, gergo, lajos));
        entityManager.flush();
        log.info("Saved testable users.");
    }

    @Test
    public void testFollowUser() {
        assumeThat(users.findById(1L).orElseThrow().getName()).isEqualTo("elon");
        assumeThat(users.findById(3L).orElseThrow().getName()).isEqualTo("gergo");

        var followeesOfElon = users.getFolloweesByUserId(1L);
        assumeThat(mapped(followeesOfElon, MAPPER)).doesNotContain("gergo");
        var followersOfGergo = users.getFollowersByUserId(3L);
        assumeThat(mapped(followersOfGergo, MAPPER)).doesNotContain("elon");

        when(jwtService.parseUserIdFromToken(anyString())).thenReturn(1L);
        log.info("Test setup complete.");

        userService.followUser("tóken", 3L);
        entityManager.flush();
        log.info("Method called, starting assertion.");

        followeesOfElon = users.getFolloweesByUserId(1L);
        assertThat(mapped(followeesOfElon, MAPPER)).contains("gergo");
        followersOfGergo = users.getFolloweesByUserId(3L);
        assertThat(mapped(followersOfGergo, MAPPER)).contains("elon");
    }

    @Test
    public void testUnfollowUser() {
        assumeThat(users.findById(2L).orElseThrow().getName()).isEqualTo("trump");
        assumeThat(users.findById(3L).orElseThrow().getName()).isEqualTo("gergo");

        var followeesOfGergo = users.getFolloweesByUserId(3L);
        assumeThat(mapped(followeesOfGergo, MAPPER)).contains("trump");
        var followersOfTrump = users.getFollowersByUserId(2L);
        assumeThat(mapped(followersOfTrump, MAPPER)).contains("gergo");

        when(jwtService.parseUserIdFromToken(anyString())).thenReturn(3L);
        log.info("Test setup complete.");

        userService.unfollowUser("tóken", 2L);
        entityManager.flush();
        log.info("Method called, starting assertion.");

        followeesOfGergo = users.getFolloweesByUserId(3L);
        assertThat(mapped(followeesOfGergo, MAPPER)).doesNotContain("trump");
        followersOfTrump = users.getFollowersByUserId(2L);
        assertThat(mapped(followersOfTrump, MAPPER)).doesNotContain("gergo");
    }

    @Test
    public void testGettingUserProfileByIdIfExists() {
        var userFound = this.userFound;
        userFound.setId(1L);
        var profileHit = TwatterUserProfileDTO.fromEntity(
                userFound,
                new SizesDTO(0, 2),
                Set.of(),
                0L
        );
        assertThat(userService.getUserProfileBy(1L)).isEqualTo(profileHit);
    }
    @Test
    public void testGettingUserProfileByIdIfNotExists() {
        assertThrows(
                EntityNotFoundException.class,
                () -> userService.getUserProfileBy(10L)
        );
    }
    @Test
    public void testGettingUserProfileByUsernameIfExists() {
        var userFound = this.userFound;
        userFound.setId(1L);
        var profileHit = TwatterUserProfileDTO.fromEntity(
                userFound,
                new SizesDTO(0, 2),
                Set.of(),
                0L
        );
        assertThat(userService.getUserProfileBy("elon")).isEqualTo(profileHit);
    }
    @Test
    public void testGettingUserProfileByUsernameIfNotExists() {
        assertThrows(
                EntityNotFoundException.class,
                () -> userService.getUserProfileBy("melon")
        );
    }
}