package com.codecool.twatterspring.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@ConditionalOnProperty(value = "testing.memdao", havingValue = "true")
@ComponentScan(
    basePackages = {"com.codecool.twatterspring.service.dao"},
    useDefaultFilters = false,
    includeFilters = @Filter(type = FilterType.REGEX, pattern = "\\b((?!Db).)*\\b")
)
@Slf4j
public class DaoMemTestConfig {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }
}
