package com.codecool.twatterspring.security.config;

import com.codecool.twatterspring.config.TwatterWebMvcTest;
import com.codecool.twatterspring.controller.AuthController;
import com.codecool.twatterspring.model.TwatterUser;
import com.codecool.twatterspring.model.dto.AuthDTO;
import com.codecool.twatterspring.security.service.AuthService;
import com.codecool.twatterspring.security.service.JwtService;
import com.codecool.twatterspring.service.dao.TwatterUserDao;
import com.codecool.twatterspring.util.TestUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseCookie;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import javax.servlet.http.Cookie;
import java.time.Duration;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TwatterWebMvcTest(controllers = AuthController.class)
class AuthControllerSecurityTest extends AbstractControllerSecurityTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AuthService authService;

    @Autowired
    private JwtService jwtService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @MockBean
    private TwatterUserDao userDao;

    private static final String NON_EXISTENT_USER_NAME = "non_existent_user";
    private static final String EXISTING_USER_NAME = "existing_user";

    @BeforeEach
    void setUp() {
        var emptyUser = TwatterUser.builder().build();
        var existingUser = TestUtils.buildUserWithEmail(EXISTING_USER_NAME, "password", passwordEncoder);
        existingUser.setId(1L);
        when(userDao.findBy(NON_EXISTENT_USER_NAME)).thenReturn(Optional.of(emptyUser));
        when(userDao.findBy(EXISTING_USER_NAME)).thenReturn(Optional.of(existingUser));
        when(userDao.exists(EXISTING_USER_NAME)).thenReturn(true);
    }

    @Test
    void smokeTest() {
        assertThat(mockMvc).isNotNull();
        assertThat(authService).isNotNull();
        assertThat(jwtService).isNotNull();
        assertThat(userDao).isNotNull();
        assertThat(passwordEncoder).isNotNull();
    }

    @Test
    void unauthenticatedUserCannotLogout() throws Exception {
        mockMvc.perform(post(BASE_URL + "/auth/logout"))
            .andExpect(status().isForbidden());
    }

    @Test
    void authenticatedUserCanLogout() throws Exception {
        mockMvc.perform(
            post(BASE_URL + "/auth/logout")
                .cookie(buildTokenCookie(EXISTING_USER_NAME, 1L))
        )
            .andExpect(status().isOk())
            .andExpect(result -> {
                Cookie jwtCookie = result.getResponse().getCookie("JWT");
                if (jwtCookie == null)
                    throw new Exception("Invalidated cookie not present in logout response.");
                if (!"".equals(jwtCookie.getValue()))
                    throw new Exception("Invalidated cookie value is not empty.");
                if (jwtCookie.getMaxAge() != 0)
                    throw new Exception("Invalidated cookie max age has to be equal to 0.");
            });
    }

    @Test
    void anyoneCanTryToRegister() throws Exception {
        AuthDTO dto = AuthDTO.builder().username(NON_EXISTENT_USER_NAME).password("password").build();
        String json = mapper.writeValueAsString(dto);

        mockMvc.perform(doPostWithJson("/auth/register", json))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(content().string("true"));
    }

    @Test
    void existingUserCannotRegister() throws Exception {
        AuthDTO dto = AuthDTO.builder().username(EXISTING_USER_NAME).password("password").build();
        String json = mapper.writeValueAsString(dto);

        mockMvc.perform(doPostWithJson("/auth/register", json))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(content().string("false"));
    }

    @Test
    void nonExistentUserCanTryButIsNotAbleToLogin() throws Exception {
        AuthDTO dto = AuthDTO.builder().username(NON_EXISTENT_USER_NAME).password("password").build();
        String json = mapper.writeValueAsString(dto);

        mockMvc.perform(doPostWithJson("/auth/login", json))
            .andExpect(status().isOk())
            .andExpect(content().string("-1"));
    }

    @Test
    void userWithValidCredentialsCanLogin() throws Exception {
        AuthDTO dto = AuthDTO.builder().username(EXISTING_USER_NAME).password("password").build();
        String json = mapper.writeValueAsString(dto);

        mockMvc.perform(doPostWithJson("/auth/login", json))
            .andExpect(status().isOk())
            .andExpect(content().string("1"));
    }

    @Test
    void whenTokenCookiePresentAndValid_IsLoggedInReturnsTrue() throws Exception {
        mockMvc.perform(
            get(BASE_URL + "/auth/isloggedin")
                .cookie(buildTokenCookie(EXISTING_USER_NAME, 1L))
        )
            .andExpect(status().isOk())
            .andExpect(content().string("true"));
    }

    @Test
    void whenTokenCookieNotPresent_IsLoggedInReturnsFalse() throws Exception {
        mockMvc.perform(get(BASE_URL + "/auth/isloggedin"))
            .andExpect(status().isOk())
            .andExpect(content().string("false"));
    }

    @Test
    void whenTokenCookiePresentButInvalid_IsLoggedInReturnsFalse() throws Exception {
        mockMvc.perform(
            get(BASE_URL + "/auth/isloggedin")
                .cookie(buildInvalidTokenCookie(EXISTING_USER_NAME, 1L))
        )
            .andExpect(status().isOk())
            .andExpect(content().string("false"));
    }

    private MockHttpServletRequestBuilder doPostWithJson(String url, String json) {
        return post(BASE_URL + url)
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .content(json);
    }

    private Cookie buildTokenCookie(String username, Long userId) {
        String jwt = jwtService.createToken(username, List.of("USER"), userId.toString());
        ResponseCookie responseCookie = authService.createTokenCookie(jwt, Duration.ofSeconds(-1));
        return new Cookie("JWT", responseCookie.getValue());
    }

    private Cookie buildInvalidTokenCookie(String username, Long userId) {
        Claims claims = Jwts.claims().setSubject(username);
        claims.put("roles", List.of("USER"));
        claims.put("id", userId);
        String jwt = Jwts.builder()
            .setClaims(claims)
            .setIssuedAt(new Date(1))
            .setExpiration(new Date(2))
            .signWith(SignatureAlgorithm.HS256, Base64.getEncoder().encodeToString("secret".getBytes()))
            .compact();

        ResponseCookie responseCookie = authService.createTokenCookie(jwt, Duration.ofSeconds(-1));
        return new Cookie("JWT", responseCookie.getValue());
    }
}