package com.codecool.twatterspring.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcRegistrations;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("test")
@ComponentScan(
    useDefaultFilters = false,
    includeFilters = @Filter(type = FilterType.ASSIGNABLE_TYPE, classes = WebMvcRegistrations.class)
)
@Slf4j
public class WebMvcTestGlobalConfig implements InitializingBean {

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println();
        log.info(getClass().getSimpleName() + " initialized.");
        System.out.println();
    }
}
