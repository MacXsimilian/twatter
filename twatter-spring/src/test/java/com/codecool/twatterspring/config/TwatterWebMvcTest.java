package com.codecool.twatterspring.config;

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.FilterType;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static org.springframework.context.annotation.ComponentScan.Filter;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@WebMvcTest(includeFilters = @Filter(type = FilterType.ASSIGNABLE_TYPE, classes = WebMvcTestGlobalConfig.class))
public @interface TwatterWebMvcTest {
    @AliasFor(
        annotation = WebMvcTest.class,
        attribute = "controllers"
    )
    Class<?>[] controllers();
}
