package com.codecool.twatterspring.redis;

import com.codecool.twatterspring.model.dto.IncomingTweetDTO;
import com.codecool.twatterspring.model.dto.TimelineTweetDTO;
import com.codecool.twatterspring.redis.config.RedisProperties;
import com.codecool.twatterspring.redis.config.TestConfig;
import com.codecool.twatterspring.redis.model.TimelineOwner;
import com.codecool.twatterspring.redis.service.TimelineOwnerDao;
import com.codecool.twatterspring.redis.service.TimelineOwnerDaoRedis;
import com.codecool.twatterspring.service.dao.TweetDaoDb;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.redis.DataRedisTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import redis.clients.jedis.Jedis;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@DataRedisTest(properties = {"spring.redis.host=localhost", "spring.redis.port=6370"})
@Import({RedisProperties.class, TimelineOwnerDaoRedis.class, TestConfig.class})
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
class RedisTest {

    @MockBean
    TweetDaoDb tweetDao;

    @Autowired
    RedisProperties redisProperties;

    @Autowired
    private TimelineOwnerDao timelineOwnerDao;

    @Autowired
    private Jedis jedis;

    @SuppressWarnings("OptionalGetWithoutIsPresent")
    @BeforeEach
    void setUp() {
        timelineOwnerDao.persist(1L, "user1");
        timelineOwnerDao.persist(2L, "user2");
        timelineOwnerDao.persist(3L, "user3");
        timelineOwnerDao.persist(4L, "user4");

        addFollowers(1L, 2L, 3L, 4L);
        addFollowers(2L, 3L);
        addFollowers(3L, 1L, 4L);
        addFollowers(4L, 2L, 3L);

        assertThat(timelineOwnerDao.findByUserId(1L)).isPresent();
        assertThat(timelineOwnerDao.findByUserId(2L)).isPresent();
        assertThat(timelineOwnerDao.findByUserId(3L)).isPresent();
        assertThat(timelineOwnerDao.findByUserId(4L)).isPresent();

        assertThat(timelineOwnerDao.findByUserId(1L).get().getFollowers()).containsExactly(2L, 3L, 4L);
        assertThat(timelineOwnerDao.findByUserId(2L).get().getFollowers()).containsExactly(3L);
        assertThat(timelineOwnerDao.findByUserId(3L).get().getFollowers()).containsExactly(1L, 4L);
        assertThat(timelineOwnerDao.findByUserId(4L).get().getFollowers()).containsExactly(2L, 3L);
    }

    @AfterEach
    void tearDown() {
        jedis.flushDB();
    }

    @Test
    void smokeTest() {
        assertThat(redisProperties.getRedisPort()).isEqualTo(6370);
        assertThat(jedis).isNotNull();
    }

    @ParameterizedTest
    @ValueSource(longs = {1L, 2L, 3L, 4L})
    void userTimelineGetsUpdated(Long tweeterId) {
        var tweet = buildIncomingTweetDTO(tweeterId);
        String postedAt = convertDate(LocalDateTime.now());
        UUID tweetId = UUID.randomUUID();

        timelineOwnerDao.saveNewTweet(tweet, tweetId, tweeterId, postedAt);

        assertThat(timelineOwnerDao.getUserTimelineBy(tweeterId))
            .containsExactly(buildTimelineTweetDTO(tweetId, tweeterId, postedAt));
    }

    @ParameterizedTest
    @ValueSource(longs = {1L, 2L, 3L, 4L})
    void homeTimelinesOfFollowersGetUpdated(Long tweeterId) {
        var tweet = buildIncomingTweetDTO(tweeterId);
        String postedAt = convertDate(LocalDateTime.now());
        UUID tweetId = UUID.randomUUID();

        timelineOwnerDao.saveNewTweet(tweet, tweetId, tweeterId, postedAt);

        timelineOwnerDao.findByUserId(tweeterId).orElseThrow().getFollowers().forEach(
            followerId -> assertThat(timelineOwnerDao.getHomeTimelineBy(followerId))
                .containsExactly(buildTimelineTweetDTO(tweetId, tweeterId, postedAt))
        );
    }

    @Test
    void persistsTimelineWithExistingTweets() {
        Long userId = 5L;
        String username = "user5";
        var userTweets = List.of(
            buildTimelineTweetDTO(UUID.randomUUID(), userId, convertDate(LocalDateTime.now())),
            buildTimelineTweetDTO(UUID.randomUUID(), userId, convertDate(LocalDateTime.now())),
            buildTimelineTweetDTO(UUID.randomUUID(), userId, convertDate(LocalDateTime.now())),
            buildTimelineTweetDTO(UUID.randomUUID(), userId, convertDate(LocalDateTime.now()))
        );
        var homeTweets = List.of(
            buildTimelineTweetDTO(UUID.randomUUID(), userId, convertDate(LocalDateTime.now())),
            buildTimelineTweetDTO(UUID.randomUUID(), userId, convertDate(LocalDateTime.now())),
            buildTimelineTweetDTO(UUID.randomUUID(), userId, convertDate(LocalDateTime.now())),
            buildTimelineTweetDTO(UUID.randomUUID(), userId, convertDate(LocalDateTime.now()))
        );
        var timelineOwner = TimelineOwner.builder()
            .userId(userId)
            .username(username)
            .userTweetDTOS(userTweets)
            .homeTweetDTOS(homeTweets)
            .build();
        timelineOwnerDao.persist(timelineOwner);

        assertThat(timelineOwnerDao.getUserTimelineBy(userId)).containsExactlyElementsOf(userTweets);
        assertThat(timelineOwnerDao.getHomeTimelineBy(userId)).containsExactlyElementsOf(homeTweets);
    }

    @Test
    void whenFollowerIsAdded_HomeTimelineGetsUpdated() {
        Long tweeterId = 2L, followerID = 1L;
        var incoming = buildIncomingTweetDTO(tweeterId);
        var tweetId = UUID.randomUUID();
        String postedAt = convertDate(LocalDateTime.now());
        timelineOwnerDao.saveNewTweet(incoming, tweetId, tweeterId, postedAt);

        timelineOwnerDao.followUser(tweeterId, followerID);

        assertThat(timelineOwnerDao.getHomeTimelineBy(followerID))
            .containsAll(timelineOwnerDao.getUserTimelineBy(tweeterId));
    }

    @Test
    void whenFollowerIsRemoved_HomeTimelineGetsUpdated() {
        Long tweeterId = 1L, followerID = 2L;
        var incoming = buildIncomingTweetDTO(tweeterId);
        var tweetId = UUID.randomUUID();
        String postedAt = convertDate(LocalDateTime.now());
        timelineOwnerDao.saveNewTweet(incoming, tweetId, tweeterId, postedAt);

        assertThat(timelineOwnerDao.getHomeTimelineBy(followerID))
            .containsAll(timelineOwnerDao.getUserTimelineBy(tweeterId));

        timelineOwnerDao.unFollowUser(tweeterId, followerID);

        assertThat(timelineOwnerDao.getHomeTimelineBy(followerID))
            .doesNotContainAnyElementsOf(timelineOwnerDao.getUserTimelineBy(tweeterId));
    }

    @Test
    void timeToLiveTest() {
        timelineOwnerDao.persist(8L, "user8");
        jedis.expire("timeline:8", 0);
        assertThat(timelineOwnerDao.existsBy(8L)).isFalse();
    }

    @Test
    void whenExpiredUserIsFollowed_TheirTweetsAreAddedToHomeTimeline() {
        Long followerId = 1L, expiredUserId = 8L;
        var expiredUserTweets = List.of(
            buildTimelineTweetDTO(UUID.randomUUID(), expiredUserId, convertDate(LocalDateTime.now())),
            buildTimelineTweetDTO(UUID.randomUUID(), expiredUserId, convertDate(LocalDateTime.now())),
            buildTimelineTweetDTO(UUID.randomUUID(), expiredUserId, convertDate(LocalDateTime.now()))
        );
        when(tweetDao.provideTweetsForUserTimelineBy(expiredUserId)).thenReturn(expiredUserTweets);

        timelineOwnerDao.followUser(expiredUserId, followerId);

        assertThat(timelineOwnerDao.getHomeTimelineBy(followerId)).containsAll(expiredUserTweets);
    }

    @Test
    void testGetUserTimeline() {
        var t1 = buildTimelineTweetDTO(UUID.randomUUID(), 1L, convertDate(LocalDateTime.now().minusDays(1)));
        var t2 = buildTimelineTweetDTO(UUID.randomUUID(), 1L, convertDate(LocalDateTime.now().minusHours(1)));
        var t3 = buildTimelineTweetDTO(UUID.randomUUID(), 1L, convertDate(LocalDateTime.now()));
        var tweets = List.of(t3, t2, t1);
        tweets.forEach(t -> {
            timelineOwnerDao.saveNewTweet(
                IncomingTweetDTO.builder().content(t.getContent()).build(),
                UUID.fromString(t.getId()),
                t.getUserId(),
                t.getPostedAt()
            );
        });

        assertThat(timelineOwnerDao.getUserTimelineBy(1L))
            .containsExactly(t3, t2, t1);
    }

    private void addFollowers(Long followeeId, Long... followerIds) {
        for (Long followerId : followerIds)
            timelineOwnerDao.followUser(followeeId, followerId);
    }

    private static String convertDate(LocalDateTime dateTime) {
        return Long.toString(dateTime.toEpochSecond(ZoneOffset.UTC));
    }

    private static IncomingTweetDTO buildIncomingTweetDTO(Long tweeterId) {
        return IncomingTweetDTO.builder().content("tweet" + tweeterId).build();
    }

    private static TimelineTweetDTO buildTimelineTweetDTO(UUID tweetId, Long userId, String postedAt) {
        return TimelineTweetDTO.builder()
            .id(tweetId.toString())
            .username("user" + userId)
            .userId(userId)
            .content("tweet" + userId)
            .postedAt(postedAt)
            .build();
    }
}